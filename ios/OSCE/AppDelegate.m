#import "AppDelegate.h"
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <ReactNativeNavigation/ReactNativeNavigation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <UIKit/UIKit.h>
#import <Firebase.h>
#import "RNSplashScreen.h"
#import "RNFirebaseNotifications.h"
#import "RNFirebaseMessaging.h"
#import <UserNotifications/UserNotifications.h>
#import <RNCPushNotificationIOS.h>
#import <RNGoogleSignin/RNGoogleSignin.h>
#import <React/RCTBridge.h>
#import <React/RCTLinkingManager.h>
#import <RNBranch/RNBranch.h>
#import <Mixpanel/Mixpanel.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  NSURL *jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
  [ReactNativeNavigation bootstrap:jsCodeLocation launchOptions:launchOptions];
   [RNSplashScreen show];
   [FIRApp configure];
   [RNFirebaseNotifications configure];
  [Mixpanel sharedInstanceWithToken:@"475e25103ef731274d35f1272f68c789"];
  UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
  center.delegate = self;
  [RNBranch initSessionWithLaunchOptions:launchOptions isReferrable:YES]; // <-- add this
  return YES;
}

- (BOOL)application:(UIApplication *)application
    continueUserActivity:(nonnull NSUserActivity *)userActivity
    restorationHandler: (nonnull void (^)(NSArray<id<UIUserActivityRestoring>> *_Nullable))restorationHandler
{
  return [RNBranch continueUserActivity:userActivity];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
  return [RNBranch application:application openURL:url options:options];


  //before end
//  BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url
//                  sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
//                         annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];

  // ||
  //  [[GIDSignIn sharedInstance] handleURL:url
  //                                    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
  //                                           annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];


//  return handled;


}

////- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
////[[RNFirebaseNotifications instance] didReceiveLocalNotification:notification];
////}
////- (void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo
////fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler{
////[[RNFirebaseNotifications instance] didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
////}
////- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
////[[RNFirebaseMessaging instance] didRegisterUserNotificationSettings:notificationSettings];
////}
//
//// Required to register for notifications
//- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
//{
// [RNCPushNotificationIOS didRegisterUserNotificationSettings:notificationSettings];
//}
//// Required for the register event.
//- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
//{
// [RNCPushNotificationIOS didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
//}
//// Required for the notification event. You must call the completion handler after handling the remote notification.
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
//fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//{
//  [RNCPushNotificationIOS didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
//}
//// Required for the registrationError event.
//- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
//{
// [RNCPushNotificationIOS didFailToRegisterForRemoteNotificationsWithError:error];
//}
//// IOS 10+ Required for localNotification event
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center
//didReceiveNotificationResponse:(UNNotificationResponse *)response
//         withCompletionHandler:(void (^)(void))completionHandler
//{
//  [RNCPushNotificationIOS didReceiveNotificationResponse:response];
//  completionHandler();
//}
//// IOS 4-10 Required for the localNotification event.
//- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
//{
// [RNCPushNotificationIOS didReceiveLocalNotification:notification];
//}
//
////Called when a notification is delivered to a foreground app.
//-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
//{
//  completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
//}
//
////-(void)userNotificationCenter:(UNUserNotificationCenter *)center
////didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(nonnull void
////    (^)(void))completionHandler
////{
////  NSMutableDictionary *userData = [NSMutableDictionary
////                                   dictionaryWithDictionary:response.notification.request.content.userInfo];
////  [userData setObject:@(1) forKey:@"openInForeground"];
////  [RNCPushNotificationIOS didReceiveRemoteNotification:userData];
////  completionHandler();
////}
//
//
//
//// Called when a notification is delivered to a foreground app.
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center
//       willPresentNotification:(UNNotification *)notification
//         withCompletionHandler:
//             (void (^)(UNNotificationPresentationOptions options))
//                 completionHandler {
//  completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert |
//                    UNAuthorizationOptionBadge);
//}
//
//// Required to register for notifications
//- (void)application:(UIApplication *)application
//    didRegisterUserNotificationSettings:
//        (UIUserNotificationSettings *)notificationSettings {
//  [RNCPushNotificationIOS
//      didRegisterUserNotificationSettings:notificationSettings];
//}
//// Required for the register event.
//- (void)application:(UIApplication *)application
//    didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//  [RNCPushNotificationIOS
//      didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
//}
//// Required for the notification event. You must call the completion handler
//// after handling the remote notification.
//- (void)application:(UIApplication *)application
//    didReceiveRemoteNotification:(NSDictionary *)userInfo
//          fetchCompletionHandler:
//              (void (^)(UIBackgroundFetchResult))completionHandler {
//  [RNCPushNotificationIOS didReceiveRemoteNotification:userInfo
//                                fetchCompletionHandler:completionHandler];
//}
//// Required for the registrationError event.
//- (void)application:(UIApplication *)application
//    didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
//  [RNCPushNotificationIOS
//      didFailToRegisterForRemoteNotificationsWithError:error];
//}
//// Required for the localNotification event.
//- (void)application:(UIApplication *)application
//    didReceiveLocalNotification:(UILocalNotification *)notification {
//  [RNCPushNotificationIOS didReceiveLocalNotification:notification];
//}
//// IOS 10+ Required for local notification tapped event
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center
//    didReceiveNotificationResponse:(UNNotificationResponse *)response
//             withCompletionHandler:(void (^)(void))completionHandler {
//  NSMutableDictionary *userData = [NSMutableDictionary
//                                    dictionaryWithDictionary:response.notification.request.content.userInfo];
//   [userData setObject:@(1) forKey:@"openedInForeground"];
//  [RNCPushNotificationIOS didReceiveNotificationResponse:response];
//  completionHandler();
//}
//
//
//
//
//- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge {
//#if DEBUG
//  return
//      [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index"
//                                                     fallbackResource:nil];
//#else
//  return [[NSBundle mainBundle] URLForResource:@"main"
//                                 withExtension:@"jsbundle"];
//#endif
//}

// Required to register for notifications
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
 [RNCPushNotificationIOS didRegisterUserNotificationSettings:notificationSettings];
}
// Required for the register event.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
 [RNCPushNotificationIOS didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}
// Required for the notification event. You must call the completion handler after handling the remote notification.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
  [RNCPushNotificationIOS didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}
// Required for the registrationError event.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
 [RNCPushNotificationIOS didFailToRegisterForRemoteNotificationsWithError:error];
}
// IOS 10+ Required for localNotification event
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler
{
  [RNCPushNotificationIOS didReceiveNotificationResponse:response];
  completionHandler();
}
// IOS 4-10 Required for the localNotification event.
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
 [RNCPushNotificationIOS didReceiveLocalNotification:notification];
}

//Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
  completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}


//- (BOOL)application:(UIApplication *)application
//    continueUserActivity:(nonnull NSUserActivity *)userActivity
//    restorationHandler: (nonnull void (^)(NSArray<id<UIUserActivityRestoring>> *_Nullable))restorationHandler
//{
//  return [RNBranch continueUserActivity:userActivity];
//}

@end
