
import { AsyncStorage } from "react-native";
import { applyMiddleware, createStore, combineReducers, compose } from "redux";
import thunk from "redux-thunk";
import * as reducers from "./../reducers";
import { persistStore, autoRehydrate } from "redux-persist";
import { createLogger } from "redux-logger";
import promise from "./promise";
import array from "./array";
import whitelist from "./whitelist";
import { goToFirstScreen, goHome } from "../config/navigation";

export const storeObj = {};

export default function setup() {






  AsyncStorage.setItem('triggerEvent','true'); 

  const isDev = global.isDebuggingInChrome || __DEV__; // eslint-disable-line

  const logger = createLogger();

  const middleware = [
    autoRehydrate(),
    applyMiddleware(...[thunk, promise, array, logger])
  ];

  if (isDev) {
    middleware.push(
      applyMiddleware(require("redux-immutable-state-invariant").default())
    );
  }
  const reducer = combineReducers(reducers);
  const store = createStore(reducer,  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), compose(...middleware));
  storeObj["store"] = store;
  // Attach the store to the Chrome debug window
  if (global.isDebuggingInChrome) {
    // eslint-disable-line
    window.store = store;
  }
  persistStore(store, { whitelist, storage: AsyncStorage }, () => {
    const {
      Auth: { token }
    } = store && store.getState();


    if (token.length <= 0) {

      goToFirstScreen();
    } else {
    
      goHome();
    }
    // on app loading the persit store loads and we have route from here
    // startApp(store.getState().app.root);
  });
  return store;
}
