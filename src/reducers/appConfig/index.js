//@flow
export const INITIAL_STATE = {
  // serverUrl: "http://172.24.8.200:4581/" //local URL
  // serverUrl: "http://18.169.252.123:4571/"
   serverUrl:'https://api.osce.ai/'
 //serverUrl: "https://api.osce.ai/" //LIVE URL
 //serverUrl: "http://18.169.252.123:4571/" //Staging URL

};
function appConfig(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "APP_CONFIG_SUCCESS":
      return action.payload;
    default:
      return state;
  }
}
export default appConfig;
