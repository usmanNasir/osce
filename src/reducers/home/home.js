import {
  GET_COMPLAINTS_PENDING,
  GET_COMPLAINTS_FULLFILLED,
  GET_COMPLAINTS_REJECTED,
  CLEAR_COMPLAINTS
} from "../../actionTypes";
const INITIAL_STATE = {
  complaints: {},
  loader: false
};

export const Home = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_COMPLAINTS_PENDING:
      return {
        ...state,
        complaints: {},
        loader: true
      };

    case GET_COMPLAINTS_REJECTED:
      return {
        ...state,
        complaints: {},
        loader: false
      };

    case GET_COMPLAINTS_FULLFILLED:
      return {
        ...state,
        complaints: action.payload,
        loader: false
      };

    case CLEAR_COMPLAINTS:
      return  {
        ...state,
        complaints:{},
        loader:false
      }

    default:
      return state;
  }
};
