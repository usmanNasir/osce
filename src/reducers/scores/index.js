import {
  GET_SCORES_PENDING,
  GET_SCORES_FULLFILLED,
  GET_SCORES_REJECTED,
  RESET_SCORES,
  GET_INFO_REJECTED,
  GET_INFO_FULLFILLED,
  GET_INFO_PENDING,
  RESET_INFO,
  GET_CONDITIONS_REJECTED,
  GET_CONDITIONS_FULLFILLED,
  GET_CONDITIONS_PENDING,
  RESET_CONDITIONS
} from "../../actionTypes";
const INITIAL_STATE = {
  scores: {},
  loader: false,
  infoData: [],
  conditionsData: []
};

function Score(state = INITIAL_STATE, action: any) {
  switch (action.type) {
    case GET_SCORES_PENDING:
      return {
        ...state,
        scores: {},
        loader: true
      };

    case GET_SCORES_REJECTED:
      return {
        ...state,
        scores: {},
        loader: false
      };

    case GET_SCORES_FULLFILLED:
      return {
        ...state,
        scores: action.payload,
        loader: false
      };

    case RESET_SCORES:
      return {
        ...state,
        scores: {}
      };

    case GET_INFO_PENDING:
      return {
        ...state,
        infoData: [],
        loader: true
      };

    case GET_INFO_REJECTED:
      return {
        ...state,
        infoData: null,
        loader: false
      };

    case GET_INFO_FULLFILLED:
      return {
        ...state,
        infoData: action.payload,
        loader: false
      };

    case RESET_INFO:
      return {
        ...state,
        infoData: [],
        loader: false
      };

    case GET_CONDITIONS_PENDING:
      return {
        ...state,
        conditionsData: [],
        loader: true
      };

    case GET_CONDITIONS_REJECTED:
      return {
        ...state,
        conditionsData: null,
        loader: false
      };

    case GET_CONDITIONS_FULLFILLED:
      return {
        ...state,
        conditionsData: action.payload,
        loader: false
      };

    case RESET_CONDITIONS:
      return {
        ...state,
        conditionsData: [],
        loader: false
      };

    default:
      return state;
  }
}

export default Score;
