// @flow
import {
    GET_EXAMINATION_LISTING,
    TOTAL_PAGE_NO_EXAMINATION,
    UPDATE_EXAMINATION_LISTING,
    ADD_GRADING,
    ADD_GRADING_HOME,
    GET_LENGTH,
    GET_EXAMINATIONS_FULLFILLED,
    GET_EXAMINATIONS_REJECTED,
    GET_EXAMINATIONS_PENDING
  } from "../../actionTypes";
  import update from "react-addons-update";
  import _ from "lodash";
  const INITIAL_STATE = {
    examinationListing: [],
    GradingList: [],
    totalPageNo: 0,
    GradeLisitngHome: [],
    examinationsLoader:false
  };
  function ExaminationListing(state = INITIAL_STATE, action) {
    switch (action.type) {
        case GET_EXAMINATIONS_PENDING:
            return {
              ...state,
              examinationsLoader:true,
            };
        case GET_EXAMINATIONS_REJECTED:
            return {
              ...state,
              examinationsLoader:false,
            };
      case GET_EXAMINATIONS_FULLFILLED:
        return {
          ...state,
          examinationsLoader:false,
          examinationListing: action.payload
        };
      case TOTAL_PAGE_NO_EXAMINATION:
        return {
          ...state,
          totalPageNo: action.payload
        };
      case UPDATE_EXAMINATION_LISTING:
        return {
          ...state,
          examinationListing: [...state.Listing, ...action.payload]
        };
      case ADD_GRADING:
        let index = _.findIndex(state.GradingList, {
          historyId: action.payload.historyId
        });
        let { GradingList } = { ...state };
        if (index > -1) {
          return update(state, {
            GradingList: {
              [index]: {
                finalScore: { $set: action.payload.finalScore },
                Sign: { $set: action.payload.Sign }
              }
            }
          });
        } else {
          return {
            ...state,
            GradingList: [...GradingList, action.payload]
          };
        }
      case ADD_GRADING_HOME:
        return {
          ...state,
          GradeLisitngHome: action.payload
        };
  
      default:
        return state;
    }
  }
  
  export default ExaminationListing;
  