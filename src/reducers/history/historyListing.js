// @flow
import {
  GET_HISTORY_LISTING,
  TOTAL_PAGE_NO,
  UPDATE_HISTORY_LISTING,
  ADD_GRADING,
  ADD_GRADING_HOME,
  GET_LENGTH
} from "../../actionTypes";
import update from "react-addons-update";
import _ from "lodash";
const INITIAL_STATE = {
  Listing: [],
  GradingList: [],
  totalPageNo: 0,
  GradeLisitngHome: []
};
function HistoryListing(state = INITIAL_STATE, action: any) {
  switch (action.type) {
    case GET_HISTORY_LISTING:
      return {
        ...state,
        Listing: action.payload
      };
    case TOTAL_PAGE_NO:
      return {
        ...state,
        totalPageNo: action.payload
      };
    case UPDATE_HISTORY_LISTING:
      return {
        ...state,
        Listing: [...state.Listing, ...action.payload]
      };
    case ADD_GRADING:
      let index = _.findIndex(state.GradingList, {
        historyId: action.payload.historyId
      });
      let { GradingList } = { ...state };
      if (index > -1) {
        return update(state, {
          GradingList: {
            [index]: {
              finalScore: { $set: action.payload.finalScore },
              Sign: { $set: action.payload.Sign }
            }
          }
        });
      } else {
        return {
          ...state,
          GradingList: [...GradingList, action.payload]
        };
      }
    case ADD_GRADING_HOME:
      return {
        ...state,
        GradeLisitngHome: action.payload
      };

    default:
      return state;
  }
}

export default HistoryListing;
