// @flow
import {
    GET_INVESTIGATION_LISTING,
    GET_INVESTIGATIONS_FULLFILLED,
    GET_INVESTIGATIONS_PENDING,
    GET_INVESTIGATIONS_REJECTED,
    TOTAL_PAGE_NO_INVESTIGATION,
    UPDATE_INVESTIGATION_LISTING,
    ADD_GRADING,
    ADD_GRADING_HOME,
    GET_LENGTH
  } from "../../actionTypes";
  import update from "react-addons-update";
  import _ from "lodash";

  const INITIAL_STATE = {
    investigationListing: [],
    GradingList: [],
    totalPageNo: 0,
    GradeLisitngHome: []
  };
  function InvestigationListing(state = INITIAL_STATE, action) {
    switch (action.type) {
      case GET_INVESTIGATIONS_PENDING:
        return {
          ...state,
          investigationLoader:true,
        };
    case GET_INVESTIGATIONS_REJECTED:
        return {
          ...state,
          investigationLoader:false,
        };
  case GET_INVESTIGATIONS_FULLFILLED:
    return {
      ...state,
      investigationLoader:false,
      investigationListing: action.payload
    };
      case GET_INVESTIGATION_LISTING:
        return {
          ...state,
          investigationListing: action.payload
        };
      case TOTAL_PAGE_NO_INVESTIGATION:
        return {
          ...state,
          totalPageNo: action.payload
        };
      case UPDATE_INVESTIGATION_LISTING:
        return {
          ...state,
          investigationListing: [...state.Listing, ...action.payload]
        };
      case ADD_GRADING:
        let index = _.findIndex(state.GradingList, {
          historyId: action.payload.historyId
        });
        let { GradingList } = { ...state };
        if (index > -1) {
          return update(state, {
            GradingList: {
              [index]: {
                finalScore: { $set: action.payload.finalScore },
                Sign: { $set: action.payload.Sign }
              }
            }
          });
        } else {
          return {
            ...state,
            GradingList: [...GradingList, action.payload]
          };
        }
      case ADD_GRADING_HOME:
        return {
          ...state,
          GradeLisitngHome: action.payload
        };
  
      default:
        return state;
    }
  }
  
  export default InvestigationListing;
  