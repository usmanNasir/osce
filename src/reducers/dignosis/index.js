// @flow
import {
  GET_ALL_DIGNOSIS_FULLFILLED,
  GET_ALL_DIGNOSIS_PENDING,
  GET_ALL_DIGNOSIS_REJECTED,
  GET_SPECIFIC_DIGNOSIS_FULLFILLED,
  GET_SPECIFIC_DIGNOSIS_PENDING,
  GET_SPECIFIC_DIGNOSIS_REJECTED,
} from "../../actionTypes";

const INITIAL_STATE = {
  dignosisList: [],
  specficDignosis: [],
  loading: false,
  error: null,
};
function Dignosis(state = INITIAL_STATE, action: any) {
  switch (action.type) {
    case GET_ALL_DIGNOSIS_PENDING:
      return {
        ...state,
        loading: true,
      };
    case GET_ALL_DIGNOSIS_REJECTED:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    case GET_ALL_DIGNOSIS_FULLFILLED:
      return {
        ...state,
        loading: false,
        dignosisList: action.payload.data,
      };
    case GET_SPECIFIC_DIGNOSIS_PENDING:
      return {
        ...state,
        loading: true,
      };
    case GET_SPECIFIC_DIGNOSIS_REJECTED:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    case GET_SPECIFIC_DIGNOSIS_FULLFILLED:
      return {
        ...state,
        loading: false,
        specficDignosis: action.payload.data,
      };

    default:
      return state;
  }
}

export default Dignosis;
