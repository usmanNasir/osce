import {
  GET_LAST_COMPLAINTS_FULLFILLED,
  GET_LAST_COMPLAINTS_REJECTED,
  GET_LAST_COMPLAINTS_PENDING,
  GET_NEWS_CASE_PENDING,
  GET_NEWS_CASE_REJECTED,
  GET_NEWS_CASE_FULLFILLED,
  GET_CHALLENGE_PENDING,
  GET_CHALLENGE_REJECTED,
  GET_CHALLENGE_FULLFILLED,
  GET_HEADING_CONSTANTS_PENDING,
  GET_HEADING_CONSTANTS_REJECTED,
  GET_HEADING_CONSTANTS_FULLFILLED,
  CREATE_FEEDBACK_FULLFILLED,
  CREATE_FEEDBACK_PENDING,
  CREATE_FEEDBACK_REJECTED,
  CREATE_LAST_COMPLAINT_PENDING,
  CREATE_LAST_COMPLAINT_REJECTED,
  CREATE_LAST_COMPLAINT_FULLFILLED,
  GET_HOME_SCREEN_DATA_PENDING,
  GET_HOME_SCREEN_DATA_REJECTED,
  GET_HOME_SCREEN_DATA_FULLFILLED,
  
} from "../../actionTypes";

const INITIAL_STATE = {
  lastComplaint: {},
  newsCase: {},
  challenge: {},
  headingConstants: {},
  loader: false,
  homeContent:[],
};

const homeScreenReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_HOME_SCREEN_DATA_PENDING:
      return {
        ...state,
        loader: true,
      };

    case GET_HOME_SCREEN_DATA_REJECTED:
      return {
        ...state,
        loader: false,
      };
    case GET_HOME_SCREEN_DATA_FULLFILLED:
      return {
        ...state,
        homeContent: action.payload.HomeContent,
        headingConstants:action.payload.headingConstants,
        loader: false,
      };
    case GET_LAST_COMPLAINTS_PENDING:
      return {
        ...state,
        lastComplaint: {},
        loader: true,
      };

    case GET_LAST_COMPLAINTS_REJECTED:
      return {
        ...state,
        lastComplaint: {},
        loader: false,
      };

    case GET_LAST_COMPLAINTS_FULLFILLED:
      return {
        ...state,
        lastComplaint: action.payload,
        loader: false,
      };
    case GET_NEWS_CASE_PENDING:
      return {
        ...state,
        newsCase: {},
        loader: true,
      };

    case GET_NEWS_CASE_REJECTED:
      return {
        ...state,
        newsCase: {},
        loader: false,
      };

    case GET_NEWS_CASE_FULLFILLED:
      return {
        ...state,
        newsCase: action.payload,
        loader: false,
      };
    case GET_CHALLENGE_PENDING:
      return {
        ...state,
        challenge: {},
        loader: true,
      };

    case GET_CHALLENGE_REJECTED:
      return {
        ...state,
        challenge: {},
        loader: false,
      };

    case GET_CHALLENGE_FULLFILLED:
      return {
        ...state,
        challenge: action.payload,
        loader: false,
      };
    case GET_HEADING_CONSTANTS_PENDING:
      return {
        ...state,
        headingConstants: {},
        loader: true,
      };

    case GET_HEADING_CONSTANTS_REJECTED:
      return {
        ...state,
        headingConstants: {},
        loader: false,
      };

    case GET_HEADING_CONSTANTS_FULLFILLED:
      return {
        ...state,
        headingConstants: action.payload,
        loader: false,
      };
    case CREATE_FEEDBACK_PENDING:
      return {
        ...state,
        feedback: {},
        loader: true,
      };

    case CREATE_FEEDBACK_REJECTED:
      return {
        ...state,
        feedback: {},
        loader: false,
      };

    case CREATE_FEEDBACK_FULLFILLED:
      return {
        ...state,
        feedback: action.payload,
        loader: false,
      };
    case CREATE_LAST_COMPLAINT_PENDING:
      return {
        ...state,
        lastComplaint: {},
        loader: true,
      };

    case CREATE_LAST_COMPLAINT_REJECTED:
      return {
        ...state,
        lastComplaint: {},
        loader: false,
      };

    case CREATE_LAST_COMPLAINT_FULLFILLED:
      return {
        ...state,
        lastComplaint: action.payload,
        loader: false,
      };
    default:
      return state;
  }
};

export default homeScreenReducer;
