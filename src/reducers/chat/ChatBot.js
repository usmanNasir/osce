// @flow
import {
  APPEND_MESAGE,
    APPEND_MESSAGE_INVESTIGATION,
    APPEND_MESSAGE_EXAMINATION,
    SESSION_ID,
    RESET_CHAT_BOT_DATA,
    RESET_CHAT_BOT_DATA_INVESTIGATION,
    RESET_CHAT_BOT_DATA_EXAMINATION
} from "../../actionTypes";

const INITIAL_STATE = {
  messagesListing: [],
  examMessageListing:[],
  examLastMessage:{},
  InvestigationMessageListing:[],
  InvestigationLastMessage:{},
  lastMessage: {},
  sessionId: null
};
function ChatBot(state = INITIAL_STATE, action) {
  switch (action.type) {
    case APPEND_MESAGE:
      console.log("INSIDE REDUCER",JSON.stringify(action))
      return {
        ...state,
        messagesListing: [...action.payload, ...state.messagesListing],
        lastMessage: action.payload
      };
    case APPEND_MESSAGE_EXAMINATION:
      return {
        ...state,
        examMessageListing: [...action.payload, ...state.examMessageListing],
        examLastMessage: action.payload
      };
    case APPEND_MESSAGE_INVESTIGATION:
      return {
        ...state,
        InvestigationMessageListing: [...action.payload, ...state.InvestigationMessageListing],
        InvestigationLastMessage: action.payload
      };
    case SESSION_ID:
      return {
        ...state,
        sessionId: action.payload
      };
    case RESET_CHAT_BOT_DATA_EXAMINATION:
      return {
        ...state,
        examMessageListing: [],
        examLastMessage:null
      };
    case RESET_CHAT_BOT_DATA_INVESTIGATION:
      return {
        ...state,
        InvestigationMessageListing: []
      };
    case RESET_CHAT_BOT_DATA:
      return {
        ...state,
        sessionId: null,
        messagesListing: []
      };
    default:
      return state;
  }
}
export default ChatBot;
