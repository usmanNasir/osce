// @flow
import {
    GET_APPSTATUS_REJECTED,
    GET_APPSTATUS_PENDING,
    GET_APPSTATUS_FULLFILLED,
  } from "../../actionTypes";
  
  const INITIAL_STATE = {
    appStatus: [],
    loading: false,
    error: null,
  };
  function AppStatus(state = INITIAL_STATE, action) {
    switch (action.type) {
      case GET_APPSTATUS_PENDING:
        return {
          ...state,
          loading: true,
        };
      case GET_APPSTATUS_REJECTED:
        return {
          ...state,
          loading: false,
          error: action.payload.error,
        };
      case GET_APPSTATUS_FULLFILLED:
        return {
          ...state,
          loading: false,
          appStatus: action.payload.data,
        };
      default:
        return state;
    }
  }
  
  export default AppStatus;
  