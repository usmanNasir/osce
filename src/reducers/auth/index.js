import {
  AUTH_PENDING,
  AUTH_FULLFILLED,
  AUTH_REJECTED,
  RESET_AUTH,
  AUTH_LOGOUT,
  GOOGLE_AUTH,
  AUTH_UPDATE_PENDING,
  AUTH_UPDATE_REJECTED,
  AUTH_UPDATE_FULLFILLED,
  AUTH_UPDATE_UNIVERSITY_PENDING,
  AUTH_UPDATE_UNIVERSITY_FULLFILLED,
  AUTH_UPDATE_UNIVERSITY_REJECTED,
} from "../../actionTypes";
const INITIAL_STATE = {
  authData: null,
  error: null,
  loader: false,
  token: "",
  userInfo: ""
};
function Auth(state = INITIAL_STATE, action: any) {
  switch (action.type) {
    case AUTH_PENDING:
      return {
        ...state,
        authdata: null,
        error: null,
        loader: true
      };
    case AUTH_FULLFILLED:
      return {
        ...state,
        authData: action.payload.data,
        token: action.payload.data ? action.payload.data.token : "",
        loader: false
      };
    case AUTH_REJECTED:
      return {
        ...state,
        error: action.payload,
        loader: false
      };
      case AUTH_UPDATE_PENDING:
        return {
          ...state,
          error:null,
          authdata: null,
          loader: true
        };
      case AUTH_UPDATE_FULLFILLED:
        return {
          ...state,
          authData: action.payload.data,
          token: action.payload.data ? action.payload.data.token : "",
          loader: false
        };
      case AUTH_UPDATE_REJECTED:
        return {
          ...state,
          error: action.payload,
          loader: false
        };
        case AUTH_UPDATE_UNIVERSITY_PENDING:
          return {
            ...state,
            error:null,
            authdata: null,
            loader: true
          };
        case AUTH_UPDATE_UNIVERSITY_FULLFILLED:
          return {
            ...state,
            authData: action.payload.data,
            loader: false
          };
        case AUTH_UPDATE_UNIVERSITY_REJECTED:
          return {
            ...state,
            error: action.payload,
            loader: false
          };
    case AUTH_LOGOUT:
      return {
        ...state,
        authData: null,
        error: null,
        loader: false,
        token: "",
        userInfo: null
      };
    case RESET_AUTH:
      return {
        ...state,
        authData: null,
        error: null,
        loader: false
      };
    case GOOGLE_AUTH:
      return {
        ...state,
        authData: action.payload,
        token: action.payload ? action.payload.idToken : "",
        loader: false
      };
    default:
      return state;
  }
}

export default Auth;
