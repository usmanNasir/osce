import { TRIGGER_SWITCH } from "../../actionTypes";

const initialState = {
  currentValue: false
};

const switchReducer = (state = initialState, action) => {
  switch (action.type) {
    case TRIGGER_SWITCH:
      return {
        currentValue: action.currentValue
      };
    default:
      return state;
  }
};

export default switchReducer;
