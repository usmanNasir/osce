// @flow
import {
  HISTORY_LOADER_VISIBILITY,
  PAGINATION_LOADER_HISTORY,
  CHAT_RESPONSE_LOADER,
  CHAT_RESPONSE_LOADER_EXAMINATION,
  CHAT_RESPONSE_LOADER_INVESTIGATION,
} from "../../actionTypes";
const INITIAL_STATE = {
  HistoryListingLoader: false,
  PaginationListingLoader: false,
  ChatResponsLoader: false,
  ChatResponsLoaderExamination: false,
  ChatResponsLoaderInvestigation: false,
};
function Loaders(state = INITIAL_STATE, action: any) {
  switch (action.type) {
    case HISTORY_LOADER_VISIBILITY:
      return {
        ...state,
        HistoryListingLoader: action.payload,
      };
    case PAGINATION_LOADER_HISTORY:
      return {
        ...state,
        PaginationListingLoader: action.payload,
      };
    case CHAT_RESPONSE_LOADER:
      return {
        ...state,
        ChatResponsLoader: action.payload,
      };
    case CHAT_RESPONSE_LOADER_EXAMINATION:
      return {
        ...state,
        ChatResponsLoaderExamination: action.payload,
      };
    case CHAT_RESPONSE_LOADER_INVESTIGATION:
      return {
        ...state,
        ChatResponsLoaderInvestigation: action.payload,
      };
    default:
      return state;
  }
}

export default Loaders;
