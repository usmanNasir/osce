import {
  CREATE_FEEDBACK_PENDING,
  CREATE_FEEDBACK_REJECTED,
  CREATE_FEEDBACK_FULLFILLED,
} from "../../actionTypes";

const initialState = {
  feedbackLoader: false,
};

const feedbackScreenReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_FEEDBACK_PENDING:
      return {
        ...state,
        feedbackLoader: true,
      };
    case CREATE_FEEDBACK_FULLFILLED:
      return {
        ...state,
        feedbackLoader: false,
      };
    case CREATE_FEEDBACK_REJECTED:
      return {
        ...state,
        feedbackLoader: false,
      };
    default:
      return state;
  }
};

export default feedbackScreenReducer;
