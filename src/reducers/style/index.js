import { TRIGGER_PADDING_CHANGE,TRIGGER_MARGIN_CHANGE,TRIGGER_RESET_CHANGE_TRIGGER,TRIGGER_BUTTON_CHANGE,TRIGGER_RESET_BUTTON_CHANGE,TRIGGER_HOME_REFRESH_CHANGE } from "../../actionTypes";

const initialState = {
  changePadding: false,
  changeMargin:false,
  changeButton:false,
  homeRefresh:false

};

const styleReducer = (state = initialState, action) => {
  switch (action.type) {
    case TRIGGER_PADDING_CHANGE:
      return {
        ...state,
        changePadding: true
      };
    case TRIGGER_MARGIN_CHANGE:
      return {
        ...state,
        changeMargin:true
      }
      case TRIGGER_RESET_CHANGE_TRIGGER:
      return {
        ...state,
        changePadding:false,
        changeMargin:false
      }
      case TRIGGER_BUTTON_CHANGE:
        return {
          ...state,
          changeButton:true
        }
      case TRIGGER_RESET_BUTTON_CHANGE:
        return {
          ...state,
          changeButton:false
        }
    case TRIGGER_HOME_REFRESH_CHANGE:
      return {
        ...state,
        homeRefresh: action.payload
      }
    default:
      return state;
  }
};

export default styleReducer;
