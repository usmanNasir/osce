import React, { Component } from "react";
import {
  View,
  Text,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  LayoutAnimation,
  FlatList,
  NativeModules,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import constants from "../../constants";
import * as AppAction from "../../actions";
import Card from "../../components/common/Card";
import { MixPanelContext } from "../../utilities/MixPanel";
import { Activity, MarkScheme_Clicked } from "../../constants/Events";
const { UIManager } = NativeModules;

class InnerListItem extends Component {
  static contextType = MixPanelContext;
  constructor(props) {
    UIManager.setLayoutAnimationEnabledExperimental &&
      UIManager.setLayoutAnimationEnabledExperimental(true);
    super(props);
    this.state = {
      isExpanded: false,
      disabled: true
    };
  }
  UNSAFE_componentWillUpdate() {
    LayoutAnimation.easeInEaseOut();
  }
  goToInfoPage(item) {
    const { componentId } = this.props.parentProps;
    this.context.mixPanel.track(Activity.userMarksSchemeInfoActivity);
    if (item.clickable) {
      this.props.AppAction.getInfo(item._id, item.accepted);
      Navigation.push(componentId, {
        component: {
          name: "Info",
          passProps: { ...this.props.parentProps,...this.props },
          options: {
            topBar: {
              visible: false
            },
            bottomTabs: {
              // drawBehind: Platform.OS === "android" ? true : false,
              visible: false,
            },
          }
        }
      });
    }
  }
  camelCase(str) {
    return str
      .replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
        return index == 0 ? word.toUpperCase() : word.toLowerCase();
      })
      .replace(/\s+/g, " ");
  }
  render() {
    let { listData, innerHeaderName, tagValue, tagColor } = this.props;
    let { key,name } = this.props.parentProps;
    return (
      <Card
        style={key === true ? styles.cardWrapper : styles.cardWrapperDisabled}
      >
        <Text
          style={key === true ? styles.testHeading : styles.testHeadingDisabled}
        >
          {innerHeaderName.trim()}
        </Text>
        {!this.state.isExpanded ? (
          <View />
        ) : (
          this.context.mixPanel.track(Activity.userMarksSchemeExpandedActivity),
          <FlatList
            data={listData}
            keyExtractor={(item, index) => item.toString() + index.toString()}
            renderItem={this.renderItem}
          />
        )}
        <TouchableWithoutFeedback
          onPress={() => { this.setSelectedOption(),
            this.context.mixPanel.track(MarkScheme_Clicked,{ComplaintType:name,Heading:innerHeaderName})
          }}
          disabled={key === false ? this.state.disabled : !this.state.disabled}
        >
          <View style={styles.horizontalView}>
            <Text
              style={[
                styles.tagStyle,
                {
                  backgroundColor: key === true ? tagColor : "#95989A"
                }
              ]}
            >
              {tagValue.toLowerCase().trim()}
            </Text>
            <View style={{ flex: 1 }} />
            {key === false ? (
              <Image
                style={styles.lockIocn}
                source={require("../../assets/img/lock.png")}
              />
            ) : null}
            {key === true ?
                <Image
                    style={styles.checkedIcon}
                    source={
                      this.state.isExpanded
                          ? constants.Images.Common.ic_arrow_up
                          : constants.Images.Common.ic_arrow_down
                    }
                /> :  <Image
                    style={styles.checkedIcon}
                    source={
                      require('../../assets/img/ic_arrow_down_iap.png')
                    }
                />}
          </View>
        </TouchableWithoutFeedback>
      </Card>
    );
  }
  renderItem = ({ item }) => {
    const blueLink = item.clickable
      ? {
          color: constants.Colors.link,
          textDecorationLine: "underline"
        }
      : { color: constants.Colors.Black };
    return (
      <View style={styles.itemWrapper}>
        <TouchableOpacity
          style={[blueLink, { flex: 0.8 }]}
          onPress={this.goToInfoPage.bind(this, item)}
        >
          <Text style={[styles.textNormal, blueLink]}>
            {this.camelCase(item.title.trim())}
          </Text>
        </TouchableOpacity>
        <View style={{ flex: 0.2, alignItems: "flex-end" }}>
          <Image
            resizeMode={"contain"}
            source={
              item.accepted
                ? constants.Images.Common.checkIcon
                : constants.Images.Common.crossIcon
            }
          />
        </View>
      </View>
    );
  };
  setSelectedOption() {
    this.setState({ isExpanded: !this.state.isExpanded });
  }
}
const styles = StyleSheet.create({
  checkedIcon: { height: 20, width: 20 },
  lockIocn: { height: 40, width: 40, right: 10 },
  horizontalView: {
    flexDirection: "row",
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center",
  },
  cardWrapper: {
    flex: 1,
    padding: moderateScale(15),
    paddingTop: moderateScale(20),
    paddingBottom: moderateScale(16),
    paddingHorizontal: moderateScale(20),
    marginHorizontal: moderateScale(2),
  },
  cardWrapperDisabled: {
    flex: 1,
    padding: moderateScale(15),
    paddingTop: moderateScale(20),
    paddingBottom: moderateScale(16),
    paddingHorizontal: moderateScale(20),
    backgroundColor: "rgb(195, 195, 195)"
  },
  testHeading: {
    color: constants.Colors.Black,
    fontSize: moderateScale(24),
    ...constants.Fonts.Medium,
    marginBottom: moderateScale(20),
    textTransform: "capitalize"
  },
  testHeadingDisabled: {
    color: "#606060",
    fontSize: moderateScale(24),
    ...constants.Fonts.Medium,
    marginBottom: moderateScale(20),
    textTransform: "capitalize"
  },
  textNormal: {
    fontSize: moderateScale(20),
    ...constants.Fonts.Regular
  },
  tagStyle: {
    padding: moderateScale(5),
    color: constants.Colors.White,
    fontSize: moderateScale(20),
    ...constants.Fonts.Medium
  },
  itemWrapper: {
    flex: 1,
    flexDirection: "row",
     marginBottom: moderateScale(16),
    alignItems: "center",
  }
});
function mapStateToProps(state) {
  return {
    scoreData: state.Score.scores
  };
}
const mapDispatchToProps = dispatch => ({
  AppAction: bindActionCreators(AppAction, dispatch),
  dispatch
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InnerListItem);
