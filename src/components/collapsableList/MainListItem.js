/*Author: Gurtej singh
 *Date:19 july 2019
 *Purpose: collapsible sets
 */
import React, { Component } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import constants from "../../constants";
import InnerListItem from "./InnerListItem";
class MainListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  // MAIN RENDERER METHOD
  render() {
    let { item } = this.props;
    return (
      <View>
        {/* <View style={styles.greyLine} /> */}
        {/* <Text style={styles.testHeading}>{'item.heading.trim()'}</Text> */}
        {this.renderCardRow(item)}
      </View>
    );
  }
  // INNER FLATLIST FOR INNER COMPONENTS LISTING
  // renderMainInnerList(obj) {
  //   return (
  //     <FlatList
  //       data={obj}
  //       extraData={this.state}
  //       keyExtractor={(item, index) => item.toString() + index.toString()}
  //       renderItem={(item, index) => this.renderCardRow(item, index)}
  //     />
  //   );
  // }
  // THE INNER MOST ITEMS (ITS EXPANDABLE)
  renderCardRow = item => {
    return (
      <InnerListItem
        state={this.props}
        listData={item.scores}
        innerHeaderName={item.subHeading}
        tagValue={item.percent}
        tagColor={item.colour}
        parentProps={this.props.parentProps}
      />
    );
  };
  setSelectedOption() {
    this.setState({ isExpanded: !this.state.isExpanded });
  }
}
const styles = StyleSheet.create({
  greyLine: {
    marginVertical: moderateScale(20),
    height: moderateScale(2),
    flex: 1,
    backgroundColor: constants.Colors.placehoder
  },
  checkedIcon: { height: 20, width: 20, marginHorizontal: 10 },
  horizontalView: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  testHeading: {
    color: constants.Colors.Black,
    fontSize: moderateScale(32),
    ...constants.Fonts.Bold,
    marginBottom: moderateScale(10)
  },
  textNormal: {
    color: constants.Colors.Black,
    fontSize: moderateScale(13),
    paddingVertical: 5
  }
});
export default MainListItem;
