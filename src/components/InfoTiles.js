import React, { Component } from "react";
import { View, Text, StyleSheet, Dimensions, ScrollView } from "react-native";

import HTML from "react-native-render-html";
import _ from "lodash";

import ImageLoad from "./ImageLoad";

import { moderateScale } from "../helpers/ResponsiveFonts";
import Constants from "../constants";
import Card from "./common/Card";

const DEFAULT_PROPS = {
  tagsStyles: {
    p: {
      color: Constants.Colors.Black,
      marginTop: moderateScale(20),
      fontSize: moderateScale(15),
      lineHeight: moderateScale(29)
    },
    font: { color: Constants.Colors.redText }
  }
};

class InfoTiles extends Component {
  setHeight(icon) {
    let value = 70;
    if (icon && icon.includes("cross")) {
      value = 30;
    }
    if (icon && icon.includes("ic_checked")) {
      value = 30;
    }
    return value;
  }

  removeBRTag(str) {
    let strdate = str.split("<br />");
    return strdate.join("");
  }

  render() {
    return (
      <View>
        {_.map(this.props.data, (data, index) => {
          return (
            <Card style={styles.conversationBox} key={index}>
              <Text style={styles.converText}>{data.title}</Text>

              <HTML
                html={this.removeBRTag(data.text)}
                {...DEFAULT_PROPS}
                imagesMaxWidth={Dimensions.get("window").width}
              />

              <View
                style={{
                  alignItems: "center",
                  marginTop: moderateScale(40)
                }}
              >
                <ImageLoad
                  isShowActivity={false}
                  style={{
                    height: moderateScale(this.setHeight(data.iconUlr)),
                    width: moderateScale(this.setHeight(data.iconUlr))
                  }}
                  placeholderStyle={{
                    height: moderateScale(this.setHeight(data.iconUlr)),
                    width: moderateScale(this.setHeight(data.iconUlr))
                  }}
                  loadingStyle={{ size: "small", color: "blak" }}
                  source={{ uri: data.iconUlr }}
                  placeholderSource={{
                    uri: data.thumbnailUrl ? data.thumbnailUrl : null
                  }}
                  resizeMode="contain"
                />
              </View>
            </Card>
          );
        })}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  conversationBox: {
    flex: 1,
    padding: moderateScale(22),
    marginVertical: moderateScale(10),
    // new changes
    padding: moderateScale(15),
    paddingTop: moderateScale(20),
    paddingBottom: moderateScale(16),
    paddingHorizontal: moderateScale(20),
    marginHorizontal: moderateScale(2),
  },
  converText: {
    color: Constants.Colors.Black,
    fontSize: moderateScale(22),
    fontWeight: "600",
    textTransform: "capitalize"
  }
});

export default InfoTiles;
