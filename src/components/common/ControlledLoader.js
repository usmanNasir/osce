import React  from "react";
import {Modal,View,ActivityIndicator} from 'react-native';
import {moderateScale} from "../../helpers/ResponsiveFonts";
import constants from "../../constants";


const ControlledLoader = ({showLoader})=>{
    return (
                <Modal
                    animationType="none"
                    transparent={true}
                    style={{ marginTop: moderateScale(20),backgroundColor:'transparent' }}
                    visible={showLoader}
                >
                    <View style={{flex:1,backgroundColor:'transparent',justifyContent:'center',alignItems:"center"}}>
                        <ActivityIndicator color={constants.Colors.Black} size="large" />

                    </View>
                </Modal>
            )

};
export default  ControlledLoader;
