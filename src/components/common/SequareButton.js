import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  Image,
} from "react-native";

import Constants from "../../constants";

import { moderateScale } from "../../helpers/ResponsiveFonts";

const SequareButton = (props) => {
  let {
    text,
    loading,
    style,
    loaderColor,
    onPress,
    showIcon,
    iconSource,
  } = props;
  return (
    <TouchableOpacity
      style={[styles.container, style]}
      onPress={loading ? null : onPress}
    >
      {loading ? (
        <ActivityIndicator color={loaderColor} />
      ) : showIcon ? (
        <View style={styles.iconContainer}>
          <View style={style.imageContainer}>
            <Image
              resizeMode="contain"
              style={styles.icon}
              source={iconSource}
            />
          </View>
          <View style={styles.textContainer}>
            <Text
              style={{
                width: "100%",
                fontSize: moderateScale(18),
                color: Constants.Colors.White,
                fontWeight: "bold",
              }}
            >
              {text}
            </Text>
          </View>
        </View>
      ) : (
        <View>
          <Text
            style={{
              width: "100%",
              fontSize: moderateScale(18),
              color: Constants.Colors.White,
              fontWeight: "bold",
            }}
          >
            {text}
          </Text>
        </View>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: moderateScale(15),
    borderRadius: moderateScale(10),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  iconContainer: {
    flexDirection: "row",
    width:'100%',
     alignItems:'center'
  },
  icon: {
    width: 30,
    height: 30,
    // paddingHorizontal:16,
  },

  textContainer:{
    paddingLeft:10
  }
});

export default SequareButton;
