import React from "react";
import { Linking } from "react-native";

import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  Modal,
} from "react-native";
import constants from "../../constants";

export default class inAppUpdateModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      needUpdate: false,
    };
  }
  handlePress = () => {
    if (Platform.OS === "ios") {
      Linking.openURL("https://apps.apple.com/pk/app/osce-ai/id1471522463");
    } else {
      Linking.openURL("https://play.google.com/store/apps/details?id=com.osce");
    }
  };

  render() {
    return (
      <Modal style={{ flex: 1, backgroundColor: constants.Colors.transparent }}>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems:'center',
          }}
        >
          <View style={{ flexDirection: "row",alignItems:'center',width:'90%',marginBottom:5,justifyContent:'center' }}>
            <Image
              style={{ width: 80, height: 80 }}
              source={require("../../assets/img/login-logo/icon.png")}
            />
            <Text style={{ fontSize: 24, fontWeight: "bold" }}>
              OSCE.AI needs an update
            </Text>
          </View>
          <Text style={{ fontSize: 16, fontWeight: "600",marginHorizontal:20 }}>
            To use this App Download the latest Version
          </Text>

          {/*<Text style={{fontSize:18,fontWeight:'600',textAlign:'center'}}>Sorry, we're having some trouble.Try reloading the page.</Text>*/}
          <TouchableOpacity
            onPress={() => this.handlePress()}
            style={{
              backgroundColor: 'green',
              borderWidth: 1,
              borderRadius: 10,
              width: "90%",
              height: 45,
              justifyContent: "center",
              borderColor: 'green',
              alignItems: "center",
              marginTop: 30,
            }}
          >
            <Text
              style={{
                color: constants.Colors.White,
                fontSize: 18,
                fontWeight: "700",
                padding: 10,
              }}
            >
              UPDATE
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{marginBottom:40,justifyContent:'center',alignItems:'center'}}>
                <Image style={{width:50,height:50}} source={Platform.OS === "android" ? require('../../assets/img/login-logo/googlePlay.png') : require('../../assets/img/login-logo/appStore.png')}/>
                {Platform.OS === 'ios' &&  <Text style={{fontSize:18,fontWeight:'500',color:'grey'}}>AppStore</Text>}
        </View>

      </Modal>
    );
  }
}
