import React from "react";
import { ActivityIndicator } from "react-native";

import { View, Text, TouchableOpacity } from "react-native";
import constants from "../../constants";
import ControlledLoader from "./ControlledLoader";

export default class Reload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }
  render() {
    const { onPress } = this.props;

    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          marginHorizontal: 20,
        }}
      >
        {/*<Text style={{fontSize:18,fontWeight:'600',textAlign:'center'}}>Sorry, we're having some trouble.Try reloading the page.</Text>*/}
        <TouchableOpacity
          onPress={() => {
            this.setState({ isLoading: !this.state.isLoading });
            onPress();
          }}
          style={{
            backgroundColor: constants.Colors.blue,
            borderWidth: 1,
            borderRadius: 10,
            width: 120,
            height: 45,
            justifyContent: "center",
            borderColor: constants.Colors.blue,
            alignItems: "center",
            marginTop: 10,
          }}
        >
          <Text style={{
                      color: constants.Colors.White,
                      fontSize: 18,
                      fontWeight: "700",
                        padding:10
          }}>
            RELOAD
          </Text>
        </TouchableOpacity>
        <View style={{ marginTop: 10 }}>
          {this.state.isLoading && <ControlledLoader showLoader={this.state.isLoading}/>}
        </View>
      </View>
    );
  }
}
