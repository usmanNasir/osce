// @flow

import React from "react";

import { ActivityIndicator } from "react-native";
import constant from "../../constants";
export default class Loader extends React.Component {
  render() {
    return (
      <ActivityIndicator
        style={{
          flex: 1
        }}
        size="large"
        color={constant.Colors.DarkBlack}
      />
    );
  }
}
