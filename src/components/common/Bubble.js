import React from "react";
import { View, Image, Text } from "react-native";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import constants from "../../constants";
const Bubble = props => {
  let { text, imageUri, color, type } = props;
  return (
    <View style={{ maxWidth: "70%", marginLeft: moderateScale(10) }}>
      <View style={styles.chatMainWrapper}>
        {type == 2 ? <Image source={imageUri} /> : null}
        <View style={[styles.containerStyle]}>
          <View
            style={[styles.chatBubbleTxtWrapper, { backgroundColor: color }]}
          >
            <Text style={styles.text}>{text}</Text>
          </View>
        </View>
        {type == 1 ? <Image source={imageUri} /> : null}
      </View>
    </View>
  );
};

const styles = {
  containerStyle: {
    alignItems: "flex-start",
    justifyContent: "flex-start",
    borderColor: constants.Colors.transparent,
    borderBottomWidth: moderateScale(0),
    shadowColor: constants.Colors.DarkBlack,
    shadowOffset: { width: 0, height: moderateScale(2) },
    shadowOpacity: moderateScale(0.1),
    shadowRadius: moderateScale(2),
    elevation: moderateScale(1)
  },
  chatMainWrapper: {
    flexDirection: "row",
    margin: moderateScale(5),
    alignItems: "flex-end",
    justifyContent: "flex-end"
  },
  chatBubbleTxtWrapper: {
    padding: moderateScale(10),
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    color: constants.Colors.ChatTextColor,
    ...constants.Fonts.Light,
    fontSize: moderateScale(18)
  }
};

export default Bubble;
