// @flow

import React from "react";

import { View, Text } from "react-native";

export default class NoRecord extends React.Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          marginTop: 50
        }}
      >
        <Text>No Record found</Text>
      </View>
    );
  }
}
