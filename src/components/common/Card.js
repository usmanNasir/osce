import React from "react";
import { View } from "react-native";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import constants from "../../constants";
const Card = props => {
  return (
    <View style={[styles.containerStyle, props.style]}>{props.children}</View>
  );
};

const styles = {
  containerStyle: {
    flex: 1,

    borderRadius: moderateScale(15),
    borderColor: constants.Colors.shadownColor,
    borderWidth: moderateScale(0),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: moderateScale(2) },
    shadowOpacity: moderateScale(0.3),
    shadowRadius: moderateScale(2),
    elevation: moderateScale(3),
    marginBottom: moderateScale(16),
    paddingBottom: moderateScale(16),
    backgroundColor: "white"
  }
};

export default Card;
