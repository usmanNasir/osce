import React from "react";
import { View, StyleSheet, TextInput, Text } from "react-native";

import { moderateScale } from "../../helpers/ResponsiveFonts";
import Constants from "../../constants";

const TextField = props => {
  let {
    onChange,
    keyboardType,
    stateVariable,
    type,
    value,
    placeholderValue,
    errors
  } = props;
  return (
    <View style={styles.field}>
      <TextInput
        style={{
          color: Constants.Colors.gunPowderShade,
          borderBottomWidth: moderateScale(1),
          height: moderateScale(40),
          borderBottomColor:
            errors[stateVariable] || errors.both
              ? Constants.Colors.errorRed
              : Constants.Colors.darkGrey2
        }}
        type={type}
        keyboardType={keyboardType ? keyboardType : "default"}
        onChangeText={text => onChange(stateVariable, text)}
        value={value}
        placeholder={placeholderValue}
        secureTextEntry={type === "password" ? true : false}
      />

      {errors && errors[stateVariable] ? (
        <View style={{ marginTop: moderateScale(2) }}>
          <Text
            style={{
              fontSize: moderateScale(13),
              color: Constants.Colors.errorRed
            }}
          >
            {errors[stateVariable]}
          </Text>
        </View>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  field: {
    marginTop: moderateScale(10),
    marginBottom: moderateScale(10)
  }
});

export default TextField;
