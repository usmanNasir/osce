import React from "react";
import { View, Text } from "react-native";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import constants from "../../constants";
const SystemMsg = props => {
  let { text } = props;
  return (
    // <View style={styles.containerStyle}>
    <View style={styles.MsgMainWrapper}>
      <View style={styles.chatTxtWrapper}>
        <Text style={styles.text} selectable>
          {text}
        </Text>
      </View>
    </View>
    // </View>
  );
};

const styles = {
  containerStyle: {
    borderRadius: moderateScale(30),
    borderColor: constants.Colors.transparent,
    borderBottomWidth: moderateScale(0),
    shadowColor: constants.Colors.DarkBlack,
    shadowOffset: { width: 0, height: moderateScale(2) },
    shadowOpacity: moderateScale(0.1),
    shadowRadius: moderateScale(2),
    elevation: moderateScale(3),
    backgroundColor: constants.Colors.White,
    margin: moderateScale(20)
  },
  MsgMainWrapper: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  chatTxtWrapper: {
    flex: 1,
    padding: moderateScale(20),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: constants.Colors.lightYellow,
    margin: moderateScale(20),
    borderColor: constants.Colors.transparent,
    borderBottomWidth: moderateScale(0),
    shadowColor: constants.Colors.DarkBlack,
    shadowOffset: { width: 0, height: moderateScale(4) },
    shadowOpacity: moderateScale(0.1),
    shadowRadius: moderateScale(2),
    elevation: moderateScale(3)
  },
  text: {
    color: constants.Colors.ChatTextColor,
    ...constants.Fonts.Light,
    fontSize: moderateScale(17)
  }
};

export default SystemMsg;
