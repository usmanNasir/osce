


const MixPanelEvents = {
    Account_Activation:'Account_Activation',
    App_Launch:'App_Launch',
    User_Login:'User_Login',
    User_Logout:'User_Logout',
    Share_News_Click:'Share_News_Click',
    Share_Score_Click:'Share_Score_Click',
    Challenge_Start_Click:'Challenge_Start_Click',
    Challenge_Retry_Click:'Challenge_Retry_Click',
    Challenge_Started:'Challenge_Started',
    Challenge_Completed:'Challenge_Completed',
    Challenge_Left_UnAttempted:'Challenge_Left_UnAttempted',
    Case_Completed:'Case_Completed',
    Case_Left_UnCompleted:'Case_Left_UnCompleted',
    Case_Type_Clicked:'Case_Type_Clicked',
    MarkScheme_Clicked:'MarkScheme_Clicked',
    News_Card_Clicked:'News_Card_Clicked',
    Time_Spent_ChatBot:'Time_Spent_ChatBot',
    Time_Spent_Examination:'Time_Spent_Examination',
    Time_Spent_Investigation:'Time_Spent_Investigation',
    Time_Spent_Dignosis:'Time_Spent_Dignosis',
    Time_Spent_MarksScheme:'Time_Spent_MarksScheme',
    Time_Spent_Conditions:'Time_Spent_Conditions',
    Attempted_Notification_Clicked:'User Attempted Challenge Notification Clicked',
    Not_Attempted_Notification_Clicked:'User Not Attempted Challenge Notification Clicked',
    Weekly_Challenge_Notification_Clicked:'Weekly Challenge Notification Clicked',
    Activity:{
        userLoginActivity:'User Login Activity',
        userSignUpActivity:'User SignUp Activity',
        userKeepLearningActivity:'User KeepLearning Activity',
        userHistoryChallengeActivity:'User Weekly History Challenge Activity',
        userLeaderBoardActivity:'User LeaderBoard Activity',
        userBlogActivity:'User Blog Activity',
        userFeedbackActivity:'User feedback Activity',
        userBotComplaintActivity:'User Bot Complaint Activity',
        userChatActivity:'User Chat Activity',
        userExaminationChatActivity:"User Examination Chat Activity",
        userInvestigationChatActivity:"User Investigation Chat Activity",
        userDignosisActivity:"User Dignosis Activity",
        userMarksSchemeActivity:'User MarksScheme Activity',
        userCaseConditionsActivity:'User Case Conditions Activity',
        userChallengeStartActivity:'User Challenge Start Activity',
        userNotificationsToggleActivity:'User Notifications Toggle Activity',
        userAccountDetailsActivity:'User Account Details Activity',
        userTabSwitchActivity:'User Tab Switch Activity',
        userLogoutActivity:'User Logout Activity',
        UserSkippedOnBoardingActivity:'User Skipped On Boarding Activity',
        userLearnScreenActivity:'User Learn Screen Activity',
        userHomeScreenActivity:'User Home Screen Activity',
        userSettingsActivity:'User Settings Screen Activity',
        userNewsSharingActivity:'User News Sharing Activity',
        userScoreSharingActivity:'User Score Sharing Activity',
        userCaseMarksActivity:'User Case MarksScheme Acitivity',
        userMarksSchemeExpandedActivity:'User MarksScheme Expanded Activity',
        userMarksSchemeInfoActivity:'User MarksScheme Info Activity',
        userChangeUniverityActivity:'User Change University Activity'

    }


};



module.exports = MixPanelEvents;
