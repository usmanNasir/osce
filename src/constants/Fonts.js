/*
 * @file: Fonts.js
 * @description: App Fonts
 * */

"use-strict";
const Fonts = {
  Bold: {
    fontFamily: "Roboto-Bold"
  },
  Medium: {
    fontFamily: "Roboto-Medium"
  },
  Light: {
    fontFamily: "Roboto-Light"
  },
  Regular: {
    fontFamily: "Roboto-Regular"
  },
  Main: {
    fontFamily: "Peddana-Regular"
  },
  Bold_saira: {
    fontFamily: "Saira-Medium"
  }
};
module.exports = Fonts;
