/*
 * @file: Images.js
 * @description: Images constants file for the application

 * */

"use strict";
module.exports = {
  Common: {
    Logo: require("../assets/img/logo.png"),
    logoBase64: require("../assets/img/logo.png"),
    meBubble: require("../assets/img/bubble/me_bubble.png"),
    youBubble: require("../assets/img/bubble/you_bubble.png"),
    sendButton: require("../assets/img/send_button.png"),
    Forward: require("../assets/img/Forward.png"),
    ic_arrow_up: require("../assets/img/ic_arrow_up.png"),
    ic_arrow_down: require("../assets/img/ic_arrow_down.png"),
    checkIcon: require("../assets/img/ic_checked.png"),
    crossIcon: require("../assets/img/cross.png"),
    backButton: require("../assets/img/Backward.png"),
    checkMark: require("../assets/img/checkmark.png"),
    pass: require("../assets/img/star.png"),
    fail: require("../assets/img/report.png")
  }
};
