/*
 * @file: i18n.js
 * @description: App i18n Localization

 * */
"use strict";

let colors = {
  manatee: "#8183A7",
  blue: "#3E96ED",
  Primary: "#EBEBEB",
  darkGrey1: "#B1B1B1",
  darkGrey2: "#8B8B8B",
  lightGrey: "#EBEBEB",
  gunPowderShade: "#47455A",
  errorRed: "#FE3F3F",
  Primary2: "#ebebeb",
  Secondary: "rgb(252, 142, 49)",
  Yellow: "#F6CF65",
  DarkGray: "#393B3B",
  LightGray: "#B7B7B7",
  appBackgroundColor: "#FAFAFA",
  ChatTextColor: "#37474F",
  Transparent: "transparent",
  White: "#FFFFFF",
  Black: "#606060",
  placehoder: "#707070",
  listBorder: "#B4C9CC",
  green: "#8BCC82",
  red: "#FF6965",
  orange: "orange",
  transparent: "transparent",
  gray: "#A9AFAF",
  menuItemTxt: "#393B3B",
  fadeBorder: "#e0e0e0",
  ocen: "rgb(175, 207, 237)",
  lightYellow: "#fcfaae",
  DarkBlack: "#000000",
  ChatBlueColor: "#4877F8",
  ChatFooterColor: "#ECEFF0",
  BoxWrapperColor: "#F5F5F5",
  shadownColor: "#00000029",
  parrotGreen: "#97DC76",
  link: "#0077FF",
  caseColor: "#AFAFAF",
  redText: "#ff4f4f"
};

module.exports = colors;
