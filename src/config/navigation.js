import { Navigation } from "react-native-navigation";
import constants from "../constants";
import { AsyncStorage, Platform,Linking } from "react-native";
import { moderateScale } from "../helpers/ResponsiveFonts";
import homeIcon from "../assets/img/home.png";
import searchIcon from "../assets/img/search.png";
import profileIcon from "../assets/img/user.png";
import settingsIcon from "../assets/img/settings.png";


export const goToFirstScreen = async () => {

  const showTutorial = await AsyncStorage.getItem("showTutorial");
  if (showTutorial === "true" || showTutorial === null) {
    return Navigation.setRoot({
      root: {
        stack: {
          id: "Tutorial",
          children: [
            {
              component: {
                name: "Tutorial",
                passProps: {
                  text: "React Native",
                },
                options: {
                  topBar: {
                    title: {
                      text: "Tutorial",
                      alignment: "center",
                    },
                    drawBehind: false,
                    visible: false,
                    animate: false,
                  },
                },
              },
            },
          ],
        },
      },
    });
  } else {
    Navigation.setRoot({
      root: {
        stack: {
          id: "Tutorial",
          children: [
            {
              component: {
                name: "FirstScreen",
                passProps: {
                  text: "React Native",
                },
                options: {
                  topBar: {
                    title: {
                      text: "Tutorial",
                      alignment: "center",
                    },
                    drawBehind: false,
                    visible: false,
                    animate: false,
                  },
                },
              },
            },
          ],
        },
      },
    });
  }

};



export const goToLeaderBoardScreen = async (id) => {
  const isBackgroundLoad = true;
  return Navigation.setRoot({
    root: {
      // id: "BOTTOM_TABS_LAYOUT",
      bottomTabs: {
        children: [
          // {
          //   component: {
          //     id: 'Home',
          //     name: 'Home'
          //   }
          // },
          {
            stack: {
              id: "HomeScreen",
              children: [
                {
                  component: {
                    name: "HomeScreen",
                    passProps: {
                      isBackgroundLoad,
                      friendId:id,
                      toggle: false,
                    },
                    options: {
                      topBar: {
                        title: {
                          text: "OSCE.AI",
                          alignment: "center",
                          color: constants.Colors.Black,
                          ...constants.Fonts.Medium,
                          fontSize: moderateScale(20),
                        },
                        drawBehind: false,
                        visible: false,
                        animate: false,
                      },
                      bottomTab: {
                        iconColor: constants.Colors.gray,
                        textColor: constants.Colors.gray,
                        selectedIconColor: constants.Colors.blue,
                        selectedTextColor: constants.Colors.blue,
                        text: "Home",
                        testID: "HomeScreen",
                        visible: true,
                        icon: homeIcon,


                      },
                      bottomTabs: {
                       visible   : true,
                       drawBehind: Platform.OS === 'android' ?true:false,// for Android
                        animate   : true,
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTabs: {
                  visible: true,
                  drawBehind:false
                },
                bottomTab: {
                  iconColor: constants.Colors.gray,
                  textColor: constants.Colors.gray,
                  selectedIconColor: constants.Colors.blue,
                  selectedTextColor: constants.Colors.blue,
                  text: "Home",
                  testID: "HomeScreen",
                  visible: true,
                  icon: homeIcon,
                },
              },
            },
          },
          {
            stack: {
              id: "Home",
              children: [
                {
                  component: {
                    name: "Home",
                    passProps: {
                      text: "React Native",
                      toggle: false,
                    },
                    options: {
                      topBar: {
                        title: {
                          text: "OSCE.AI",
                          alignment: "center",
                          color: constants.Colors.Black,
                          ...constants.Fonts.Medium,
                          fontSize: moderateScale(20),
                        },
                        visible: false,
                        animate: false,
                        drawBehind: false,
                      },
                      bottomTab: {
                        iconColor: constants.Colors.gray,
                        textColor: constants.Colors.gray,
                        selectedIconColor: constants.Colors.blue,
                        selectedTextColor: constants.Colors.blue,
                        text: "Learn",
                        testID: "Learn",
                        visible: true,
                        icon: searchIcon,
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTabs: {
                  visible: true,
                  drawBehind: false,
                },
                bottomTab: {
                  iconColor: constants.Colors.gray,
                  textColor: constants.Colors.gray,
                  selectedIconColor: constants.Colors.blue,
                  selectedTextColor: constants.Colors.blue,
                  text: "Learn",
                  testID: "Learn",
                  visible: true,
                  icon: searchIcon,
                },
              },
            },
          },
          {
            stack: {
              id: "Profile",
              children: [
                {
                  component: {
                    name: "Profile",
                    passProps: {
                      text: "React Native",
                      item: { name: "usmsn" },
                      toggle: false,
                    },
                    options: {
                      topBar: {
                        title: {
                          text: "OSCE.AI",
                          alignment: "center",
                          color: constants.Colors.Black,
                          ...constants.Fonts.Medium,
                          fontSize: moderateScale(20),
                        },
                        drawBehind: false,
                        visible: false,
                        animate: false,
                      },
                      bottomTabs: {
                        visible: true,
                        drawBehind: false,
                      },
                      bottomTab: {
                        iconColor: constants.Colors.gray,
                        textColor: constants.Colors.gray,
                        selectedIconColor: constants.Colors.blue,
                        selectedTextColor: constants.Colors.blue,
                        text: "Settings",
                        testID: "Settings",
                        visible: true,
                        icon: settingsIcon,
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTabs: {
                  visible: true,
                  drawBehind: false,
                },
                bottomTab: {
                  iconColor: constants.Colors.gray,
                  textColor: constants.Colors.gray,
                  selectedIconColor: constants.Colors.blue,
                  selectedTextColor: constants.Colors.blue,
                  text: "Settings",
                  testID: "Settings",
                  visible: true,
                  icon: settingsIcon,
                },
              },
            },
          },
        ],
      }, //end of bottom tab
    }, //end of roottab
  });

   
  
};





export const goHome = () =>

  Navigation.setRoot({
    root: {
      // id: "BOTTOM_TABS_LAYOUT",
      bottomTabs: {
        children: [
          // {
          //   component: {
          //     id: 'Home',
          //     name: 'Home'
          //   }
          // },
          {
            stack: {
              id: "HomeScreen",
              children: [
                {
                  component: {
                    name: "HomeScreen",
                    passProps: {
                      text: "React Native",
                      toggle: false,
                    },
                    options: {
                      topBar: {
                        title: {
                          text: "OSCE.AI",
                          alignment: "center",
                          color: constants.Colors.Black,
                          ...constants.Fonts.Medium,
                          fontSize: moderateScale(20),
                        },
                        drawBehind: false,
                        visible: false,
                        animate: false,
                      },
                      bottomTab: {
                        iconColor: constants.Colors.gray,
                        textColor: constants.Colors.gray,
                        selectedIconColor: constants.Colors.blue,
                        selectedTextColor: constants.Colors.blue,
                        text: "Home",
                        testID: "HomeScreen",
                        visible: true,
                        icon: homeIcon,


                      },
                      bottomTabs: {
                       visible   : true,
                       drawBehind: Platform.OS === 'android' ?true:false,// for Android
                        animate   : true,
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTabs: {
                  visible: true,
                  drawBehind:false
                },
                bottomTab: {
                  iconColor: constants.Colors.gray,
                  textColor: constants.Colors.gray,
                  selectedIconColor: constants.Colors.blue,
                  selectedTextColor: constants.Colors.blue,
                  text: "Home",
                  testID: "Home",
                  visible: true,
                  icon: homeIcon,
                },
              },
            },
          },
          {
            stack: {
              id: "Home",
              children: [
                {
                  component: {
                    name: "Home",
                    passProps: {
                      text: "React Native",
                      toggle: false,
                    },
                    options: {
                      topBar: {
                        title: {
                          text: "OSCE.AI",
                          alignment: "center",
                          color: constants.Colors.Black,
                          ...constants.Fonts.Medium,
                          fontSize: moderateScale(20),
                        },
                        visible: false,
                        animate: false,
                        drawBehind: false,
                      },
                      bottomTab: {
                        iconColor: constants.Colors.gray,
                        textColor: constants.Colors.gray,
                        selectedIconColor: constants.Colors.blue,
                        selectedTextColor: constants.Colors.blue,
                        text: "Learn",
                        testID: "Learn",
                        visible: true,
                        icon: searchIcon,
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTabs: {
                  visible: true,
                  drawBehind: false,
                },
                bottomTab: {
                  iconColor: constants.Colors.gray,
                  textColor: constants.Colors.gray,
                  selectedIconColor: constants.Colors.blue,
                  selectedTextColor: constants.Colors.blue,
                  text: "Learn",
                  testID: "Learn",
                  visible: true,
                  icon: searchIcon,
                },
              },
            },
          },
          {
            stack: {
              id: "Profile",
              children: [
                {
                  component: {
                    name: "Profile",
                    passProps: {
                      text: "React Native",
                      item: { name: "usmsn" },
                      toggle: false,
                    },
                    options: {
                      topBar: {
                        title: {
                          text: "OSCE.AI",
                          alignment: "center",
                          color: constants.Colors.Black,
                          ...constants.Fonts.Medium,
                          fontSize: moderateScale(20),
                        },
                        drawBehind: false,
                        visible: false,
                        animate: false,
                      },
                      bottomTabs: {
                        visible: true,
                        drawBehind: false,
                      },
                      bottomTab: {
                        iconColor: constants.Colors.gray,
                        textColor: constants.Colors.gray,
                        selectedIconColor: constants.Colors.blue,
                        selectedTextColor: constants.Colors.blue,
                        text: "Settings",
                        testID: "Settings",
                        visible: true,
                        icon: settingsIcon,
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTabs: {
                  visible: true,
                  drawBehind: false,
                },
                bottomTab: {
                  iconColor: constants.Colors.gray,
                  textColor: constants.Colors.gray,
                  selectedIconColor: constants.Colors.blue,
                  selectedTextColor: constants.Colors.blue,
                  text: "Settings",
                  testID: "Settings",
                  visible: true,
                  icon: settingsIcon,
                },
              },
            },
          },
        ],
      }, //end of bottom tab
    }, //end of roottab
  });
