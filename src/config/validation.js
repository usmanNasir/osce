/* eslint-disable */
const passwordRegex = /^[a-zA-Z0-9!@#$&(),.+-]{6,15}$/;
const emailReg = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;

export function checkValidation(state, signup = "") {
  let errors = {};

  if (!state.email) {
    errors["email"] = "Email cannot be left empty.";
  } else if (state.email && !emailReg.test(state.email)) {
    errors["email"] = "Invalid email";
  }

  if (!state.password) {
    errors["password"] = "Password cannot be left empty.";
  } else if (state.password && !passwordRegex.test(state.password) && signup) {
    errors["password"] = "Password should have atleast 8 characters.";
  }

  if (!state.name && signup) {
    errors["name"] = "Name cannot be left empty.";
  }

  if (!state.university && signup) {
    errors["university"] = "University cannot be left empty.";
  }

  if (!state.job_title && signup) {
    errors["job_title"] = "Job title cannot be left empty.";
  }

  if (!state.year_group && signup) {
    errors["year_group"] = "Year group cannot be left empty.";
  }

  return errors;
}
