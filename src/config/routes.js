/* eslint-disable */
import React from "react";

import { Navigation } from "react-native-navigation";

import { View } from "react-native";
import { goToAppUpdate, goToLeaderBoardScreen } from "./navigation";
import { Provider } from "react-redux";

import {
  FirstScreen,
  Login,
  SignUp,
  Home,
  Tutorial,
  Chatbot,
  ComplaintTree,
  Feedback,
  Profile,
  Info,
  ConditionPage,
  PrivacyPolicy,
  TermsAndConditions,
  HowItWorks,
  HomeScreen,
  CaseViewScreen,
  FeedBackScreen,
  LeaderBoardScreen,
  ChallengeScoreScreen,
  ExaminationScreen,
  DignosisScreen,
  InvestigationScreen
} from "../container";
import SideMenu from "../components/common/SideMenu";
import { Linking } from "react-native";
import branch from "react-native-branch";
import FlashMessage from "react-native-flash-message";
import MixPanelProvider from "../utilities/MixPanel";
import InAppUpdateModal from "../components/common/inAppUpdateModal";
import { checkVersion } from "../utilities/updateCheck";


const WrapScreen = (ReduxScreen, store) => (props) => (
  <MixPanelProvider>
    <Provider store={store}>
      <View style={{ flex: 1 }}>
        <ReduxScreen {...props} />
        <FlashMessage position="top" />
      </View>
    </Provider>
  </MixPanelProvider>
);

export const registerScreens =async (store, Provider) => {


  setTimeout(() => {
 
    branch.subscribe(({ error, params, uri }) => {
      if (error) {
        console.error("Error from Branch: " + error);
        return;
      }
      const url = params["~referring_link"];
      // alert(JSON.stringify(params))
      if (url) {
        const routeName = params["deeplink_path"];
        const id = params["friendId"];
        if (routeName === "LeaderBoard") {
          // alert(uri);
          goToLeaderBoardScreen(id);
        }
      }
    });
  }, 2000);

  // Loader Stack
  Navigation.registerComponentWithRedux(
    "Loader",
    () => require("../container/AppContainer").default,
    Provider,
    store
  );

  // Auth Stack
  Navigation.registerComponent(
    "Login",
    () => WrapScreen(Login, store),

    () => Login
  );
  Navigation.registerComponent(
    "SignUp",
    () => WrapScreen(SignUp, store),

    () => FirstScreen
  );
  Navigation.registerComponent(
    "FirstScreen",
    () => WrapScreen(FirstScreen, store),

    () => SignUp
  );

  Navigation.registerComponent(
    "Tutorial",
    () => WrapScreen(Tutorial, store),

    () => FirstScreen
  );

  // Dashboard Stack
  Navigation.registerComponent(
    "Home",
    () => WrapScreen(Home, store),

    () => Home
  );

  // Dashboard Stack

  Navigation.registerComponent(
    "ComplaintTree",
    () => WrapScreen(ComplaintTree, store),
    () => ComplaintTree
  );

  Navigation.registerComponent(
    "SideMenu",
    () => WrapScreen(SideMenu, store),
    () => SideMenu
  );

  Navigation.registerComponent(
    "Profile",
    () => WrapScreen(Profile, store),
    () => Profile
  );

  Navigation.registerComponent(
    "HowItWorks",
    () => WrapScreen(HowItWorks, store),
    () => HowItWorks
  );

  Navigation.registerComponent(
    "PrivacyPolicy",
    () => WrapScreen(PrivacyPolicy, store),
    () => PrivacyPolicy
  );

  Navigation.registerComponent(
    "TermsAndConditions",
    () => WrapScreen(TermsAndConditions, store),
    () => TermsAndConditions
  );

  Navigation.registerComponent(
    "Chatbot",
    () => WrapScreen(Chatbot, store),
    () => Chatbot
  );

  Navigation.registerComponent(
    "Feedback",
    () => WrapScreen(Feedback, store),
    () => Feedback
  );

  Navigation.registerComponent(
    "Info",
    () => WrapScreen(Info, store),
    () => Info
  );

  Navigation.registerComponent(
    "ConditionPage",
    () => WrapScreen(ConditionPage, store),
    () => ConditionPage
  );

  Navigation.registerComponent(
    "HomeScreen",
    () => WrapScreen(HomeScreen, store),
    () => HomeScreen
  );
  Navigation.registerComponent(
    "CaseViewScreen",
    () => WrapScreen(CaseViewScreen, store),
    () => CaseViewScreen
  );
  Navigation.registerComponent(
    "FeedBackScreen",
    () => WrapScreen(FeedBackScreen, store),
    () => FeedBackScreen
  );
  Navigation.registerComponent(
    "LeaderBoardScreen",
    () => WrapScreen(LeaderBoardScreen, store),
    () => LeaderBoardScreen
  );
  Navigation.registerComponent(
    "ChallengeScoreScreen",
    () => WrapScreen(ChallengeScoreScreen, store),
    () => ChallengeScoreScreen
  );
  Navigation.registerComponent(
    "Examination",
    () => WrapScreen(ExaminationScreen, store),
    () => ExaminationScreen
  );
  Navigation.registerComponent(
    "Dignosis",
    () => WrapScreen(DignosisScreen, store),
    () => DignosisScreen
  );
  Navigation.registerComponent(
    "Investigate",
    () => WrapScreen(InvestigationScreen, store),
    () => InvestigationScreen
  );
  Navigation.registerComponent(
    "AppUpdate",
    ()=>WrapScreen(InAppUpdateModal,store),
    ()=>InAppUpdateModal
  )
};
