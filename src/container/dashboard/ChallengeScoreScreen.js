import React from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  Platform,
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import { getSharingUrl } from "../../actions";
import { MixPanelContext } from "../../utilities/MixPanel";
import { Activity, Share_Score_Click } from "../../constants/Events";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import constants from "../../constants";
import Header from "../Header";
import Card from "../../components/common/Card";
import Share from "react-native-share";
import Loader from "../../components/common/loader";
import _ from "underscore";
import * as AppAction from "../../actions";

const shareHandler = async (props, sharingUrl, context) => {
  context.mixPanel.track(Share_Score_Click);
  context.mixPanel.track(Activity.userScoreSharingActivity);
  let AppLink = `\nhttps://osceai.app.link/xLAyWn5Auib?deeplink_path=LeaderBoard&friendId=${
    props.user._id
  }`;
  let url = `${props.user.name} has Scored ${
    props.scoreData?.totalPoints
  } Points. Open OSCE AI to view the leaderboard ${AppLink}`;

  const shareOptions = {
    title: "Share Score",
    failOnCancel: false,
    message: url,
  };

  Share.open(shareOptions)
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      err && console.log(err);
    });
};

class ChallengeScoreScreen extends React.Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
    this.state = {
      pageNo: 1,
      sharingUrl: "",
    };
  }

  getShareUrl = async () => {
    const url = `${this.props.serverUrl}SharingUrl/get/`;
    const data = await getSharingUrl(url);
    if (data) {
      this.setState({ sharingUrl: data });
    }
  };

  async componentDidMount() {
    await this.getScoreData();
    this.getShareUrl();
  }

  async getScoreData() {
    await this.props.AppAction.getScores(
      this.props.item._id,
      this.props.item.complaintId,
      this.props.sessionId,
      true,
      this.props.awardDignosisScore,
      this.props.awardScore
    );
  }

  goToFeedbackPage = async () => {
    this.props.AppAction.triggerButtonChange();
    Navigation.push(this.props.componentId, {
      component: {
        name: "Feedback",
        passProps: { ...this.props },
        options: {
          topBar: {
            visible: false,
          },
          bottomTabs: {
            visible: false,
            drawBehind: Platform.OS === "android" ? true : false,
          },
        },
      },
    });
  };

  render() {
    const position =
      this.props.rank === 1
        ? "1st"
        : this.props.rank === 2
        ? "2nd"
        : this.props.rank === 3
        ? "3rd"
        : `${this.props.rank}th`;

    return (
      <SafeAreaView style={styles.container}>
        <Header
          text="Score"
          hideBack
          nextComponent={this.goToFeedbackPage.bind(this)}
          showForward={true}
          isScoreScreen={true}
          border={true}
        />
        {this.props.scoreLoader ? (
          <Loader />
        ) : (
          <ScrollView>
            <View style={{ marginHorizontal: 10, marginTop: 10 }}>
              <Card
                style={{
                  ...styles.cardWrapper,
                  paddingHorizontal: 30,
                  paddingVertical: 30,
                }}
              >
                <View
                  style={{ justifyContent: "center", alignItems: "center" }}
                >
                  <Text style={styles.cardTitle}>Your score!</Text>
                  <View
                    style={{
                      flexDirection: "row",
                      marginVertical: moderateScale(15),
                    }}
                  >
                    <View
                      style={{
                        marginRight: moderateScale(10),
                        justifyContent: "center",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: moderateScale(55),
                          fontWeight: "bold",
                          color: constants.Colors.Black,
                          alignSelf: "center",
                        }}
                      >
                        {this.props.scoreData?.totalPoints}
                      </Text>
                      <Text
                        style={{
                          fontSize: moderateScale(12),
                          fontWeight: "bold",
                          color: constants.Colors.Black,
                          alignSelf: "center",
                        }}
                      >
                        POINTS
                      </Text>
                    </View>
                    <View
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        height: 130,
                        width: 90,
                      }}
                    >
                      <Image
                        height={130}
                        source={require("../../assets/img/gemScore.png")}
                      />
                    </View>
                  </View>
                  <Text
                    style={{
                      fontSize: moderateScale(30),
                      fontWeight: "bold",
                      marginBottom: moderateScale(30),
                      color: constants.Colors.Black,
                    }}
                  >
                    {this.props.user?.name}
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      shareHandler(
                        this.props,
                        this.state.sharingUrl,
                        this.context
                      )
                    }
                    style={styles.shareContainer}
                  >
                    <Text style={styles.shareText}>SHARE</Text>
                  </TouchableOpacity>
                </View>
              </Card>
              <Card
                style={{
                  ...styles.cardWrapper,
                }}
              >
                <View
                  style={{ justifyContent: "center", alignItems: "center" }}
                >
                  <Text style={{ ...styles.cardTitle, fontWeight: "600" }}>
                    Leaderboard Position
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      marginVertical: moderateScale(20),
                    }}
                  >
                    <Text
                      style={{
                        fontSize: moderateScale(55),
                        fontWeight: "bold",
                        color: constants.Colors.Black,
                        alignSelf: "flex-end",
                        marginRight: 10,
                      }}
                    >
                      {position}
                    </Text>
                    <Image
                      style={styles.headerImage}
                      resizeMode="contain"
                      source={require("../../assets/img/trophy.png")}
                    />
                  </View>
                </View>
              </Card>
            </View>
          </ScrollView>
        )}
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    user: state.Auth.authData,
    leaderboardArray: state.homeScreenReducer.homeContent[2],
    scoreData: state.Score.scores,
    scoreLoader: state.Score.loader,
  };
}
const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChallengeScoreScreen);

//style sheet
const styles = StyleSheet.create({
  container: {
    backgroundColor: constants.Colors.appBackgroundColor,
    flex: 1,
    paddingTop: 20,
  },

  cardWrapper: {
    paddingVertical: moderateScale(20),
    justifyContent: "space-between",
    paddingHorizontal: moderateScale(16),
    marginTop: moderateScale(20),
    backgroundColor: constants.Colors.White,
  },
  cardTitle: {
    fontSize: moderateScale(30),
    fontWeight: "600",
    color: constants.Colors.Black,
  },
  shareContainer: {
    backgroundColor: "#3E96ED",
    height: moderateScale(45),
    width: "60%",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#3E96ED",
  },
  shareText: {
    fontSize: moderateScale(17),
    color: constants.Colors.White,
    fontWeight: "700",
  },
});
