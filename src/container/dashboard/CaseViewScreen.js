import React from "react";
import { 
  View,
   StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    Image 
  } from "react-native";
import { WebView } from "react-native-webview";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import constants from "../../constants";
import _ from "underscore";
import * as AppAction from "../../actions";


class CaseViewScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => Navigation.pop("HomeScreen")}
            style={{ flex: 0.1,paddingHorizontal:moderateScale(20) }}
          >
            <Image
              resizeMode="contain"
              source={constants.Images.Common.backButton}
            />
          </TouchableOpacity>
        </View>
        <WebView scalesPageToFit style={{flex:1,borderWidth:1}} startInLoadingState={true} source={{ uri: this.props.url }} />
       
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    feedbackLoader: state.feedbackScreenReducer.feedbackLoader,
  };
};

const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CaseViewScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: moderateScale(20),
    
  },
  header: {
    flexDirection: "row",
    height: moderateScale(45),
    justifyContent: "space-between",
    alignItems: "center",
  },
  headingText: {
    fontSize: moderateScale(18),
    ...constants.Fonts.Bold,
  },
});
