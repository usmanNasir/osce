import React from "react";
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import constants from "../../constants";
import checkMark from "../../assets/img/checkmark.png";
import ControlledLoader from "../../components/common/ControlledLoader";
import _ from "underscore";
import * as AppAction from "../../actions";

const handleSubmitfeedback = (feedback, props) => {
  const requestBody = {
    receipt: "usmanbajwa878@gmail.com",
    sender: "usmanbajwa878@gmail.com",
    subject: "testing",
    feedback: feedback,
  };
  props.AppAction.createFeedBack({ ...requestBody }).then(() => {
    Alert.alert("SUCCESS", "Feedback Submitted Successfully", [{ text: "OK" }]);
  });
};

class FeedBackScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isChecked: false,
      loader: false,
      feedback: "",
    };
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View>
            <View style={styles.header}>
              <TouchableOpacity
                onPress={() =>
                  Navigation.pop("HomeScreen", {
                      bottomTabs: {
                        drawBehind: false,
                        visible: true,
                      },
                  })
                }
                style={{ flex: 0.1 }}
              >
                <Image
                  resizeMode="contain"
                  source={constants.Images.Common.backButton}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.contentContainer}>
              <Text style={styles.headingText}>
                How can we make the app more useful for you?
              </Text>
              <TextInput
                onChangeText={(text) => {
                  this.setState({ feedback: text });
                }}
                multiline={true}
                style={styles.textBox}
              />
              <View style={styles.confirmationContainer}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({ isChecked: !this.state.isChecked })
                  }
                  style={styles.checkBox}
                >
                  {this.state.isChecked && (
                    <Image
                      resizeMode="contain"
                      style={styles.checkedImage}
                      source={checkMark}
                    />
                  )}
                </TouchableOpacity>
                <View style={styles.confirmationTextContainer}>
                  <Text style={styles.confirmationText}>
                    Click here if your are happy to be contacted by email
                    regarding your feedback.
                  </Text>
                </View>
              </View>
              <TouchableOpacity
                onPress={() =>
                  handleSubmitfeedback(this.state.feedback, this.props)
                }
                style={styles.buttonContainer}
              >
                {this.props.feedbackLoader ? (
                    <ControlledLoader showLoader={this.props.feedbackLoader}/>
                ) : (
                  <Text style={styles.buttonText}>Send FeedBack</Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    feedbackLoader: state.feedbackScreenReducer.feedbackLoader,
  };
};

const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FeedBackScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginHorizontal: moderateScale(20),
  },
  header: {
    flexDirection: "row",
    height: moderateScale(45),
    justifyContent: "space-between",
    alignItems: "center",
  },
  headingText: {
    fontSize: moderateScale(18),
    ...constants.Fonts.Bold,
  },
  textBox: {
    marginVertical: 10,
    height: moderateScale(200),
    borderWidth: 1,
    borderColor: constants.Colors.gray,
    paddingTop: 0,
    paddingBottom: 0,
    textAlignVertical: "top",
    fontSize: moderateScale(15),
  },
  confirmationContainer: {
    flexDirection: "row",
  },
  checkBox: {
    height: 25,
    width: 25,
    borderWidth: 1,
    borderColor: constants.Colors.gray,
  },
  confirmationTextContainer: {
    marginHorizontal: 5,
    flex: 1,
  },
  confirmationText: {
    fontSize: moderateScale(15),
  },
  confirmationSupport: {
    fontSize: moderateScale(15),
    color: constants.Colors.blue,
  },
  buttonContainer: {
    width: "100%",
    height: 60,
    backgroundColor: "#428FDC",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    borderRadius: moderateScale(12),
    marginTop: moderateScale(40),
  },
  buttonText: {
    fontSize: moderateScale(20),
    color: constants.Colors.White,
  },
  checkedImage: {
    height: 25,
    width: 25,
  },
});
