import React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Text,
  SafeAreaView,
  Image,
  Dimensions,
  Alert,
  Platform,
} from "react-native";
import {
  getHistory,
  setScrenStack,
  createLastComplaint,
  getExamination,
  getInvestigation,
  triggerHomeRefreshChange,
} from "../../actions";
import { connect } from "react-redux";
import { removeListeners } from "../../utilities/listeners";
import { Navigation } from "react-native-navigation";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { logger } from "./../../utilities/analytics-logger";
import { MixPanelContext } from "../../utilities/MixPanel";
import { Case_Type_Clicked } from "../../constants/Events";
import { maleIcon, femaleIcon } from "../../assets";
import constants from "../../constants";
import NoRecord from "../../components/common/NoRecord";
import ImageLoad from "../../components/ImageLoad";
import Loader from "./../../components/common/loader";
import Header from "../Header";
import * as Progress from "react-native-progress";

const width = Dimensions.get("window").width;
const defaultColor = "#E2E2E2";
let removeListener = true;

// const createLastComplaintCall = async (props, progress) => {
//   console.log(
//     "INSIDE CREATE COMPLAINT CALL",
//     props.HomelastComplaint.subTitle,
//     props.item.name,
//     props.HomelastComplaint.contentType
//   );
//   if (
//     props.HomelastComplaint.contentType === "keeplearning" &&
//     props.HomelastComplaint.subTitle !== props.item.name
//   ) {
//     const bodyObject = {
//       userId: props.Auth._id,
//       complaintId: props.item._id,
//       progressBar: `${progress}%`,
//       complaintTitle: props.item.name,
//       complaint: props.item,
//       caseCompleted: progress,
//     };
//     await props.createlastComplaint(bodyObject);
//     props.triggerHomeRefreshChange(true);
//   }
// };

class ComplaintTree extends React.Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
    this.state = {
      pageNo: 1,
      Grades: [],
      progress: null,
    };
    this.isSideDrawerVisible = false;
    Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    this.getData();
    setTimeout(() => {
      this.context.mixPanel.track(Case_Type_Clicked, {
        CaseType: this.props.item.name,
      });
    }, 3000);
  }

  componentWillUnmount() {
    if (removeListener) {
      removeListeners();
    }
  }
  async getData() {
    await this.getHistories();
    await this.getExaminations();
    await this.getInvestigations();
    const progress =  this.getProgress();
    await this.createLastComplaintCall(progress);
    logger.logEvent(this.props.item.name);

  }
  previousComponent = () => {
    this.props.isDifferentStack
      ? Navigation.pop("HomeScreen", {
          bottomTabs: {
            visible: true,
            drawBehind: Platform.OS === "android" ? true : false,
          },
        }) &&
        this.props.setScrenStack(this.props.componentId, "HomeScreen", false, {
          navigationCalled: true,
          bottomTabs: { drawBehind: true },
        })
      : Navigation.pop("Home") &&
        this.props.setScrenStack(this.props.componentId, "Home", false, {
          navigationCalled: true,
          bottomTabs: { visible: true },
        });
  };

  getHistories = () => {
    let { pageNo } = this.state;
    let { _id = "" } = this.props.item ? this.props.item : {};
    this.props.getHistory({ pageNo, _id });
  };

  getExaminations = () => {
    let { pageNo } = this.state;
    let { _id = "" } = this.props.item ? this.props.item : {};
    this.props.getExamination({ pageNo, _id });
  };

  
 createLastComplaintCall = async (progress) => {
  console.log(
    "INSIDE CREATE COMPLAINT CALL",
    this.props.HomelastComplaint.subTitle,
    this.props.item.name,
    this.props.HomelastComplaint.contentType
  );
  if (
    this.props.HomelastComplaint.contentType === "keeplearning" &&
    this.props.HomelastComplaint.subTitle !== this.props.item.name
  ) {
    const bodyObject = {
      userId: this.props.Auth._id,
      complaintId: this.props.item._id,
      progressBar: `${progress}%`,
      complaintTitle: this.props.item.name,
      complaint: this.props.item,
      caseCompleted: progress,
    };
    await this.props.createlastComplaint(bodyObject);
    console.log("AFTER CALL",bodyObject)
    this.props.triggerHomeRefreshChange(true);
  }
  if (
    this.props.HomelastComplaint.contentType === "challenge" 
  ) {
    //means no keep learning exists
    const bodyObject = {
      userId: this.props.Auth._id,
      complaintId: this.props.item._id,
      progressBar: `${progress}%`,
      complaintTitle: this.props.item.name,
      complaint: this.props.item,
      caseCompleted: progress,
    };
    await this.props.createlastComplaint(bodyObject);
    console.log("AFTER CALL",bodyObject)
    this.props.triggerHomeRefreshChange(true);
  }
};


  getInvestigations = () => {
    let { pageNo } = this.state;
    let { _id = "" } = this.props.item ? this.props.item : {};
    this.props.getInvestigation({ pageNo, _id });
  };

  getProgress =  () => {
    const { Grades, item } = this.props;
    let find = Grades.find((obj) => obj.complaintId === item._id);
    console.log("FIND",find)
    if (find && find.count && find.count != undefined) {
      this.setState({
        progress: find.count,
      });
    }
    const progressBar = find != undefined || find?.count || find?.count != undefined ? find.count : 0;
    console.log("INSIDE CALL");
    return progressBar
  };

  pressBot(_id, dialogflowCredentials, history) {
    const { item } = this.props;
    if (item)
      if (
        !dialogflowCredentials ||
        !dialogflowCredentials.length ||
        !Object.keys(dialogflowCredentials[0]).length
      ) {
        Alert.alert("Under Construction", "New patient case coming soon...");
      } else {
        Navigation.push(this.props.componentId, {
          component: {
            name: "Chatbot",
            passProps: {
              item: history,
              name: item.name,
              complaintData: item,
              Grades: this.props.Grades,
              isDifferentStack: this.props.isDifferentStack ? true : false,
            },
            options: {
              topBar: {
                visible: false,
              },
              bottomTabs: {
                visible: false,
                drawBehind: Platform.OS === "android" ? true : false,
              },
              bottomTab: {
                visible: false,
              },
            },
          },
        });
      }
  }

  renderItems = ({ item }) => {
    const {
      colour = "",
      iconUrl = "",
      age = "",
      gender = "",
      grade = null,
      _id,
      dialogflowCredentials = [],
      thumbnailUrl,
    } = item;
    let history = item;
    return this.renderTree({
      gender,
      age,
      iconUrl,
      colour,
      grade,
      _id,
      dialogflowCredentials,
      history,
      thumbnailUrl,
    });
  };

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20; // how far from the bottom
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  renderRoundImage({
    uri,
    color,
    grade = null,
    _id,
    dialogflowCredentials = [],
    history,
    thumbnailUrl,
  }) {
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => this.pressBot(_id, dialogflowCredentials, history)}
        style={{
          ...styles.roundWrap,
          backgroundColor: grade ? color : defaultColor,
        }}
      >
        <View style={styles.outerRing}>
          <View style={{ ...styles.roundImageWrap, backgroundColor: color }}>
            <ImageLoad
              isShowActivity={false}
              style={styles.roundImage}
              placeholderStyle={styles.roundImage}
              loadingStyle={{ size: "small", color: "black" }}
              source={{ uri: uri }}
              placeholderSource={{ uri: thumbnailUrl ? thumbnailUrl : null }}
              resizeMode="contain"
            />
          </View>
        </View>
        {grade && (
          <View style={[styles.badge, styles.gradeShadow]}>
            <Text style={[styles.ageText, { fontSize: moderateScale(32) }]}>
              {grade}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  }

  renderTree({
    gender,
    age,
    iconUrl,
    colour,
    grade,
    _id,
    dialogflowCredentials = [],
    history,
    thumbnailUrl,
  }) {
    return (
      <View
        style={{
          height: moderateScale(150),
          justifyContent: "center",
          alignItems: "center",
          width: width / 2,
        }}
      >
        {this.renderRoundImage({
          uri: iconUrl,
          color: colour,
          grade,
          _id,
          dialogflowCredentials,
          history,
          thumbnailUrl,
        })}
        <View style={{ height: moderateScale(5) }} />
        <View style={styles.roundFooterView}>
          <Text style={styles.ageText}>{age}</Text>
          {gender ? (
            <Image
              source={gender === "male" ? maleIcon : femaleIcon}
              style={styles.genderIcon}
            />
          ) : null}
        </View>
      </View>
    );
  }

  renderFirstElement = () => {
    let { histories = [] } = this.props;
    const {
      colour = "",
      iconUrl = "",
      age = "",
      gender = "",
      grade = null,
      _id,
      dialogflowCredentials = [],
      thumbnailUrl,
    } = histories[0] ? histories[0] : {};

    const history = histories[0] ? histories[0] : {};
    return (
      <View style={styles.singleView}>
        {this.renderTree({
          gender,
          age,
          iconUrl,
          colour,
          grade,
          _id,
          dialogflowCredentials,
          history,
          thumbnailUrl,
        })}
      </View>
    );
  };

  renderFlatItems() {
    let { histories = [], loader = false } = JSON.parse(
      JSON.stringify(this.props)
    );
    const list = histories && histories.length > 0 ? histories.splice(1) : [];
    return loader ? (
      <View style={styles.loaderView}>
        <Loader />
      </View>
    ) : histories && histories.length > 0 ? (
      <FlatList
        data={list}
        numColumns={2}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => item.toString() + index.toString()}
        ListHeaderComponent={this.renderFirstElement}
        renderItem={this.renderItems}
      />
    ) : (
      <NoRecord />
    );
  }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          paddingTop: moderateScale(20),
          backgroundColor: constants.Colors.appBackgroundColor,
        }}
      >
        <Header
          text={this.props.item.name}
          border={true}
          navigate={true}
          previousComponent={this.previousComponent}
        />

        <View
          style={{
            marginTop: 15,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Progress.Bar
            progress={
              this.state.progress == 3
                ? 1
                : this.state.progress == 2
                ? 0.6
                : this.state.progress == 1
                ? 0.3
                : 0
            }
            color="lightgreen"
            borderColor="white"
            unfilledColor="lightgrey"
            width={width - moderateScale(40)}
          />
        </View>
        <View style={{ height: moderateScale(5) }} />
        {this.renderFlatItems()}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = ({ Home, Auth, homeScreenReducer }, state) => {
  const { histories = [] } = Home.complaints;
  const HomelastComplaint = homeScreenReducer.homeContent[0];
  return {
    histories,
    HomelastComplaint,
    loader: Home.loader,
    Grades: state.Grades,
    Auth: Auth.authData,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getHistory: (obj) => dispatch(getHistory(obj)),
  getExamination: (obj) => dispatch(getExamination(obj)),
  getInvestigation: (obj) => dispatch(getInvestigation(obj)),
  createlastComplaint: (obj) => dispatch(createLastComplaint(obj)),
  clearComplaints: () => dispatch(),
  triggerHomeRefreshChange: (value) =>
    dispatch(triggerHomeRefreshChange(value)),
  setScrenStack: (componentId, name, visible) =>
    dispatch(setScrenStack(componentId, name, visible)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ComplaintTree);

const styles = StyleSheet.create({
  roundWrap: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: defaultColor,
    height: moderateScale(100),
    width: moderateScale(100),
    borderRadius: moderateScale(50),
  },
  outerRing: {
    justifyContent: "center",
    alignItems: "center",
    height: moderateScale(92),
    width: moderateScale(92),
    borderRadius: moderateScale(50),
    backgroundColor: "#FFF",
  },
  roundImageWrap: {
    justifyContent: "center",
    alignItems: "center",
    height: moderateScale(83),
    width: moderateScale(83),
    borderRadius: moderateScale(50),
    backgroundColor: defaultColor,
  },
  roundImage: {
    height: moderateScale(72),
    width: moderateScale(72),
    borderRadius: moderateScale(50),
  },
  gradeShadow: {
    borderBottomColor: constants.Colors.shadownColor,
    borderBottomWidth: 0,
    shadowColor: constants.Colors.shadownColor,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 1,
    shadowRadius: 1,
    elevation: 1,
  },
  badge: {
    justifyContent: "center",
    alignItems: "center",
    height: moderateScale(40),
    width: moderateScale(40),
    borderRadius: moderateScale(20),
    backgroundColor: constants.Colors.White,
    right: moderateScale(-5),
    bottom: moderateScale(-4),
    position: "absolute",
  },
  roundFooterView: {
    justifyContent: "center",
    alignItems: "center",
    height: moderateScale(50),
    width: "100%",
    flex: 0.5,
    flexDirection: "row",
  },
  ageText: {
    color: constants.Colors.Black,
    fontSize: moderateScale(20),
    ...constants.Fonts.Bold,
  },
  genderIcon: {
    height: moderateScale(20),
    width: moderateScale(20),
    left: moderateScale(5),
  },
  singleView: {
    width,
    alignItems: "center",
    paddingTop: moderateScale(20),
  },
  loaderView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
