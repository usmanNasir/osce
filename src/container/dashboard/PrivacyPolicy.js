import React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Text,
  RefreshControl,
  Alert,
  Button,
  Image,
  SafeAreaView
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import _ from "underscore";
import ImageLoad from "../../components/ImageLoad";
import * as AppAction from "../../actions";
import { removeListeners } from "../../utilities/listeners";
import Loader from "./../../components/common/loader";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import constants from "../../constants";
import NoRecord from "../../components/common/NoRecord";
import Card from "../../components/common/Card";
import SideMenu from "../../components/common/SideMenu";
import Header from "../Header";
import { logger } from "./../../utilities/analytics-logger";
import WebView from "react-native-webview";
let removeListener = true;
class PrivacyPolicy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNo: 1
    };
    Navigation.events().bindComponent(this);
  }

  componentWillUnmount() {
    if (removeListener) {
      removeListeners();
    }
  }
  navigateToPrevioudScreen = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "Profile",
        passProps: {},
        options: {
          topBar: {
            visible: false
          }
        }
      }
    });
  };
  renderFooterComponent = () => {};
  //Main rendering method
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Header
          previousComponent={this.navigateToPrevioudScreen}
          text="Privacy Policy"
          border={true}
          navigate={true}
        />
        <WebView source={{ uri: "http://osce.ai/privacy-policy/" }} />
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    user: state.Auth.authData
  };
}
const mapDispatchToProps = dispatch => ({
  AppAction: bindActionCreators(AppAction, dispatch)
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PrivacyPolicy);
//style sheet
const width = constants.BaseStyle.DEVICE_WIDTH;
const styles = StyleSheet.create({
  container: {
    backgroundColor: constants.Colors.appBackgroundColor,
    flex: 1,
    paddingTop: moderateScale(20)
  },
  cardWrapper: {
    width: width / 2 - moderateScale(30),
    margin: moderateScale(10),
    padding: moderateScale(16),
    justifyContent: "space-between",
    alignItems: "flex-start",
    height: width / 2 - moderateScale(30),
    backgroundColor: constants.Colors.White
  },
  testHeading: {
    flex: 3,
    color: constants.Colors.gunPowderShade,
    fontSize: moderateScale(20),
    ...constants.Fonts.Bold,
    textAlign: "left"
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    width: width / 2 - moderateScale(62)
  },
  caseTxt: {
    flex: 4,
    color: constants.Colors.LightGray,
    fontSize: moderateScale(18),
    ...constants.Fonts.Bold,
    textAlign: "left"
  },
  gradeTxt: {
    color: constants.Colors.Black,
    fontSize: moderateScale(32),
    ...constants.Fonts.Bold,
    textAlign: "right",
    bottom: -4
  },
  gradeSign: {
    ...constants.Fonts.Bold,
    color: constants.Colors.Black,
    fontSize: moderateScale(20),
    marginLeft: -4
  },
  loader: {
    position: "absolute",
    marginTop: 10,
    backgroundColor: constants.Colors.LightGray,
    borderRadius: 50,
    padding: 4
  },
  checkedIcon: {
    height: 40,
    width: 40
  },
  bottomView: {
    width: "100%",
    height: 70,
    backgroundColor: "#393939",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 0
  },
  CircleShapeView: {
    margin: 20,
    width: 120,
    height: 120,
    padding: 20,
    borderRadius: 150 / 2,
    backgroundColor: "#E8E8E8",
    justifyContent: "center",
    alignItems: "center"
  },
  close: {
    margin: 5,
    position: "absolute",
    top: moderateScale(160),
    left: moderateScale(90)
  }
});
