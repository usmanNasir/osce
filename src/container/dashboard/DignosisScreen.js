import React from "react";
import {
  View,
  SafeAreaView,
  BackHandler,
  Platform,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import {
  Challenge_Left_UnAttempted,
  Challenge_Completed,
  Activity,
  Time_Spent_Dignosis,
} from "../../constants/Events";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { MixPanelContext } from "../../utilities/MixPanel";
import { DignosisData } from "../../helpers/DiagnosisData";
import constants from "../../constants";
import Header from "../Header";
import ControlledLoader from "../../components/common/ControlledLoader";
import _ from "underscore";
import * as AppAction from "../../actions";


class DignosisScreen extends React.Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
    this.state = {
      Listing: this.props.Listing,
      item: this.props.item,
      messagesList: this.props.messagesList,
      lastMessage: this.props.lastMessage,
      sessionId: "",
      messages: [],
      startTime: null,
      endTime: null,
      itemList: [],
      loading: true,
      dignosisData: [],
      correctDignosis: "",
      selectedDignosis: "",
    };
    Navigation.events().bindComponent(this);
  }
  static options() {
    return {
      topBar: {
        rightButtons: {
          id: "rightButton",
          icon: constants.Images.Common.checkMark,
        },
        leftButtons: {
          id: "backButton",
          icon: constants.Images.Common.backButton,
        },
      },
    };
  }

  componentDidMount() {

    const historyId = this.props.item._id;
    this.setState({
      startTime: new Date().getTime(),
    });
    this.props.AppAction.getSpecificDignosis(historyId).then(() => {
      const list = this.props.Dignosis.specficDignosis;
      let newArray = [];
      if (list?.DignosisList.length > 0) {
        newArray = list?.DignosisList.map((item) => ({
          ...item,
          selected: false,
        }));
      }
      this.setState({
        itemList: newArray,
        dignosisData: this.props.Dignosis,
        loading: false,
        correctDignosis: list?.correctDignosis ? list.correctDignosis : "",
      });
    });
    setTimeout(
      () => this.context.mixPanel.track(Activity.userDignosisActivity),
      3000
    );
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.backPressed
    );
  }

  backPressed = () => {
    let { componentName } = this.state;
    if (componentName == "ChatBot") {
      this.props.AppAction.resetChatBotData();
    }
    this.props.isDifferentStack &&
      this.context.mixPanel.track(Challenge_Left_UnAttempted);
    this.props.isDifferentStack
      ? Navigation.pop("HomeScreen", {
          bottomTab: { visible: true },
          bottomTabs: { visible: true },
        })
      : Navigation.pop("Home", {
          bottomTabs: { visible: true },
          bottomTab: { visible: true },
        });
    return true;
  };

  componentWillUnmount() {
    BackHandler.removeEventListener(this.backHandler);
    const difference = new Date().getTime() - this.state.startTime;
    this.context.mixPanel.track(Time_Spent_Dignosis, {
      TimeSpent: `${difference / 60000} minutes`,
    });
  }

  goToFeedbackPage = async () => {
    const awardDignosisScore =
      this.state.selectedDignosis === this.state.correctDignosis;
    const awardScore = this.state.dignosisData?.specficDignosis?.score
      ? this.state.dignosisData?.specficDignosis?.score
      : 0;
    Navigation.push(this.props.componentId, {
      component: {
        name: "Feedback",
        passProps: {
          ...this.props,
          awardDignosisScore:awardDignosisScore,
          awardScore:awardScore,
        },
        options: {
          topBar: {
            visible: false,
          },
          bottomTabs: {
            visible: false,
            drawBehind: Platform.OS === "android" ? true : false,
          },
        },
      },
    });
  };

  goToChallengeScoreScreen = async () => {
    const awardDignosisScore =
      this.state.selectedDignosis === this.state.correctDignosis;
    const awardScore = this.state.dignosisData?.specficDignosis?.score
      ? this.state.dignosisData?.specficDignosis?.score
      : 0;
    this.context.mixPanel.track(Challenge_Completed);
    const userId = this.props.Auth._id;
    const url = this.props.serverUrl;
    const rankData = await this.props.AppAction.getRank(userId, url);
    Navigation.push(this.props.componentId, {
      component: {
        name: "ChallengeScoreScreen",
        passProps: {
          ...this.props,
          rank: rankData?.rank ? rankData.rank : 1,
          awardDignosisScore:awardDignosisScore,
          awardScore:awardScore,
        },
        options: {
          topBar: {
            visible: false,
          },
          bottomTabs: {
            visible: false,
            drawBehind: Platform.OS === "android" ? true : false,
          },
        },
      },
    });
  };

  gotoNextScreen = () => {
    if (
      this.props.investigationItem &&
      Object.keys(this.props.investigationItem).length > 0
    ) {
      const awardDignosisScore =
        this.state.selectedDignosis === this.state.correctDignosis;
      const awardScore = this.state.dignosisData?.specficDignosis?.score
        ? this.state.dignosisData?.specficDignosis?.score
        : 0;
      Navigation.push(this.props.componentId, {
        component: {
          name: "Investigate",
          passProps: {
            ...this.props,
            awardDignosisScore,
            awardScore,
          },
          options: {
            topBar: {
              visible: false,
            },
            bottomTabs: {
              visible: false,
              drawBehind: Platform.OS === "android" ? true : false,
            },
          },
        },
      });
    } else if (this.props.isChallenge) {
      this.goToChallengeScoreScreen();
    } else {
      this.goToFeedbackPage();
    }
  };

  handleSelect(itemValue) {
    const selectedList = this.state.itemList.map((item) => {
      if (item.text === itemValue.text && !item.selected) {
        item.selected = !itemValue.selected;
      } else if (item.text === itemValue.text && item.selected) {
        item.selected = true;
      } else {
        item.selected = false;
      }
      return item;
    });
    const selectedItem = selectedList.filter((item) => item.selected === true);
    this.setState({
      itemList: selectedList,
      selectedDignosis: selectedItem[0].text,
    });
  }

  render() {
    const previousScreen = this.props.isDifferentStack ? "HomeScreen" : "Home";
    return (
      <SafeAreaView
        style={{
          backgroundColor: constants.Colors.appBackgroundColor,
          flex: 1,
          paddingTop: moderateScale(18),
        }}
      >
        <Header
          previousScreen={previousScreen}
          text={"Diagnosis"}
          border={true}
          showForward
          componentId={this.props.componentId}
          nextComponent={this.gotoNextScreen.bind(this)}
        />
        <View style={{ flex: 1 }}>
          <Text style={styles.heading}>{DignosisData.heading}</Text>

          <ScrollView>
            {this.state.loading ?
             <ControlledLoader showLoader={this.state.loading}/>
            : (
              <View style={{ marginHorizontal: 20, marginTop: 20 }}>
                {this.state.itemList.length > 0 ? (
                  this.state.itemList.map(
                    (item, index) => (
                        <TouchableOpacity
                          key={index}
                          onPress={() => this.handleSelect(item)}
                          style={
                            item?.selected ? styles.itemSelected : styles.item
                          }
                        >
                          <Text
                            style={
                              item.selected
                                ? styles.itemTextSelected
                                : styles.itemText
                            }
                          >
                            {item.text}
                          </Text>
                        </TouchableOpacity>
                      
                    )
                  )
                ) : (
                  <Text>No Dignosis Found For this History</Text>
                )}
              </View>
            )}
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  item: {
    borderRadius: moderateScale(10),
    borderColor: constants.Colors.shadownColor,
    borderWidth: moderateScale(0),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: moderateScale(2) },
    shadowOpacity: moderateScale(0.3),
    shadowRadius: moderateScale(2),
    elevation: moderateScale(3),
    marginBottom: moderateScale(16),
    backgroundColor: "white",
    height: 60,
    paddingHorizontal: 16,
    justifyContent: "center",
  },
  itemSelected: {
    borderRadius: moderateScale(10),
    borderColor: constants.Colors.shadownColor,
    borderWidth: moderateScale(0),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: moderateScale(2) },
    shadowOpacity: moderateScale(0.3),
    shadowRadius: moderateScale(2),
    elevation: moderateScale(3),
    marginBottom: moderateScale(16),
    backgroundColor: constants.Colors.blue,
    height: 60,
    paddingHorizontal: 16,
    justifyContent: "center",
  },
  itemText: {
    fontSize: 20,
    color: constants.Colors.Black,
    justifyContent: "center",
  },
  itemTextSelected: {
    fontSize: 20,
    color: "white",
    justifyContent: "center",
  },
  heading: {
    fontSize: 28,
    fontWeight: "600",
    marginHorizontal: 20,
    color: constants.Colors.Black,
    marginTop: 20,
  },
});

function mapStateToProps(state) {
  return {
    lastMessage: state.ChatBot.lastMessage,
    messagesListing: state.ChatBot.messagesListing,
    sessionId: state.ChatBot.sessionId,
    ChatResponsLoader: state.Loaders.ChatResponsLoader,
    scoreLoader: state.Score.loader,
    Auth: state.Auth.authData,
    serverUrl: state.AppConfig.serverUrl,
    Dignosis: state.Dignosis,
    specficDignosis: state.Dignosis.specficDignosis,
    loading: state.Dignosis.loading,
  };
}

const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DignosisScreen);
