import React from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Alert,
  Image,
  SafeAreaView,
  Modal,
  AsyncStorage,
  Switch,
  ActivityIndicator,
  ScrollView,
  Dimensions,
  TextInput,
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import { WebView } from "react-native-webview";
import { MixPanelContext } from "../../utilities/MixPanel";
import { Activity } from "../../constants/Events";
import { removeListeners } from "../../utilities/listeners";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import constants from "../../constants";
import Header from "../Header";
import _ from "underscore";
import * as AppAction from "../../actions";

let removeListener = true;
const deviceWidth = Dimensions.get("screen").width;

class Profile extends React.Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
    this.state = {
      pageNo: 1,
      toggle: this.props.toggle,
      name: this.props.user?.name,
      email: this.props.user?.email,
      university: this.props.user?.university,
      isEnabled: false,
      openModal: false,
      url: "",
      screenNo: 0,
      openScreen: false,
      visible: false,
      editing:false
    };
    this.isSideDrawerVisible = false;
    Navigation.events().bindComponent(this);
  }

  showModal = (url) => {
    this.setState({
      url: url,
      openModal: !this.state.openModal,
    });
  };

  showScreen = () => {
    this.setState({
      openScreen: !this.state.openScreen,
    });
  };

  ActivityIndicatorElement = () => {
    return (
      <View style={styles.activityIndicatorStyle}>
        <ActivityIndicator color="#009688" size="large" />
      </View>
    );
  };

  logout = () => {
    Alert.alert(
      "Logout",
      "Are you sure want to logout?",
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "Logout",
          onPress: () =>
            this.props.user.gmailId
              ? 
              (this.props.AppAction.logoutfromGoogle(),
                this.props.AppAction.triggerResetChange(),
                this.context.mixPanel.track(Activity.userLogoutActivity),
                this.props.AppAction.triggerResetButtonChange())
              : 
              (this.props.AppAction.logout(),
                this.props.AppAction.triggerResetChange(),
                this.context.mixPanel.track(Activity.userLogoutActivity),
                this.props.AppAction.triggerResetButtonChange()),
        },
      ],
      { cancelable: true }
    );
  };

  clearAppData = async () => {
    try {
      const keys = await AsyncStorage.getAllKeys();
      const updatedKeys = keys.filter((item) => item !== "reduxPersist:Auth");
      await AsyncStorage.multiRemove(updatedKeys);
    } catch (error) {
      console.error("Error clearing app data.");
    }
  };

  async componentDidMount() {
    this.context.mixPanel.track(Activity.userSettingsActivity);
    await AsyncStorage.getItem("Enabled").then((value) =>
      this.props.AppAction.triggerSwitch(this.stringToBoolean(value))
    );
  }

  stringToBoolean = (string) => {
    switch (string) {
      case "true":
      case "yes":
      case "1":
        return true;
      case "false":
      case "no":
      case "0":
      case null:
        return false;
      default:
        return Boolean(string);
    }
  };

  componentWillUnmount() {
    if (removeListener) {
      removeListeners();
    }
  }

  toggoleSwitch = async (value) => {
    this.context.mixPanel.track(Activity.userNotificationsToggleActivity);
    this.props.AppAction.triggerSwitch(value);
    this.props.AppAction.updateNotificationPermission(value,this.state.email)
    this.savetoggle(value);
    if(value){
      this.props.AppAction.updateFcmToken();
    }
  };

  savetoggle = async (value) => {
    if (value === true) {
      await AsyncStorage.setItem("Enabled", "true");
    } else {
      await AsyncStorage.setItem("Enabled", "false");
    }
  };

  openSide = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "Home",
        passProps: { componentName: "Profile" },
        options: {
          topBar: {
            visible: false,
          },
        },
      },
    });
  };

  goToHowitWorks = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "Tutorial",
        passProps: { route: "Profile" },
        options: {
          topBar: {
            visible: false,
          },
        },
      },
    });
  };

  gotoPrivacyPolicy = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "PrivacyPolicy",
        passProps: {},
        options: {
          topBar: {
            visible: false,
          },
        },
      },
    });
  };

  gotoTermsAndConditions = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "TermsAndConditions",
        passProps: {},
        options: {
          topBar: {
            visible: false,
          },
        },
      },
    });
  };

  handleUserUpdate = async  ()=> {
    this.context.mixPanel.track(Activity.userChangeUniverityActivity);
    await this.props.AppAction.updateUniversity(this.state.university,this.state.email);
    this.setState({editing:false});
  }

  render() {
    let user = {
      name: this.state.name,
      email: this.state.email,
      university: this.state.university,
    };
    var initials = user.name.match(/\b\w/g) || [];
    initials = (
      (initials.shift() || "") + (initials.pop() || "")
    ).toUpperCase();

    return (
      <SafeAreaView style={styles.container}>
        <Header
          text="Settings"
          onDrawerPress={this.openSide}
          logout={this.logout.bind(this)}
          hideBack={true}
        />
        <ScrollView>
          <View
            style={{
              marginVertical:
                deviceWidth > 375
                  ? moderateScale(50)
                  : deviceWidth / 2 - moderateScale(185),
            }}
          >
            <View
              style={{
                marginTop: moderateScale(10),
                paddingVertical: moderateScale(10),
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    screenNo: 1,
                    openScreen: true,
                  }),
                  this.context.mixPanel.track(Activity.userAccountDetailsActivity);
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      padding: moderateScale(15),
                      marginLeft: moderateScale(10),
                    }}
                  >
                    Account Details
                  </Text>
                  <Image
                    source={require("../../assets/img/ic_arrow_down.png")}
                    style={{
                      height: 20,
                      width: 20,
                      transform: [{ rotate: "270deg" }],
                      top: 20,
                      right: 20,
                    }}
                  />
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ borderBottomWidth: 1, borderBottomColor: "grey" }} />
            <View
              style={{
                borderBottomWidth: 1,
                borderBottomColor: "grey",
                opacity: 0.3,
              }}
            />
            <View
              style={{
                marginTop: moderateScale(5),
                paddingVertical: moderateScale(10),
              }}
            >
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    screenNo: 2,
                    openScreen: true,
                  })
                }
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      padding: moderateScale(15),
                      marginLeft: moderateScale(10),
                    }}
                  >
                    Notifications
                  </Text>
                  <Image
                    source={require("../../assets/img/ic_arrow_down.png")}
                    style={{
                      height: 20,
                      width: 20,
                      transform: [{ rotate: "270deg" }],
                      top: 20,
                      right: 20,
                    }}
                  />
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ borderBottomWidth: 1, borderBottomColor: "grey" }} />
            <View
              style={{
                marginTop: moderateScale(5),
                paddingVertical: moderateScale(10),
              }}
            >
              <TouchableOpacity
                onPress={() =>
                  this.showModal("http://osce.ai/terms-conditions/")
                }
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      padding: moderateScale(15),
                      marginLeft: moderateScale(10),
                    }}
                  >
                    Terms and Conditions
                  </Text>
                  <Image
                    source={require("../../assets/img/ic_arrow_down.png")}
                    style={{
                      height: 20,
                      width: 20,
                      transform: [{ rotate: "270deg" }],
                      top: 20,
                      right: 20,
                    }}
                  />
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                borderBottomWidth: 1,
                borderBottomColor: "grey",
                opacity: 0.3,
              }}
            />
            <View
              style={{
                marginTop: moderateScale(5),
                paddingVertical: moderateScale(10),
              }}
            >
              <TouchableOpacity
                onPress={() => this.showModal("http://osce.ai/privacy-policy/")}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      padding: moderateScale(15),
                      marginLeft: moderateScale(10),
                    }}
                  >
                    Privacy Policy
                  </Text>
                  <Image
                    source={require("../../assets/img/ic_arrow_down.png")}
                    style={{
                      height: 20,
                      width: 20,
                      transform: [{ rotate: "270deg" }],
                      top: 20,
                      right: 20,
                    }}
                  />
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ borderBottomWidth: 1, borderBottomColor: "grey" }} />
          </View>
          <View>
            <Modal
              animationType="slide"
              transparent={false}
              style={{ marginTop: moderateScale(20) }}
              visible={this.state.openModal}
            >
              <TouchableOpacity
                style={{
                  marginVertical: moderateScale(30),
                  marginRight: moderateScale(20),
                }}
                onPress={() => this.showModal()}
              >
                <Text
                  style={{
                    fontSize: moderateScale(25),
                    fontWeight: "bold",
                    alignSelf: "flex-end",
                  }}
                >
                  X
                </Text>
              </TouchableOpacity>
              <WebView
                source={{ uri: this.state.url }}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                onLoadStart={() => this.setState({ visible: true })}
                onLoad={() => this.setState({ visible: false })}
              />
              {this.state.visible ? this.ActivityIndicatorElement() : null}
            </Modal>
          </View>

          <View>
            <Modal
              animationType="slide"
              transparent={false}
              style={{ marginTop: moderateScale(20) }}
              visible={this.state.openScreen}
            >
              <TouchableOpacity
                style={{
                  marginVertical: moderateScale(30),
                  marginRight: moderateScale(20),
                }}
                onPress={() => this.showScreen()}
              >
                <Text
                  style={{
                    fontSize: moderateScale(25),
                    fontWeight: "bold",
                    alignSelf: "flex-end",
                  }}
                >
                  X
                </Text>
              </TouchableOpacity>
              {this.state.screenNo === 1 && (
                <>
                  <View style={styles.CircleShapeView}>
                    <Text style={{ fontSize: 50 }}>{initials}</Text>
                  </View>
                  <TouchableOpacity style={styles.close}>
                    <Image
                      source={require("../../assets/img/plusForImage.png")}
                      height={30}
                      width={30}
                    />
                  </TouchableOpacity>
                  <View
                    style={{
                      alignItems: "flex-start",
                      paddingTop: moderateScale(5),
                      marginLeft: moderateScale(30),
                    }}
                  >
                    <Text style={{ fontWeight: "bold", fontSize: 20 }}>
                      {user.name}
                    </Text>
                    {this.state.university && !this.state.editing ? (
                      <View style={{flexDirection:'row',justifyContent:'space-between',width:'90%'}}>
                        <Text
                          style={{
                            fontSize: 15,
                            marginBottom:
                              deviceWidth > 400
                                ? moderateScale(20)
                                : moderateScale(10),
                          }}
                        >
                          {this.state.university}
                        </Text>
                        <TouchableOpacity onPress={()=>this.setState({editing:true})}><Text style={{color:constants.Colors.blue,fontSize:15,fontWeight:'800'}}>Edit</Text></TouchableOpacity>
                      </View>
                    ) : (
                      <View style={styles.textInputContainer}>
                        <TextInput
                          placeholder="Enter University"
                          value={this.state.university}
                          onFocus={()=>this.setState({editing:true})}
                          style={{padding:10}}
                          onChangeText={(value) =>
                            this.setState({ university: value })
                          }
                        />
                      </View>
                    )}
                  </View>
                {this.state.editing &&  <TouchableOpacity onPress={this.handleUserUpdate} style={styles.buttonContainer}>
                    <Text style={{fontSize:18,color:'white',fontWeight:'700'}}>Update</Text>
                  </TouchableOpacity>}
                </>
              )}
              {this.state.screenNo === 2 && (
                <View style={{ flexDirection: "row" }}>
                  <Text
                    style={{
                      fontSize: 20,
                      padding: moderateScale(15),
                      marginLeft: moderateScale(10),
                    }}
                  >
                    Notfications
                  </Text>
                  <Switch
                    style={{
                      marginTop: moderateScale(16),
                      marginLeft: deviceWidth / 2 - moderateScale(40),
                    }}
                    trackColor={{ false: "#ffffff", true: "#80b6ec" }}
                    thumbColor={this.state.isEnabled ? "white" : "white"}
                    onValueChange={this.toggoleSwitch}
                    value={this.props.profile.currentValue}
                  />
                </View>
              )}
            </Modal>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: moderateScale(100) / 6,
              padding: 20,
            }}
          >
            <Text style={{ fontSize: 15, textAlign: "center" }}>
              Logged in as {user.email}
            </Text>
            <Text style={{ fontSize: 15 }}>Version 1.0</Text>
          </View>
          <View style={{ paddingVertical: moderateScale(15) }}>
            <TouchableOpacity
              onPress={() => this.logout()}
              style={styles.bottomView}
            >
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 20,
                  color: "white",
                  justifyContent: "center",
                  alignSelf: "center",
                  marginTop: moderateScale(5),
                }}
              >
                LOG OUT
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    user: state.Auth.authData,
    profile: state.switchReducer,
  };
}
const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);


const width = constants.BaseStyle.DEVICE_WIDTH;
const styles = StyleSheet.create({
  container: {
    backgroundColor: constants.Colors.appBackgroundColor,
    flex: 1,
    paddingTop: moderateScale(20),
  },
  activityIndicatorStyle: {
    flex: 1,
    position: "absolute",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "auto",
    marginBottom: "auto",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: "center",
  },
  cardWrapper: {
    width: width / 2 - moderateScale(30),
    margin: moderateScale(10),
    padding: moderateScale(16),
    justifyContent: "space-between",
    alignItems: "flex-start",
    height: width / 2 - moderateScale(30),
    backgroundColor: constants.Colors.White,
  },
  buttonContainer:{
    height:45,
    backgroundColor:constants.Colors.blue,
    marginVertical:30,
    justifyContent:'center',
    alignItems:'center',
    borderColor:constants.Colors.blue,
    borderWidth:1,
    borderRadius:10,
    marginHorizontal:30
  },
  testHeading: {
    flex: 3,
    color: constants.Colors.Black,
    fontSize: moderateScale(20),
    ...constants.Fonts.Bold,
    textAlign: "left",
  },
  textInputContainer:{
    height:45,
    borderWidth:1,
    width:'90%',
    marginTop:10,
    borderRadius:10,
    borderColor:'#ccc',

  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    width: width / 2 - moderateScale(62),
  },
  caseTxt: {
    flex: 4,
    color: constants.Colors.LightGray,
    fontSize: moderateScale(18),
    ...constants.Fonts.Bold,
    textAlign: "left",
  },
  gradeTxt: {
    color: constants.Colors.Black,
    fontSize: moderateScale(32),
    ...constants.Fonts.Bold,
    textAlign: "right",
    bottom: -4,
  },
  gradeSign: {
    ...constants.Fonts.Bold,
    color: constants.Colors.Black,
    fontSize: moderateScale(20),
    marginLeft: -4,
  },
  loader: {
    position: "absolute",
    marginTop: 10,
    backgroundColor: constants.Colors.LightGray,
    borderRadius: 50,
    padding: 4,
  },
  checkedIcon: {
    height: 40,
    width: 40,
  },
  bottomView: {
    width: "90%",
    height: deviceWidth > 375 ? 70 : 60,
    backgroundColor: "#428FDC",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    borderRadius: moderateScale(12),
  },
  CircleShapeView: {
    margin: 20,
    width: 120,
    height: 120,
    padding: 20,
    borderRadius: 150 / 2,
    backgroundColor: "#E8E8E8",
    justifyContent: "center",
    alignItems: "center",
  },
  close: {
    margin: 5,
    position: "absolute",
    top: deviceWidth / 2 - moderateScale(10),
    left: deviceWidth / 2 - moderateScale(95),
  },
});
