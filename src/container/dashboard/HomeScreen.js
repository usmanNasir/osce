import React from "react";
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  Platform,
  Modal,
  Linking,
  AsyncStorage,
} from "react-native";
import {
  Activity,
  App_Launch,
  Challenge_Retry_Click,
  Challenge_Started,
  Challenge_Start_Click,
  News_Card_Clicked,
  Share_News_Click,
} from "../../constants/Events";
import { MixPanelContext } from "../../utilities/MixPanel";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import { navigate } from "../../actions";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { maleIcon, femaleIcon } from "../../assets/";
import { checkVersion } from "../../utilities/updateCheck";
import constants from "../../constants";
import ReloadView from "../../components/common/ReloadView";
import Card from "../../components/common/Card";
import NotifService from "../../utilities/NotifyService";
import ShareIcon from "../../assets/img/share.png";
import Medal1 from "../../assets/img/medal1.png";
import Medal2 from "../../assets/img/medal2.png";
import Medal3 from "../../assets/img/medal3.png";
import moment from "moment";
import LeaderBoardScreen from "./LeaderBoardScreen";
import branch from "react-native-branch";
import InAppUpdateModal from "../../components/common/inAppUpdateModal";
import Share from "react-native-share";
import axios from "axios";
import SpInAppUpdates from "sp-react-native-in-app-updates";
import VersionCheck from "react-native-version-check";
import ControlledLoader from "../../components/common/ControlledLoader";
import * as Progress from "react-native-progress";
import * as AppAction from "../../actions";
import _ from "underscore";

const defaultColor = "#E2E2E2";
const MedalImages = [Medal1, Medal2, Medal3];

const shareHandler = (url, context) => {
  context?.mixPanel?.track(Share_News_Click);
  context?.mixPanel?.track(Activity.userNewsSharingActivity);
  const shareOptions = {
    title: "Share Blog",
    failOnCancel: false,
    urls: [url],
  };

  Share.open(shareOptions)
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      err && console.log(err);
    });
};

const renderCard = ({ item, index, props, state, setState, context }) => {
  let counter = item.counter;
  let challengeText = props.isButtonChange
    ? "Re-take the challenge to boost your score and climb the leaderboard!"
    : "See how your score stacks up against our students from all over the world";

  if (!item) return;
  return (
    <View key={index} style={styles.renderContainer}>
      {state.loader && <ControlledLoader showLoader={state.loader} />}
      {item.title ? (
        <Text
          style={
            item.contentType === "keeplearning"
              ? styles.cardTitleTop
              : item.contentType === "case" ||
                item.contentType === "news" ||
                item.contentType === "blog"
              ? styles.cardCaseTitle
              : styles.cardTitle
          }
        >
          {item.title && item.title}
        </Text>
      ) : null}
      {item.contentType === "case" ||
      item.contentType === "news" ||
      item.contentType === "blog" ? (
        <Card style={styles.caseContainer}>
          <TouchableOpacity
            onPress={() =>
              CaseViewNavigationHandler(
                props,
                item.extraData.contentUrl,
                context
              )
            }
          >
            <View style={styles.cardImageCaseContainer}>
              <Image
                resizeMode="cover"
                style={styles.cardCaseImage}
                source={{ uri: item.image }}
              />
              {item.age ? (
                <View style={styles.ageContainer}>
                  <Text style={styles.ageText}>{item.age}</Text>
                  <Image
                    style={styles.ageImage}
                    source={require("../../assets/img/femenine.png")}
                  />
                </View>
              ) : null}
            </View>
            <View style={styles.cardCaseTextContainer}>
              <Text style={styles.subTitleText}>{item.subTitle}</Text>
              <Text
                style={{
                  ...styles.cardContent,
                  color: constants.Colors.Black,
                  marginTop: 20,
                }}
              >
                {item.content}
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => shareHandler(item.extraData.contentUrl, context)}
              style={styles.shareButton}
            >
              <Text style={styles.shareText}>Share</Text>
              <Image style={styles.shareImage} source={ShareIcon} />
            </TouchableOpacity>
          </TouchableOpacity>
        </Card>
      ) : item.contentType === "LeaderBoard" &&
        item.extraData.shortLeaderBoard.length > 0 ? (
        <Card style={styles.leaderBoardCard}>
          <TouchableOpacity
            onPress={() => LeaderBoardNavigationHandler(props, null, context)}
            style={{}}
          >
            {item.extraData.shortLeaderBoard.length > 0 && (
              <View style={styles.cardCaseTextContainer}>
                <Text style={styles.subTitleText}>{item.subTitle}</Text>
              </View>
            )}
            <View style={styles.shortLeaderBoard}>
              {item.extraData.shortLeaderBoard.length > 0 &&
                item.extraData.shortLeaderBoard.map((item, index) => {
                  return (
                    <View
                      key={index}
                      style={
                        item?.background && item.background === "true"
                          ? styles.selectedUser
                          : styles.UnSelectedUser
                      }
                    >
                      <View
                        style={item?.rank > 3 && { ...styles.rankContainer }}
                      >
                        {item?.rank > 3 ? (
                          <Text style={styles.rankText}>{item?.rank}</Text>
                        ) : (
                          <Image
                            resizeMode="contain"
                            style={{ width: 25, height: 25 }}
                            source={MedalImages[item?.rank - 1]}
                          />
                        )}
                      </View>
                      <View style={styles.userContainer}>
                        <Text style={styles.userText}>
                          {item?.userData.name}
                        </Text>
                        {item?.userData.university && (
                          <Text numberOfLines={1} style={styles.universityText}>
                            {item?.userData.university}
                          </Text>
                        )}
                      </View>
                      <View style={styles.highScoreContainer}>
                        <View style={styles.highScoreContainerText}>
                          <Text style={styles.highScoreText}>
                            {item?.highScore}
                          </Text>
                        </View>
                        <View style={{ marginLeft: 5, marginRight: 2 }}>
                          <Image source={require("../../assets/img/gem.png")} />
                        </View>
                      </View>
                    </View>
                  );
                })}
            </View>
          </TouchableOpacity>
        </Card>
      ) : (
        item.contentType !== "LeaderBoard" && (
          <Card style={styles.cardWrapper}>
            <TouchableOpacity
              disabled={item.contentType === "challenge"}
              onPress={
                item.contentType === "feedback"
                  ? () => FeedBackNavigationHandler(props, context)
                  : item.contentType === "challenge"
                  ? () =>
                      ChallengeNavigationHandler(
                        props,
                        item.id,
                        item.extraData.complaint.iconUrl,
                        item.subTitle,
                        state,
                        item.extraData,
                        context
                      )
                  : item.contentType === "keeplearning"
                  ? () =>
                      lastComplaintNavigationHandler(
                        props,
                        item.id,
                        item.image,
                        item.subTitle,
                        state,
                        item.extraData,
                        context
                      )
                  : {}
              }
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <View
                  style={
                    item.contentType === "challenge" &&
                    item.contentType !== "LeaderBoard"
                      ? {
                          ...styles.cardTextContainer,
                          marginRight: moderateScale(30),
                          paddingBottom: 23,
                        }
                      : styles.cardTextContainer
                  }
                >
                  <Text style={styles.cardSubTitle}>
                    {item.contentType === "feedback" &&
                    item.contentType !== "LeaderBoard"
                      ? props.headingConstants.home_tiles_section4_heading
                      : item.subTitle}
                  </Text>
                  <Text
                    numberOfLines={4}
                    style={
                      item.contentType === "challenge"
                        ? {
                            fontSize: moderateScale(14),
                            color: constants.Colors.Black,
                            paddingTop: 23,
                          }
                        : {
                            ...styles.cardContent,
                            marginTop: 10,
                            color: constants.Colors.Black,
                          }
                    }
                  >
                    {item.contentType === "challenge"
                      ? challengeText
                      : item.content}
                  </Text>
                </View>

                <View
                  style={
                    item.contentType === "challenge"
                      ? styles.cardChallengeImageContainer
                      : styles.cardImageContainer
                  }
                >
                  {item.contentType === "challenge" ? (
                    <View
                      style={
                        item.extraData.challenge[0].case.colour
                          ? {
                              ...styles.roundRing,

                              borderColor:
                                item.extraData.challenge[0].case.colour,
                            }
                          : styles.roundRing
                      }
                    >
                      <View
                        style={
                          item.extraData.challenge[0].case.colour
                            ? {
                                ...styles.roundImageWrap,
                                backgroundColor:
                                  item.extraData.challenge[0].case.colour,
                              }
                            : styles.roundImageWrap
                        }
                      >
                        <Image
                          resizeMode="contain"
                          style={styles.roundImage}
                          source={{ uri: item.image }}
                        />
                      </View>
                    </View>
                  ) : (
                    <Image
                      resizeMode="contain"
                      style={styles.cardImage}
                      source={{ uri: item.image }}
                    />
                  )}
                  {item.age ? (
                    <View style={styles.ageContainer}>
                      <Text style={styles.ageTextBold}>{item.age}</Text>
                      <Image
                        style={{ width: 18, height: 18, marginLeft: 5 }}
                        source={
                          item.extraData.case?.gender === "male"
                            ? maleIcon
                            : femaleIcon
                        }
                      />
                    </View>
                  ) : null}
                </View>
              </View>
              {item.contentType === "challenge" && (
                <View style={styles.challengeContainer}>
                  <TouchableOpacity
                    onPress={() =>
                      ChallengeNavigationHandler(
                        props,
                        item.id,
                        item.extraData.challenge[0].complaint.iconUrl,
                        item.subTitle,
                        state,
                        item.extraData,
                        context
                      )
                    }
                    style={{
                      backgroundColor: props.isButtonChange
                        ? "#E16B6B"
                        : "#3E96ED",
                      width: "40%",
                      justifyContent: "center",
                      alignItems: "center",
                      borderRadius: 10,
                    }}
                  >
                    <Text style={styles.buttonText}>
                      {props.isButtonChange ? "RETRY!" : "START!"}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
              {item.contentType === "challenge" && (
                <View style={{ flexDirection: "row" }}>
                  <View style={styles.nextTextStart}>
                    <Text style={{ ...styles.cardContent }}>
                      {item.content}
                    </Text>
                  </View>
                  <View style={styles.nextText}>
                    <Text style={{ ...styles.cardContent }}>
                      Next Challenge
                    </Text>
                    <Text
                      style={{
                        ...styles.cardContent,
                        color: "#00C325",
                      }}
                    >{`${6 - moment().day()} ${
                      6 - moment().day > 1 ? "Days" : "Day"
                    } ${24 - moment().hour()} Hours`}</Text>
                  </View>
                </View>
              )}
            </TouchableOpacity>
          </Card>
        )
      )}
      {item.contentType === "keeplearning" && (
        <View>
          <Progress.Bar
            progress={
              counter == 3 ? 1 : counter == 2 ? 0.6 : counter == 1 ? 0.3 : 0
            }
            color="lightgreen"
            borderColor="white"
            unfilledColor="lightgrey"
            width={width - 40}
          />
        </View>
      )}
    </View>
  );
};

const ChallengeNavigationHandler = (
  props,
  id,
  iconUrl,
  name,
  state,
  itemData,
  context
) => {
  if (props.isButtonChange) {
    context?.mixPanel?.track(Challenge_Retry_Click);
    context?.mixPanel?.track(Challenge_Started);
  } else {
    context?.mixPanel?.track(Challenge_Start_Click);
    context?.mixPanel?.track(Challenge_Started);
  }
  context?.mixPanel?.track(Activity.userChallengeStartActivity);
  const ComplaintData = {
    _id: id,
    name,
    iconUrl,
  };
  const Grades = state.Grades;
  props.AppAction.triggerPaddingchange();
  Navigation.push(props.componentId, {
    component: {
      name: "Chatbot",
      passProps: {
        item: itemData.challenge[0].case,
        name: name,
        complaintData: ComplaintData,
        Grades: Grades,
        isDifferentStack: true,
        isChallenge: true,
      },
      options: {
        topBar: {
          visible: false,
        },
        bottomTabs: {
          visible: false,
          drawBehind: Platform.OS === "android" ? true : false,
        },
        bottomTab: {
          visible: true,
        },
      },
    },
  });
};

const LeaderBoardNavigationHandler = (props, id = null, context = null) => {
  // context?.mixPanel?.track(Activity.userLeaderBoardActivity);
  // props.AppAction.triggerPaddingchange();
  Navigation.push(props.componentId, {
    component: {
      name: "LeaderBoardScreen",
      passProps: {
        friendId: id,
        ...props,
        isDifferentStack: true,
      },
      options: {
        topBar: {
          visible: false,
        },
        bottomTabs: {
          visible: false,
          drawBehind: Platform.OS === "android" ? true : false,
        },
        bottomTab: {
          visible: false,
        },
      },
    },
  });
};

const lastComplaintNavigationHandler = (
  props,
  id,
  iconUrl,
  name,
  state,
  itemData,
  context = null
) => {
  const item = {
    _id: id,
    iconUrl: iconUrl,
    name: name,
  };
  const Grades = state.Grades;
  props.AppAction.triggerPaddingchange();
  props.AppAction.triggerMarginchange();
  context?.mixPanel?.track(Activity.userKeepLearningActivity);
  Navigation.push(props.componentId, {
    component: {
      name: "ComplaintTree",
      passProps: { item, Grades, isDifferentStack: true },
      options: {
        topBar: {
          visible: false,
        },
        bottomTabs: {
          visible: true,
          drawBehind: Platform.OS === "android" ? true : false,
        },
        bottomTab: {
          visible: true,
        },
      },
    },
  });
};

const FeedBackNavigationHandler = (props, context) => {
  props.AppAction.triggerPaddingchange();
  context?.mixPanel?.track(Activity.userFeedbackActivity);
  Navigation.push(props.componentId, {
    component: {
      name: "FeedBackScreen",
      passProps: {},
      options: {
        topBar: {
          visible: false,
        },
        bottomTabs: {
          drawBehind: Platform.OS === "android" ? true : false,
          visible: false,
        },
        bottomTab: {
          visible: true,
        },
      },
    },
  });
};

const CaseViewNavigationHandler = (props, url, context) => {
  props.AppAction.triggerPaddingchange();
  context?.mixPanel?.track(News_Card_Clicked);
  context?.mixPanel?.track(Activity.userBlogActivity);

  Navigation.push(props.componentId, {
    component: {
      name: "CaseViewScreen",
      passProps: {
        url: url,
      },
      options: {
        topBar: {
          visible: false,
        },
        bottomTabs: {
          visible: false,
        },
        bottomTab: {
          visible: true,
        },
      },
    },
  });
};

const getIp = async () => {
  try {
    const response = await fetch("https://api.ipify.org");
    const ip = response.text();
    return ip;
  } catch (e) {
    throw "Unable to get IP address.";
  }
};

const postMixPanel = async (user, distinctId) => {
  let ipAddress = await getIp();
  if (!ipAddress) {
    ipAddress = "0.0.0.0";
  }

  let body = new URLSearchParams({
    data: `{\n    "$token": "475e25103ef731274d35f1272f68c789",\n    "$distinct_id": "${distinctId}",\n     "$ip": "${ipAddress}",\n    "$set_once": {\n          "$university": "${
      user.university
    }",\n          "$name":"${user.name}",\n       "$email":"${
      user.email
    }"\n\n    }\n}\n`,
  });
  const response = await axios.post(
    "https://api.mixpanel.com/engage#profile-set-once",
    body
  );
};

class HomeScreen extends React.Component {
  static options() {
    return {
      bottombar: {
        drawBehind: Platform.OS === "android" ? true : false,
      },
    };
  }

  static contextType = MixPanelContext;
  abortController = new AbortController();
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      Grades: [],
      gradingList: [],
      item: "",
      _id: "",
      pageNo: 1,
      notifSend: 0,
      notifToggle: "",
      toggle: this.props.toggle,
      loader: true,
      showLeaderBoard: false,
      listData: [],
      showAppUpdate: false,
      onRefreshLoader: false,
    };
    this.localNotify = null;
    this.notif = new NotifService(
      this.onRegister.bind(this),
      this.onNotif.bind(this)
    );
    Navigation.events().bindComponent(this);
  }

  async checkForVersion() {
    const result = await checkVersion();
    this.props.AppAction.getAppStatus().then(() => {
      if (
        Platform.OS === "android" &&
        this.props.appStatus.isUpdateAvailableAndroid
      ) {
        if (result) {
          this.setState({ showAppUpdate: true });
        }
      } else if (
        Platform.OS === "ios" &&
        this.props.appStatus.isUpdateAvailableIOS
      ) {
        const inAppUpdates = new SpInAppUpdates(
          false // isDebug
        );
        const currentVersion = VersionCheck.getCurrentVersion();
        inAppUpdates
          .checkNeedsUpdate({ curVersion: currentVersion })
          .then((result) => {
            if (result.shouldUpdate) {
              this.setState({ showAppUpdate: true });
            }
          });
      }
    });
  }

  onFocusFunction = () => {
    if (this.props.shouldHomeRefresh) {
      this.setState({ loader: true });
      this.props.AppAction.getHomeScreenData(this.props.Auth._id).then(() => {
        this.setState({ listData: this.props.homeContent, loader: false });
        this.props.AppAction.triggerHomeRefreshChange(false);
        this.props.AppAction.triggerPaddingchange();
      });
    }
  };

  getExaminationData = async (complaintId) => {
    let pageNo = this.state.pageNo ? this.state.pageNo : "1";
    let _id = complaintId ? complaintId : "";
    await this.props.AppAction.getExamination({ pageNo, _id });
  };

  getInvestigationData = async (complaintId) => {
    let pageNo = this.state.pageNo ? this.state.pageNo : "1";
    let _id = complaintId ? complaintId : "";
    await this.props.AppAction.getInvestigation({ pageNo, _id });
  };

  componentDidMount() {
    this.props.AppAction.getHomeScreenData(this.props.Auth._id).then(() => {
      this.setState({ listData: this.props.homeContent, loader: false });
    }).then(()=>{
      this.getData();
    })


    if (this.props.isBackgroundLoad && this.props.friendId !== null) {
      const id = this.props.friendId;
      LeaderBoardNavigationHandler(this.props, id);
    }
    branch.setIdentity(this.props.Auth._id);
  
  }
  async getData() {
 
    await this.checkForVersion();
    const token = this.context?.mixPanel?.token;
    const distinctId = await this.context?.mixPanel?.getDistinctId(token);
    postMixPanel(this.props.Auth, distinctId);
    const complaintId = this.props?.homeContent
      ? this.props.homeContent[1]?.id
      : "";
    await this.getExaminationData(complaintId);
    await this.getInvestigationData(complaintId);
    await this.props.AppAction.updateFcmToken();
    await this.props.AppAction.triggerPaddingchange();
    const value = await AsyncStorage.getItem("triggerEvent");
    if (value === "true") {
      this.context?.mixPanel?.track(App_Launch);
      this.context?.mixPanel?.track(Activity.userHomeScreenActivity);
      AsyncStorage.setItem("triggerEvent", "false");
    }
  }

  onRegister(token) {
    this.setState({ registerToken: token.token, fcmRegistered: true });
  }

  onNotif(notif) {
    navigate("HomeScreen");
  }
  handlePerm(perms) {
    Alert.alert("Permissions", JSON.stringify(perms));
  }

  getHeadingConstants() {
    this.props.AppAction.getHeadingConstants(this.props.Auth?.token);
  }
  componentWillUnmount() {
    Linking.removeEventListener("url", this.handleDeepLink);
  }
  componentDidAppear() {
    this.onFocusFunction();
  }

  onRefresh = () => {
    this.setState({ onRefreshLoader: true });
    this.props.AppAction.getHomeScreenData(this.props.Auth._id).then(() => {
      this.setState({
        listData: this.props.homeContent,
        onRefreshLoader: false,
      });
    });
  };

  render() {
    const isAndroid = Platform.OS === "android" ? true : false;
    const contStyle =
      isAndroid && this.props.ispaddingChange && this.props.isMarginChange
        ? styles.containerMarginChange
        : isAndroid && this.props.ispaddingChange
        ? styles.containerPaddingChange
        : styles.container;
    if (this.state.showAppUpdate) {
      return <InAppUpdateModal />;
    } else {
      return (
        <SafeAreaView style={contStyle}>
          <View style={{ flex: 1 }}>
            {this.state.listData && this.state.listData?.length === 0 ? (
              <ControlledLoader loader={this.state.loader} />
            ) : (
              // <ReloadView onPress={this.onRefresh} />
              <FlatList
                extraData={this.state}
                data={this.state.listData}
                renderItem={({ item, index }) =>
                  renderCard({
                    item,
                    index,
                    props: this.props,
                    state: this.state,
                    setState: this.setState,
                    context: this.context,
                  })
                }
                keyExtractor={(item, index) => index.toString()}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.onRefreshLoader}
                    onRefresh={this.onRefresh}
                    progressBackgroundColor={constants.Colors.LightGray}
                  />
                }
              />
            )}
            <View>
              <Modal
                animationType="slide"
                transparent={false}
                style={{ marginTop: moderateScale(20) }}
                visible={this.state.showLeaderBoard}
              >
                <TouchableOpacity
                  style={styles.closeIconContainer}
                  onPress={this.showModal}
                >
                  <Text style={styles.closeIconText}>X</Text>
                </TouchableOpacity>
                <LeaderBoardScreen />
              </Modal>
            </View>
          </View>
        </SafeAreaView>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    appStatus: state.AppStatus.appStatus,
    Auth: state.Auth.authData,
    HistoryListingLoader: state.Loaders.HistoryListingLoader,
    Listing: state.HistoryListing.Listing,
    PaginationListingLoader: state.Loaders.PaginationListingLoader,
    profile: state.switchReducer.currentValue,
    totalPageNo: state.HistoryListing.totalPageNo,
    challenge: state.homeScreenReducer.challenge,
    lastComplaint: state.homeScreenReducer.lastComplaint,
    newsCase: state.homeScreenReducer.newsCase,
    loader: state.homeScreenReducer.loader,
    headingConstants: state.homeScreenReducer.headingConstants,
    homeContent: state.homeScreenReducer.homeContent,
    ispaddingChange: state.style.changePadding,
    isMarginChange: state.style.changeMargin,
    isButtonChange: state.style.changeButton,
    shouldHomeRefresh: state.style.homeRefresh,
  };
};

const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);

const width = constants.BaseStyle.DEVICE_WIDTH;
const styles = StyleSheet.create({
  container: {
    backgroundColor: constants.Colors.appBackgroundColor,
    flex: 1,
  },
  cardWrapper: {
    paddingVertical: moderateScale(16),
    justifyContent: "space-between",
    paddingHorizontal: moderateScale(16),
    marginTop: moderateScale(20),
    backgroundColor: constants.Colors.White,
  },
  containerMarginChange: {
    paddingBottom: moderateScale(50),
    marginBottom: moderateScale(0),
    backgroundColor: constants.Colors.appBackgroundColor,
    flex: 1,
  },
  containerPaddingChange: {
    paddingBottom: moderateScale(130),
    marginBottom: moderateScale(-80),
    backgroundColor: constants.Colors.appBackgroundColor,
    flex: 1,
  },
  cardLeaderBoardWrapper: {
    paddingVertical: moderateScale(16),
    justifyContent: "space-between",
    paddingHorizontal: moderateScale(16),
    marginTop: 0,
    backgroundColor: constants.Colors.White,
  },
  caseTxt: {
    flex: 4,
    color: constants.Colors.LightGray,
    fontSize: moderateScale(18),
    ...constants.Fonts.Bold,
    marginLeft: 10,
  },
  checkedIcon: {
    height: 40,
    width: 40,
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    width: width / 2 - moderateScale(62),
  },
  gradeTxt: {
    color: constants.Colors.Black,
    fontSize: moderateScale(32),
    ...constants.Fonts.Bold,
    textAlign: "right",
    bottom: -4,
  },
  gradeSign: {
    ...constants.Fonts.Bold,
    color: constants.Colors.Black,
    fontSize: moderateScale(20),
    marginLeft: -4,
  },
  loader: {
    position: "absolute",
    marginTop: 40,
    backgroundColor: constants.Colors.LightGray,
    borderRadius: 50,
    padding: 4,
  },
  shadowContainerStyle: {
    //<--- Style with elevation
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.9,
    shadowRadius: 1,
    elevation: 3,
  },
  testHeading: {
    flex: 3,
    color: constants.Colors.Black,
    fontSize: moderateScale(20),
    ...constants.Fonts.Bold,
    textAlign: "left",
  },
  cardTitleTop: {
    color: constants.Colors.Black,
    fontSize: moderateScale(24),
    ...constants.Fonts.Bold,
    paddingTop: moderateScale(40),
  },

  cardTitle: {
    color: constants.Colors.Black,
    fontSize: moderateScale(24),
    ...constants.Fonts.Bold,
    marginTop: moderateScale(30),
  },
  cardCaseTitle: {
    color: constants.Colors.Black,
    fontSize: moderateScale(24),
    ...constants.Fonts.Bold,
    marginTop: moderateScale(18),
  },
  cardSubTitle: {
    color: constants.Colors.Black,
    fontSize: moderateScale(24),
    ...constants.Fonts.Bold,
  },
  cardContent: {
    color: constants.Colors.Black,
    fontSize: moderateScale(16),
  },
  cardTextContainer: {
    flex: 1,
    justifyContent: "space-between",
  },
  cardCaseTextContainer: {
    flex: 1,
    justifyContent: "space-between",
    paddingHorizontal: moderateScale(20),
  },
  cardImageContainer: {
    marginLeft: 10,
  },
  cardChallengeImageContainer: {
    marginLeft: 10,
    paddingTop: 25,
  },
  roundRing: {
    justifyContent: "center",
    alignItems: "center",
    height: moderateScale(84),
    width: moderateScale(84),
    borderRadius: moderateScale(42),
    backgroundColor: "#FFF",
    borderColor: defaultColor,
    borderWidth: 4,
    marginLeft: 10,
  },
  roundImageWrap: {
    justifyContent: "center",
    alignItems: "center",
    height: moderateScale(68),
    width: moderateScale(68),
    borderRadius: moderateScale(39),
    backgroundColor: defaultColor,
  },
  roundImage: {
    height: moderateScale(60),
    width: moderateScale(60),
    borderRadius: moderateScale(30),
    padding: 10,
  },
  cardImageCaseContainer: {
    margin: 0,
  },
  cardImage: {
    height: moderateScale(70),
    width: moderateScale(70),
  },
  cardCaseImage: {
    height: moderateScale(160),
    width: "100%",
    flex: 1,
    borderColor: "white",
    borderRadius: 10,
  },
  ageContainer: {
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 15,
    paddingTop: 5,
  },
  shareButton: {
    marginHorizontal: moderateScale(14),
    alignSelf: "flex-end",
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
  },
  rankContainer: {
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
  },
  closeIconText: {
    fontSize: moderateScale(25),
    fontWeight: "bold",
    alignSelf: "flex-end",
  },
  closeIconContainer: {
    marginVertical: moderateScale(30),
    marginRight: moderateScale(20),
  },
  renderContainer: {
    flex: 1,
    paddingHorizontal: moderateScale(16),
  },
  caseContainer: {
    paddingVertical: moderateScale(16),
    justifyContent: "space-between",
    paddingHorizontal: moderateScale(16),
    marginTop: moderateScale(20),
    backgroundColor: constants.Colors.White,
    paddingHorizontal: 0,
    paddingVertical: 0,
  },
  ageText: {
    fontSize: 15,
    fontWeight: "700",
    color: constants.Colors.Black,
  },
  ageImage: {
    width: 15,
    height: 15,
    marginLeft: 2,
  },
  shareText: {
    fontSize: 16,
    fontWeight: "bold",
    color: constants.Colors.Black,
  },
  shareImage: {
    width: 25,
    height: 25,
    tintColor: constants.Colors.Black,
  },
  subTitleText: {
    color: constants.Colors.Black,
    fontSize: moderateScale(24),
    ...constants.Fonts.Bold,
    marginTop: 20,
  },
  leaderBoardCard: {
    paddingVertical: moderateScale(16),
    justifyContent: "space-between",
    paddingHorizontal: moderateScale(16),
    marginTop: 0,
    backgroundColor: constants.Colors.White,
    paddingHorizontal: 0,
    paddingVertical: 0,
  },
  shortLeaderBoard: {
    marginBottom: moderateScale(16),
    marginLeft: moderateScale(25),
    marginTop: moderateScale(30),
    marginRight: moderateScale(16),
  },
  selectedUser: {
    flexDirection: "row",
    height: moderateScale(40),
    alignItems: "center",
    backgroundColor: "#E2FDFF",
    paddingLeft: moderateScale(5),
    marginBottom: moderateScale(12),
  },
  UnSelectedUser: {
    flexDirection: "row",
    height: moderateScale(40),
    alignItems: "center",
    paddingLeft: moderateScale(5),
    marginBottom: moderateScale(12),
  },
  rankText: {
    fontSize: 14,
    fontWeight: "700",
    color: constants.Colors.Black,
  },
  userContainer: {
    marginLeft: 30,
    width: "55%",
    justifyContent: "center",
    height: 40,
    marginRight: 15,
  },
  userText: {
    fontSize: 16,
    fontWeight: "700",
    color: constants.Colors.Black,
  },
  universityText: {
    fontSize: 12,
    fontWeight: "500",
    color: constants.Colors.Black,
  },
  highScoreContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    width: "20%",
  },
  highScoreContainerText: {
    flex: 1,
    alignSelf: "flex-end",
    width: "15%",
  },
  highScoreText: {
    fontSize: 16,
    fontWeight: "700",
    color: constants.Colors.Black,
    alignSelf: "flex-end",
  },
  nextText: {
    width: "50%",
    alignItems: "flex-end",
  },
  nextTextStart: {
    width: "50%",
    alignItems: "flex-start",
  },
  buttonText: {
    padding: 15,
    color: constants.Colors.White,
    fontSize: 18,
    fontWeight: "700",
  },
  challengeContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 21,
    marginBottom: 43,
  },
  ageTextBold: {
    fontSize: 18,
    fontWeight: "bold",
    color: constants.Colors.Black,
    paddingHorizontal: 1,
  },
});
