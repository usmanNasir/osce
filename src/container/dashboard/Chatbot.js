import React from "react";
import {
  View,
  SafeAreaView,
  Image,
  BackHandler,
  ActivityIndicator,
  Platform,
} from "react-native";
import {
  GiftedChat,
  Send,
  InputToolbar,
  Composer,
} from "react-native-gifted-chat";
import {
  Activity,
  Challenge_Left_UnAttempted,
  Time_Spent_ChatBot,
} from "../../constants/Events";
import { MixPanelContext } from "../../utilities/MixPanel";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import { APPEND_MESAGE } from "../../actionTypes";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { logger } from "./../../utilities/analytics-logger";
import constants from "../../constants";
import Bubble from "../../components/common/Bubble";
import SystemMsg from "../../components/common/systemMsg";
import Spinner from "react-native-spinkit";
import Header from "../Header";
import _ from "underscore";
import * as AppAction from "../../actions";

class Chatbot extends React.Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
    this.state = {
      Listing: this.props.Listing,
      item: this.props.item,
      messagesList: this.props.messagesList,
      lastMessage: this.props.lastMessage,
      sessionId: "",
      messages: [],
      startTime: null,
      endTime: null,
      examinationItem: {},
      investigationItem: {},
    };
    Navigation.events().bindComponent(this);
  }

  static options() {
    return {
      topBar: {
        rightButtons: {
          id: "rightButton",
          icon: constants.Images.Common.Forward,
        },
        leftButtons: {
          id: "backButton",
          icon: constants.Images.Common.backButton,
        },
      },
    };
  }

  componentDidMount() {
    logger.logEventWithData("chatbot", { category: this.props.name });
    let msgList = this.props.item;
    this.setState({
      startTime: new Date().getTime(),
    });
    const systemMsg = [
      {
        _id: 1,
        text: msgList ? msgList.prompt : "",
        createdAt: new Date(),
        user: {
          _id: "bot",
          name: "Bot",
        },
        system: true,
      },
    ];
    Navigation.events().registerComponentDidAppearListener(
      ({ componentName }) => {
        this.setState({ componentName });
        setTimeout(
          () => this.context.mixPanel.track(Activity.userChatActivity),
          3000
        );
      }
    );
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.backPressed
    );
    this.props.AppAction.resetChatBotData();
    this.props.dispatch({ type: APPEND_MESAGE, payload: systemMsg });
    this.getSelectedItems();
  }

  getSelectedItems = () => {
    const examinationItem = this.props.examinationsListing?.examinations?.filter(
      (item) => item.historyId === this.props.item._id
    );
    const investigationItem = this.props.investigationListing?.investigations?.filter(
      (item) => item.historyId === this.props.item._id
    );
    this.setState({
      examinationItem: examinationItem?.length > 0 ? examinationItem[0] : {},
      investigationItem:
        investigationItem?.length > 0 ? investigationItem[0] : {},
    });
  };

  backPressed = () => {
    let { componentName } = this.state;
    if (componentName == "Chatbot") {
      this.props.AppAction.resetChatBotData();
      this.props.AppAction.resetChatBotDataInvestigation();
      this.props.AppAction.resetChatBotDataExam();
    }
    this.setState({ componentName: null });
    this.props.isDifferentStack &&
      this.context.mixPanel.track(Challenge_Left_UnAttempted);
    this.props.isDifferentStack
      ? Navigation.pop("HomeScreen", {
          bottomTab: { visible: true },
          bottomTabs: { visible: true },
        })
      : Navigation.pop("Home", {
          bottomTabs: { visible: true },
          bottomTab: { visible: true },
        });
    return true;
  };

  componentWillUnmount() {
    BackHandler.removeEventListener(this.backHandler);
    const difference = new Date().getTime() - this.state.startTime;
    this.context.mixPanel.track(Time_Spent_ChatBot, {
      TimeSpent: `${difference / 60000} minutes`,
    });
    this.props.AppAction.resetChatBotData();
    this.setState({ componentName: null });
  }

  onSend(messages = []) {
    let { item } = this.props;
    let { _id } = item;
    let newMessage = [
      {
        _id: _.uniqueId("OSCE_"),
        text: messages[0].text,
        createdAt: new Date(),
        user: {
          _id: "you",
          name: "You",
        },
      },
    ];
    this.props.dispatch({ type: APPEND_MESAGE, payload: newMessage });
    let inputModel = {
      query: messages[0].text,
      dialogflowCredentials: this.props.item.dialogflowCredentials[0],
      historyId: _id,
    };
    if (this.props.sessionId) {
      inputModel = {
        ...inputModel,
        sessionId: this.props.sessionId,
      };
    }
    this.props.AppAction.sendMessage(inputModel, this.state.item._id);
  }

  renderComposer(props) {
    return (
      <Composer
        {...props}
        textInputStyle={{
          backgroundColor: constants.Colors.White,
          borderRadius: 50,
          borderWidth: 0.1,
          borderColor: constants.Colors.placehoder,
          paddingHorizontal: moderateScale(20),
          marginHorizontal: moderateScale(20),
          paddingTop: moderateScale(7),
        }}
      />
    );
  }

  gotoNextScreen = () => {
    if (
      this.state.examinationItem &&
      Object.keys(this.state.examinationItem).length > 0
    ) {
      Navigation.push(this.props.componentId, {
        component: {
          name: "Examination",
          passProps: {
            ...this.props,
            sessionId: this.props?.sessionId,
            examinationItem: this.state.examinationItem,
            investigationItem: this.state.investigationItem,
            historyId: this.props.item._id,
          },
          options: {
            topBar: {
              visible: false,
            },
            bottomTabs: {
              visible: false,
              drawBehind: Platform.OS === "android" ? true : false,
            },
          },
        },
      });
    } else {
      Navigation.push(this.props.componentId, {
        component: {
          name: "Dignosis",
          passProps: {
            ...this.props,
            historyId: this.props.item._id,
          },
          options: {
            topBar: {
              visible: false,
            },
            bottomTabs: {
              visible: false,
              drawBehind: Platform.OS === "android" ? true : false,
            },
          },
        },
      });
    }
  };

  render() {
    let { messagesListing, ChatResponsLoader, scoreLoader } = this.props;
    return (
      <SafeAreaView
        style={{
          backgroundColor: constants.Colors.appBackgroundColor,
          flex: 1,
          paddingTop: moderateScale(18),
        }}
      >
        <Header
          mixPanelEvent={
            this.props.isDifferentStack
              ? () => this.context.mixPanel.track(Challenge_Left_UnAttempted)
              : () => {}
          }
          navigate={true}
          previousComponent={this.backPressed}
          text={"History"}
          border={true}
          showForward
          componentId={this.props.componentId}
          nextComponent={this.gotoNextScreen.bind(this)}
        />

        {scoreLoader ? (
          <ActivityIndicator
            size="large"
            style={{ flex: 1 }}
            color={constants.Colors.gunPowderShade}
          />
        ) : (
          <View
            style={{
              marginTop: moderateScale(5),
              backgroundColor: constants.Colors.appBackgroundColor,
              flex: 1,
            }}
          >
            <GiftedChat
              messages={messagesListing}
              onSend={(messages) => this.onSend(messages)}
              keyboardShouldPersistTaps={"handled"}
              minInputToolbarHeight={60}
              user={{
                _id: "you",
              }}
              listViewProps={{ marginBottom: moderateScale(20) }}
              alwaysShowSend={true}
              renderAvatar={null}
              renderFooter={() => {
                return (
                  <Spinner
                    style={{ marginLeft: moderateScale(10) }}
                    isVisible={ChatResponsLoader}
                    size={50}
                    type={"ThreeBounce"}
                    color={constants.Colors.LightGray}
                  />
                );
              }}
              renderBubble={(res) => {
                return res.currentMessage.user._id == "you" ? (
                  <Bubble
                    text={res.currentMessage.text}
                    imageUri={constants.Images.Common.meBubble}
                    color={"#c9fec1"}
                    type={1}
                  />
                ) : (
                  <Bubble
                    text={res.currentMessage.text}
                    imageUri={constants.Images.Common.youBubble}
                    color={"#ffffff"}
                    type={2}
                  />
                );
              }}
              renderInputToolbar={(props) => {
                return (
                  <InputToolbar
                    {...props}
                    containerStyle={{
                      backgroundColor: constants.Colors.ChatFooterColor,
                      borderWidth: 0,
                      paddingVertical: 9,
                    }}
                  />
                );
              }}
              renderSystemMessage={(res) => {
                return <SystemMsg text={res.currentMessage.text} />;
              }}
              renderSend={(props) => (
                <Send {...props}>
                  <View
                    style={{
                      borderRadius: 70,
                      marginRight: moderateScale(10),
                      padding: moderateScale(10),
                      backgroundColor: constants.Colors.ChatBlueColor,
                      marginBottom: -5,
                    }}
                  >
                    <Image
                      style={{ width: moderateScale(18) }}
                      source={constants.Images.Common.sendButton}
                      resizeMode={"center"}
                    />
                  </View>
                </Send>
              )}
              renderComposer={(props) => this.renderComposer(props)}
            />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    examinationsListing: state.ExaminationListing.examinationListing,
    investigationListing: state.InvestigationListing.investigationListing,
    lastMessage: state.ChatBot.lastMessage,
    messagesListing: state.ChatBot.messagesListing,
    sessionId: state.ChatBot.sessionId,
    ChatResponsLoader: state.Loaders.ChatResponsLoader,
    scoreLoader: state.Score.loader,
    Auth: state.Auth.authData,
    serverUrl: state.AppConfig.serverUrl,
  };
}

const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chatbot);
