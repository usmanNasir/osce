import React from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
} from "react-native";
import { connect } from "react-redux";
import { Activity } from "../../constants/Events";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { MixPanelContext } from "../../utilities/MixPanel";
import constants from "../../constants";
import Medal1 from "../../assets/img/medal1.png";
import Medal2 from "../../assets/img/medal2.png";
import Medal3 from "../../assets/img/medal3.png";
import _ from "underscore";
import * as AppAction from "../../actions";

const MedalImages = [Medal1, Medal2, Medal3];
class LeaderboardScreen extends React.Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
    this.state = {
      pageNo: 1,
    };
  }

  componentDidMount() {
    this.context?.mixPanel?.track(Activity.userLeaderBoardActivity);
    this.props.AppAction.triggerPaddingchange();
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerTextView}>
            <Text style={styles.headerText}>Highscores</Text>
            <TouchableOpacity
              style={{
                position: "absolute",
                alignItems: "center",
                justifyContent: "center",
                width: 25,
                height: 25,
                right: 30,
              }}
              onPress={() => Navigation.pop("HomeScreen", { friendId: null })}
            >
              <Image
                height={25}
                width={25}
                source={require("../../assets/img/closeIcon.png")}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ marginTop: moderateScale(20) }}>
          {this.props.leaderboardArray.leaderBoard.map((item, index) => {
            const showFriendBackground = this.props.friendId === item.userId;
            if (index < 3)
              return (
                <View
                key={index}
                  style={
                    item.background === "true"
                      ? styles.itemContainerBackground
                      : showFriendBackground
                      ? styles.friendBackground
                      : styles.itemContainer
                  }
                >
                  <Image
                    resizeMode="contain"
                    style={{ width: 25, height: 25 }}
                    source={MedalImages[item.rank - 1]}
                  />
                  <View
                    style={{
                      marginLeft: 30,
                      width: "60%",
                      justifyContent: "center",
                      height: 40,
                      marginRight: 15,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 18,
                        fontWeight: "700",
                        color: constants.Colors.Black,
                      }}
                    >
                      {item.userData?.name}
                    </Text>
                    {item.userData?.university && (
                      <Text
                        numberOfLines={1}
                        style={{
                          fontSize: 14,
                          fontWeight: "500",
                          color: constants.Colors.Black,
                        }}
                      >
                        {item.userData?.university}
                      </Text>
                    )}
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                      width: "20%",
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        alignSelf: "flex-end",
                        width: "15%",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 18,
                          fontWeight: "700",
                          alignSelf: "flex-end",
                          color: constants.Colors.Black,
                        }}
                      >
                        {item.highScore}
                      </Text>
                    </View>
                    <View style={{ marginLeft: 5, marginRight: 2 }}>
                      <Image
                        style={{}}
                        source={require("../../assets/img/gem.png")}
                      />
                    </View>
                  </View>
                </View>
              );
          })}
        </View>
        <ScrollView showsVerticalScrollIndicator>
          <View>
            {this.props.leaderboardArray.leaderBoard.map((item, index) => {
              const showFriendBackground = this.props.friendId === item.userId;
              if (index >= 3)
                return (
                  <View
                  key={index}
                    style={
                      item.background === "true"
                        ? styles.itemContainerBackground
                        : showFriendBackground
                        ? styles.friendBackground
                        : styles.itemContainer
                    }
                  >
                    <View style={item.rank > 3 && styles.rankContainer}>
                      <Text style={{ fontSize: 16, fontWeight: "700" }}>
                        {item.rank}
                      </Text>
                    </View>
                    <View
                      style={{
                        marginLeft: 30,
                        width: "60%",
                        justifyContent: "center",
                        height: 40,
                        marginRight: 15,
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 18,
                          fontWeight: "700",
                          color: constants.Colors.Black,
                        }}
                      >
                        {item.userData.name}
                      </Text>
                      {item.userData?.university && (
                        <Text
                          numberOfLines={1}
                          style={{
                            fontSize: 14,
                            fontWeight: "500",
                            color: constants.Colors.Black,
                          }}
                        >
                          {item.userData?.university}
                        </Text>
                      )}
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        width: "20%",
                      }}
                    >
                      <View
                        style={{
                          flex: 1,
                          alignSelf: "flex-end",
                          width: "15%",
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 18,
                            fontWeight: "700",
                            alignSelf: "flex-end",
                            color: constants.Colors.Black,
                          }}
                        >
                          {item.highScore === 0
                            ? `  ${item.highScore}`
                            : item.highScore}
                        </Text>
                      </View>

                      <View style={{ marginLeft: 5, marginRight: 2 }}>
                        <Image
                          style={{}}
                          source={require("../../assets/img/gem.png")}
                        />
                      </View>
                    </View>
                  </View>
                );
            })}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    user: state.Auth.authData,
    leaderboardArray:
      state.homeScreenReducer.homeContent.length === 4
        ? state.homeScreenReducer.homeContent[1].extraData
        : state.homeScreenReducer.homeContent[2].extraData,
  };
}
const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LeaderboardScreen);

const styles = StyleSheet.create({
  container: {
    backgroundColor: constants.Colors.appBackgroundColor,
    flex: 1,
  },
  header: {
    height: moderateScale(60),
    flexDirection: "row",
    borderBottomColor: constants.Colors.shadownColor,
    borderBottomWidth: 0.5,
    shadowColor: constants.Colors.DarkBlack,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0,
    shadowRadius: 1,
    elevation: 2,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    backgroundColor: constants.Colors.appBackgroundColor,
  },
  headerText: {
    color: constants.Colors.Black,
    fontSize: moderateScale(22),
    fontWeight: "bold",
  },
  headerTextView: {
    justifyContent: "center",
    alignItems: "center",
    height: moderateScale(50),
    flexDirection: "row",
    width: "100%",
  },
  rankContainer: {
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
  },
  itemContainer: {
    flexDirection: "row",
    height: moderateScale(45),
    alignItems: "center",
    marginHorizontal: moderateScale(30),
    marginBottom: moderateScale(12),
    paddingRight: 15,
    paddingLeft: 5,
  },
  itemContainerBackground: {
    flexDirection: "row",
    height: moderateScale(45),
    alignItems: "center",
    backgroundColor: "#E2FDFF",
    marginHorizontal: moderateScale(30),
    marginBottom: moderateScale(12),
    paddingRight: 15,
    paddingLeft: 5,
  },
  friendBackground: {
    flexDirection: "row",
    height: moderateScale(45),
    alignItems: "center",
    backgroundColor: "#FFFEE2",
    marginHorizontal: moderateScale(30),
    marginBottom: moderateScale(12),
    paddingRight: 15,
    paddingLeft: 5,
  },
});
