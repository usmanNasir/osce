import React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Text,
  RefreshControl,
  Alert,
  SafeAreaView,
  AsyncStorage,
} from "react-native";
import {
  Activity,
  Attempted_Notification_Clicked,
  Not_Attempted_Notification_Clicked,
  Weekly_Challenge_Notification_Clicked,
} from "../../constants/Events";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { logger } from "./../../utilities/analytics-logger";
import { MixPanelContext } from "../../utilities/MixPanel";
import PushNotification from "react-native-push-notification";
import ImageLoad from "../../components/ImageLoad";
import Loader from "./../../components/common/loader";
import constants from "../../constants";
import ReloadView from "../../components/common/ReloadView";
import Card from "../../components/common/Card";
import Profile from "./Profile";
import Header from "../Header";
import NotifService from "../../utilities/NotifyService";
import * as AppAction from "../../actions";
import * as Progress from "react-native-progress";
import _ from "underscore";

class Home extends React.Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
    this.state = {
      pageNo: 1,
      toggle: this.props.toggle,
      item: "",
      _id: "",
      activeIndex: 0,
      Grades: [],
      gradingList: [],
    };
    this.localNotify = null;
    this.notif = new NotifService(
      this.onRegister.bind(this),
      this.onNotif.bind(this)
    );
    this.isSideDrawerVisible = false;
    Navigation.events().bindComponent(this);
  }

  logout = () => {
    Alert.alert(
      "Logout",
      "Are you sure want to logout?",
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "Logout",
          onPress: () =>
            this.props.Auth.type === "general"
              ? this.props.AppAction.logout()
              : this.props.AppAction.logoutfromGoogle(),
        },
      ],
      { cancelable: true }
    );
  };

  async componentDidMount() {
    
    logger.logEvent("Viewed" + "Home");
    let { pageNo } = this.state;
    this.props.AppAction.HistoryListing(pageNo);
    this.getDaataa();
    this.toggleData();
    this.navigationEventListener = Navigation.events().bindComponent(this);
    this.context.mixPanel.track(Activity.userLearnScreenActivity);
  }
  // componentDidAppear() {
  //   this.getDaataa();
  // }

  toggleData = async () => {
    let toggle = await AsyncStorage.getItem("Enabled");
    this.setState({
      notifToggle: this.stringToBoolean(toggle),
    });
  };

  stringToBoolean = (string) => {
    switch (string) {
      case "true":
      case "yes":
      case "1":
        return true;
      case "false":
      case "no":
      case "0":
      case null:
        return false;
      default:
        return Boolean(string);
    }
  };

  onRegister(token) {
    this.setState({ registerToken: token.token, fcmRegistered: true });
  }

  onNotif(notif) {
    PushNotification.presentLocalNotification(notif);
    if (notif.userInteraction) {
      if (notif.data.type === "Attempted") {
        this.context.mixPanel.track(Attempted_Notification_Clicked);
      } else if (notif.data.type === "NotAttempted") {
        this.context.mixPanel.track(Not_Attempted_Notification_Clicked);
      } else {
        this.context.mixPanel.track(Weekly_Challenge_Notification_Clicked);
      }
    }
  }

  handlePerm(perms) {
    Alert.alert("Permissions", JSON.stringify(perms));
  }

  getDaataa = async () => {
    let response = await fetch("http://35.178.22.72:4571/history/get", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.Auth.token,
      },
    });
    let responseJson = await response.json();
    this.setState(
      {
        gradingList: responseJson.data.histories,
      },
      function() {
        this.getProgress();
      }
    );
  };

  onRefresh = () => {
    this.setState({ pageNo: 1 }, () => {
      let { pageNo } = this.state;
      this.props.AppAction.HistoryListing(pageNo);
    });
  };

  onEndReached = () => {
    if (!this.canAction) return;
    const IS_PAGINATION = true;
    let { pageNo } = this.state;
    let { totalPageNo } = this.props;
    if (pageNo < totalPageNo) {
      pageNo++;
      this.setState({ pageNo }, () => {
        this.props.AppAction.HistoryListing(this.state.pageNo, IS_PAGINATION);
      });
    }
  };

  navigateToNextScreen = (item, Grades) => {
    this.context.mixPanel.track(Activity.userBotComplaintActivity);
    Navigation.push(this.props.componentId, {
      component: {
        name: "ComplaintTree",
        passProps: { item, Grades },
        options: {
          topBar: {
            visible: false,
          },
          bottomTabs: {
            drawBehind: false,
            visible: true,
          },
          bottomTab: {
            visible: true,
          },
        },
      },
    });
  };

  getProgress = () => {
    let Data = this.state.gradingList;
    function onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
    }
    let newArr = [];
    let newArr2 = [];
    if (Data != undefined) {
      for (var i = 0; i < Data.length; i++) {
        newArr[i] = Data[i].complaintId;
      }
      for (var i = 0; i < Data.length; i++) {
        newArr2.push({
          complaintId: Data[i].complaintId,
          grade: Data[i].grade,
        });
      }
      let gradingArrayList = [];
      var unique = newArr.filter(onlyUnique);
      for (var j = 0; j < unique.length; j++) {
        gradingArrayList.push({
          count: 0,
          complaintId: unique[j],
        });
      }
      for (var j = 0; j < gradingArrayList.length; j++) {
        for (var k = 0; k < newArr2.length; k++) {
          if (unique[j] == newArr2[k].complaintId) {
            if (
              newArr2[k].grade == "A" ||
              newArr2[k].grade == "B" ||
              newArr2[k].grade == "C"
            ) {
              gradingArrayList[j].count++;
            }
          }
        }
      }
      this.state.Grades = gradingArrayList;
    } 
  };
  //this will render particular history item
  renderCardRow = ({ item,index }) => {
    let { GradingList } = this.props;
    const { iconUrl = "", name = "", _id = "", thumbnailUrl = null } = item;
    let indexOfRating = _.findIndex(GradingList, { historyId: _id });
    let { Grades } = this.state;
    let counter = 0;
    for (let i = 0; i < Grades.length; i++) {
      if (item._id == Grades[i].complaintId) {
        counter = Grades[i].count;
      }
    }
    let Rating = "";
    let Plus = "";
    if (indexOfRating > -1) {
      let obj = GradingList[indexOfRating];
      Rating = obj.finalScore;
      Plus = obj.Sign;
      this.setState({ _id: _id });
    }
    return (
      <TouchableOpacity
      key={index}
        onPress={() => {
          this.navigateToNextScreen(item, Grades);
        }}
      >
        <Card style={styles.cardWrapper}>
          <Text numberOfLines={3} style={styles.testHeading}>
            {name}
          </Text>
          <View style={styles.footer}>
            <ImageLoad
              isShowActivity={false}
              style={{ height: moderateScale(60), width: moderateScale(60) }}
              placeholderStyle={{
                height: moderateScale(60),
                width: moderateScale(60),
              }}
              loadingStyle={{ size: "small", color: "blak" }}
              source={{ uri: iconUrl }}
              placeholderSource={{ uri: thumbnailUrl ? thumbnailUrl : null }}
              resizeMode="contain"
            />
          </View>
          <View style={{ marginTop: 10 }}>
            <Progress.Bar
              progress={
                counter == 3 ? 1 : counter == 2 ? 0.6 : counter == 1 ? 0.3 : 0
              }
              color="lightgreen"
              borderColor="white"
              unfilledColor="lightgrey"
              width={width / 2 - moderateScale(62)}
            />
          </View>
        </Card>
      </TouchableOpacity>
    );
  };

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20; // how far from the bottom
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

 
  render() {
    let { Listing, HistoryListingLoader, PaginationListingLoader } = this.props;

    return this.state.toggle ? (
      <SafeAreaView style={styles.container}>
        <Profile />
      </SafeAreaView>
    ) : (
      <SafeAreaView style={styles.container}>
        <Header
          previousScreen="Home"
          home="Home"
          hideback="true"
          text="osce ai"
          border={true}
          logout={this.logout.bind(this)}
        />
        <View
          style={styles.contentContainer}
        >

          {Listing && Listing?.length === 0 ? (
            <ReloadView onPress={this.onRefresh} />
          ) : (
            <FlatList
              ref={(ref) => {
                this.ListView_Ref = ref;
              }}
              data={Listing}
              numColumns={2}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item, index) => item.toString() + index.toString()}
              renderItem={this.renderCardRow}
              refreshControl={
                <RefreshControl
                  refreshing={HistoryListingLoader}
                  onRefresh={this.onRefresh}
                  progressBackgroundColor={constants.Colors.LightGray}
                />
              }
              onEndReached={this.onEndReached}
              ListFooterComponent={
                PaginationListingLoader ? (
                  <View style={{ marginBottom: 20 }}>
                    <Loader />
                  </View>
                ) : null
              }
              ListEmptyComponent={
                !HistoryListingLoader && !Listing.length ? (
                  <View style={{ marginTop: 30 }}>
                    <ReloadView onPress={this.onRefresh} />
                  </View>
                ) : (
                  <View>
                    <Text>Loading</Text>
                  </View>
                )
              }
              onEndReachedThreshold={100}
              onScrollBeginDrag={() => {
                this.canAction = false;
              }}
              onScrollEndDrag={() => {
                this.canAction = false;
              }}
              onMomentumScrollBegin={() => {
                this.canAction = false;
              }}
              onMomentumScrollEnd={() => {
                this.canAction = false;
              }}
            />
          )}
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    Listing: state.HistoryListing.Listing,
    totalPageNo: state.HistoryListing.totalPageNo,
    HistoryListingLoader: state.Loaders.HistoryListingLoader,
    PaginationListingLoader: state.Loaders.PaginationListingLoader,
    Auth: state.Auth.authData,
    profile: state.switchReducer.currentValue,
  };
}

const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);

//style sheet
const width = constants.BaseStyle.DEVICE_WIDTH;

const styles = StyleSheet.create({
  container: {
    backgroundColor: constants.Colors.appBackgroundColor,
    flex: 1,
    paddingTop: moderateScale(20),
  },
  cardWrapper: {
    width: width / 2 - moderateScale(30),
    margin: moderateScale(10),
    padding: moderateScale(16),
    justifyContent: "space-between",
    alignItems: "flex-start",
    height: width / 2 - moderateScale(20),
    backgroundColor: constants.Colors.White,
  },
  testHeading: {
    flex: 3,
    color: constants.Colors.Black,
    fontSize: moderateScale(20),
    ...constants.Fonts.Bold,
    textAlign: "left",
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    width: width / 2 - moderateScale(62),
  },
  caseTxt: {
    flex: 4,
    color: constants.Colors.LightGray,
    fontSize: moderateScale(18),
    ...constants.Fonts.Bold,
    textAlign: "left",
  },
  gradeTxt: {
    color: constants.Colors.Black,
    fontSize: moderateScale(32),
    ...constants.Fonts.Bold,
    textAlign: "right",
    bottom: -4,
  },
  gradeSign: {
    ...constants.Fonts.Bold,
    color: constants.Colors.Black,
    fontSize: moderateScale(20),
    marginLeft: -4,
  },
  loader: {
    position: "absolute",
    marginTop: 10,
    backgroundColor: constants.Colors.LightGray,
    borderRadius: 50,
    padding: 4,
  },
  checkedIcon: {
    height: 40,
    width: 40,
  },
  shadowContainerStyle: {
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.9,
    shadowRadius: 1,
    elevation: 3,
  },
  contentContainer:{
    alignItems: "center",
    flex: 1,
    paddingTop: moderateScale(5),
  }
});
