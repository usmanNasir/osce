import React from "react";
import {
  View,
  SafeAreaView,
  Image,
  BackHandler,
  ActivityIndicator,
  Platform,
  StyleSheet,
  TouchableOpacity,
  Text,
  Modal,
  Dimensions,
} from "react-native";
import {
  GiftedChat,
  Send,
  InputToolbar,
  Composer,
} from "react-native-gifted-chat";
import {
  Activity,
  Challenge_Completed,
  Challenge_Left_UnAttempted,
  Time_Spent_Examination,
} from "../../constants/Events";
import {
  APPEND_MESSAGE_EXAMINATION,
  RESET_CHAT_BOT_DATA_EXAMINATION,
} from "../../actionTypes";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { logger } from "./../../utilities/analytics-logger";
import { MixPanelContext } from "../../utilities/MixPanel";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import ImageViewer from "react-native-image-zoom-viewer";
import Slider from "@react-native-community/slider";
import thumb from "../../assets/img/thumb.png";
import constants from "../../constants";
import Bubble from "../../components/common/Bubble";
import SystemMsg from "../../components/common/systemMsg";
import Spinner from "react-native-spinkit";
import Header from "../Header";
import Sound from "react-native-sound";
Sound.setCategory("Playback");
import _ from "underscore";
import * as AppAction from "../../actions";
import { style } from "../../reducers";

let playerInterval = null;
class ExaminationScreen extends React.Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
    this.state = {
      Listing: this.props.Listing,
      item: this.props.item,
      messagesList: this.props.messagesList,
      lastMessage: this.props.lastMessage,
      sessionId: "",
      messages: [],
      startTime: null,
      endTime: null,
      isAudioPlaying: false,
      wasAudioPlaying: false,
      isAudioLoaded: false,
      isAudioClicked: false,
      currentAudioMessage: null,
      audio: null,
      duration: 0,
      progress: 0,
      showImage: false,
      clickedImage: null,
    };
    this.soundPlayerRef = React.createRef();
    Navigation.events().bindComponent(this);
  }

  static options() {
    return {
      topBar: {
        rightButtons: {
          id: "rightButton",
          icon: constants.Images.Common.Forward,
        },
        leftButtons: {
          id: "backButton",
          icon: constants.Images.Common.backButton,
        },
      },
    };
  }

  componentDidMount() {
    logger.logEventWithData("chatbot", { category: this.props.name });
    let msgList = this.props.examinationItem;
    this.setState({
      startTime: new Date().getTime(),
    });
    const systemMsg = [
      {
        _id: 1,
        text: msgList ? msgList.prompt : "",
        createdAt: new Date(),
        user: {
          _id: "bot",
          name: "Bot",
        },
        system: true,
      },
    ];

    Navigation.events().registerComponentDidAppearListener(
      ({ componentName }) => {
        this.setState({ componentName });
        if (
          this.props.messagesListing &&
          this.props.messagesListing.length <= 0
        ) {
          this.props.dispatch({ type: RESET_CHAT_BOT_DATA_EXAMINATION });
          this.props.dispatch({
            type: APPEND_MESSAGE_EXAMINATION,
            payload: systemMsg,
          });
        }

        setTimeout(
          () =>
            this.context.mixPanel.track(Activity.userExaminationChatActivity),
          3000
        );
      }
    );
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.backPressed
    );
  }

  backPressed = async () => {
    await this.soundStop();
    this.props.isDifferentStack &&
      this.context.mixPanel.track(Challenge_Left_UnAttempted);
    this.props.isDifferentStack
      ? Navigation.pop("HomeScreen", {
          bottomTab: { visible: true },
          bottomTabs: { visible: true },
        })
      : Navigation.pop("Home", {
          bottomTabs: { visible: true },
          bottomTab: { visible: true },
        });
    return true;
  };

  componentWillUnmount() {
    this.soundStop().then(() => {
      BackHandler.removeEventListener(this.backHandler);
    });
    const difference = new Date().getTime() - this.state.startTime;
    this.context.mixPanel.track(Time_Spent_Examination, {
      TimeSpent: `${difference / 60000} minutes`,
    });
  }

  async soundStop() {
    this.soundPlayerRef.current?.stop();
    if (playerInterval) clearInterval(playerInterval);
    this.soundPlayerRef.current = null;
    if (this.state.isAudioPlaying) {
      this.setState({
        isAudioPlaying: false,
        currentAudioMessage: null,
        progress: 0,
        audio: null,
      });
    }
  }

  async pauseSound(url, id) {
    if (
      this.state.currentAudioMessage &&
      this.state.currentAudioMessage == id
    ) {
      if (playerInterval) clearInterval(playerInterval);
      await this.soundPlayerRef.current.pause();
      this.setState({ isAudioPlaying: false });
    } else {
      if (this.state.isAudioPlaying) {
        this.soundPlayerRef.current?.stop();
        this.soundPlayerRef.current = null;
        this.setState({
          isAudioPlaying: false,
          progress: 0,
          audio: null,
          wasAudioPlaying: false,
        });
        if (playerInterval) clearInterval(playerInterval);
      }
      this.PlaySound(url, id);
    }
  }

  async ResumeSound() {
    if (playerInterval) clearInterval(playerInterval);
    playerInterval = setInterval(() => {
      if (!this.soundPlayerRef.current) return this.soundStop();
      this.soundPlayerRef.current.getCurrentTime((progress) => {
        if (progress < this.state.duration && this.state.progress != progress) {
          this.setState({ progress, wasAudioPlaying: true });
        } else if (
          progress === this.state.progress &&
          progress === 0 &&
          this.state.wasAudioPlaying
        ) {
          this.setState({ wasAudioPlaying: false });
          this.soundStop();
        } else if (progress >= this.state.duration) {
          this.soundStop();
        } else if (progress > 0 && this.state.progress === progress) {
          this.soundStop();
        }
      });
    }, 100);
  }

  async PlaySound(url, id) {
    this.setState({ isAudioClicked: true });
    if (
      this.soundPlayerRef &&
      this.state.progress > 0 &&
      this.state.currentAudioMessage == id
    ) {
      await this.soundPlayerRef.current.play();
      this.setState({ isAudioPlaying: true });
      this.ResumeSound();
    } else {
      try {
        this.setState({
          isAudioLoaded: false,
          currentAudioMessage: id,
          progress: 0,
          wasAudioPlaying: false,
        });
        const sound = new Sound(url, null, (error) => {
          if (error) {
            return console.log("error", error);
          }
          const duration = sound.getDuration();
          if (sound.isLoaded()) {
            this.setState({ isAudioLoaded: true, isAudioClicked: false });
            sound.play();
            this.soundPlayerRef.current = sound;
            this.setState({ audio: sound, isAudioPlaying: true, duration });
            this.ResumeSound();
          }
        });
      } catch (e) {
        alert(e);
      }
    }
  }

  onSend(messages = []) {
    let { item } = this.props;
    let { _id } = item;
    let newMessage = [
      {
        _id: _.uniqueId("OSCE_"),
        text: messages[0].text,
        createdAt: new Date(),
        user: {
          _id: "you",
          name: "You",
        },
      },
    ];
    this.props.dispatch({
      type: APPEND_MESSAGE_EXAMINATION,
      payload: newMessage,
    });
    let inputModel = {
      query: messages[0].text,
      dialogflowCredentials: this.props.examinationItem
        .dialogflowCredentials[0],
      historyId: _id,
    };
    if (this.props.sessionId) {
      inputModel = {
        ...inputModel,
        sessionId: this.props.sessionId,
      };
    }
    this.props.AppAction.sendMessageExam(inputModel, _id);
  }

  renderComposer(props) {
    return (
      <Composer
        {...props}
        textInputStyle={{
          backgroundColor: constants.Colors.White,
          borderRadius: 50,
          borderWidth: 0.1,
          borderColor: constants.Colors.placehoder,
          paddingHorizontal: moderateScale(20),
          marginHorizontal: moderateScale(20),
          paddingTop: moderateScale(7),
        }}
      />
    );
  }
  goToFeedbackPage = async () => {
    const { item } = this.props;
    const difference = new Date().getTime() - this.state.startTime;
    this.context.mixPanel.track(Time_Spent_Examination, {
      TimeSpent: `${difference / 60000} minutes`,
    });
    this.context.mixPanel.track(Activity.userCaseMarksActivity);
    const {
      statusCode = "",
      message = "",
      data = "",
    } = await this.props.AppAction.getScores(
      item._id,
      item.complaintId,
      this.props.sessionId
    );
    if (statusCode !== 200) {
      alert(message);
    } else {
      Navigation.push(this.props.componentId, {
        component: {
          name: "Feedback",
          passProps: { ...this.props, scores: data },
          options: {
            topBar: {
              visible: false,
            },
            bottomTabs: {
              visible: false,
              drawBehind: Platform.OS === "android" ? true : false,
            },
          },
        },
      });
    }
  };

  goToChallengeScoreScreen = async () => {
    const { item } = this.props;
    const difference = new Date().getTime() - this.state.startTime;
    this.context.mixPanel.track(Time_Spent_Examination, {
      TimeSpent: `${difference / 60000} minutes`,
    });
    const {
      statusCode = "",
      message = "",
      data = "",
    } = await this.props.AppAction.getScores(
      item._id,
      item.complaintId,
      this.props.sessionId,
      true
    );
    this.context.mixPanel.track(Challenge_Completed);
    const userId = this.props.Auth._id;
    const url = this.props.serverUrl;
    const rankData = await this.props.AppAction.getRank(userId, url);
    Navigation.push(this.props.componentId, {
      component: {
        name: "ChallengeScoreScreen",
        passProps: {
          ...this.props,
          scores: data,
          rank: rankData?.rank ? rankData.rank : 1,
        },
        options: {
          topBar: {
            visible: false,
          },
          bottomTabs: {
            visible: false,
            drawBehind: Platform.OS === "android" ? true : false,
          },
        },
      },
    });
  };

  gotoDignosisScreen = async () => {
    await this.soundStop();
    Navigation.push(this.props.componentId, {
      component: {
        name: "Dignosis",
        passProps: {
          ...this.props,
        },
        options: {
          topBar: {
            visible: false,
          },
          bottomTabs: {
            visible: false,
            drawBehind: Platform.OS === "android" ? true : false,
          },
        },
      },
    });
  };

  render() {
    let { messagesListing, ChatResponsLoader, scoreLoader } = this.props;
    const previousScreen = this.props.isDifferentStack ? "HomeScreen" : "Home";

    return (
      <SafeAreaView style={styles.container}>
        <Header
          showForward
          previousScreen={previousScreen}
          text={"Examination"}
          border={true}
          componentId={this.props.componentId}
          nextComponent={this.gotoDignosisScreen.bind(this)}
        />

        {scoreLoader ? (
          <ActivityIndicator
            size="large"
            style={{ flex: 1 }}
            color={constants.Colors.gunPowderShade}
          />
        ) : (
          <View style={styles.chatContainer}>
            <GiftedChat
              messages={messagesListing}
              extraData={this.state}
              onSend={(messages) => this.onSend(messages)}
              keyboardShouldPersistTaps={"handled"}
              minInputToolbarHeight={60}
              user={{
                _id: "you",
              }}
              listViewProps={{ marginBottom: moderateScale(20) }}
              alwaysShowSend={true}
              renderAvatar={null}
              renderFooter={() => {
                return (
                  <Spinner
                    style={{ marginLeft: moderateScale(10) }}
                    isVisible={ChatResponsLoader}
                    size={50}
                    type={"ThreeBounce"}
                    color={constants.Colors.LightGray}
                  />
                );
              }}
              renderBubble={(res) => {
                return res.currentMessage.user._id === "you" ? (
                  <Bubble
                    text={res.currentMessage.text}
                    imageUri={constants.Images.Common.meBubble}
                    color={"#c9fec1"}
                    type={1}
                  />
                ) : res.currentMessage.user._id === "bot" &&
                  res.currentMessage?.image ? (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        showImage: true,
                        clickedImage: res.currentMessage.image,
                      });
                    }}
                    style={styles.imageContainer}
                  >
                    <Image
                      resizeMethod="resize"
                      style={{ width: 200, height: 250 }}
                      source={{ uri: res.currentMessage.image }}
                    />
                  </TouchableOpacity>
                ) : res.currentMessage.user._id === "bot" &&
                  res.currentMessage?.audio ? (
                  <View style={styles.audioContainer}>
                    {!this.state.isAudioLoaded &&
                    this.state.isAudioClicked &&
                    this.state?.currentAudioMessage ==
                      res.currentMessage._id ? (
                      <ActivityIndicator size="small" color="green" />
                    ) : (
                      <TouchableOpacity
                        onPress={
                          this.state.isAudioPlaying
                            ? () => {
                                this.pauseSound(
                                  res.currentMessage.audio,
                                  res.currentMessage._id
                                );
                              }
                            : () => {
                                this.PlaySound(
                                  res.currentMessage.audio,
                                  res.currentMessage._id
                                );
                              }
                        }
                      >
                        {(this.state.isAudioPlaying &&
                          this.state.currentAudioMessage) ==
                        res.currentMessage._id ? (
                          <Image
                            style={styles.iconImage}
                            source={require("../../assets/img/pause.png")}
                          />
                        ) : (
                          <Image
                            style={styles.iconImage}
                            source={require("../../assets/img/play.png")}
                          />
                        )}
                      </TouchableOpacity>
                    )}
                    <View style={styles.sliderContainer}>
                      <Slider
                        minimumTrackTintColor={"green"}
                        disabled={true}
                        thumbImage={thumb}
                        minimumValue={0}
                        onValueChange={(val) =>
                          this.setState({ progress: val })
                        }
                        maximumValue={this.state.duration || 1}
                        value={
                          this.state.currentAudioMessage ==
                          res.currentMessage._id
                            ? this.state.progress
                            : 0
                        }
                      />
                    </View>
                    <View style={styles.progressContainer}>
                      <Text style={{ fontSize: 14, color: "green" }}>
                        {this.state.currentAudioMessage ==
                        res.currentMessage._id
                          ? this.state.progress.toFixed(2)
                          : ""}
                      </Text>
                    </View>
                  </View>
                ) : (
                  <Bubble
                    text={res.currentMessage.text}
                    imageUri={constants.Images.Common.youBubble}
                    color={"#ffffff"}
                    type={2}
                  />
                );
              }}
              renderInputToolbar={(props) => {
                return (
                  <InputToolbar
                    {...props}
                    containerStyle={{
                      backgroundColor: constants.Colors.ChatFooterColor,
                      borderWidth: 0,
                      paddingVertical: 9,
                    }}
                  />
                );
              }}
              renderSystemMessage={(res) => {
                return <SystemMsg text={res.currentMessage.text} />;
              }}
              renderSend={(props) => (
                <Send {...props}>
                  <View style={styles.sendContainer}>
                    <Image
                      style={{ width: moderateScale(18) }}
                      source={constants.Images.Common.sendButton}
                      resizeMode={"center"}
                    />
                  </View>
                </Send>
              )}
              renderComposer={(props) => this.renderComposer(props)}
            />
          </View>
        )}
        {this.state.showImage && (
          <Modal
            visible={this.state.showImage}
            animationType="fade"
            onRequestClose={() => this.setState({ showImage: false })}
          >
            <SafeAreaView style={{ flex: 1 }}>
              <TouchableOpacity
                onPress={() => this.setState({ showImage: false })}
                style={styles.backContainer}
              >
                <Image source={constants.Images.Common.backButton} />
              </TouchableOpacity>
              <View style={{ flex: 1 }}>
                <ImageViewer
                  saveToLocalByLongPress={false}
                  renderIndicator={() => <></>}
                  enableSwipeDown={true}
                  onSwipeDown={() => this.setState({ showImage: false })}
                  enablePreload={true}
                  enableImageZoom={true}
                  style={{ flex: 1 }}
                  backgroundColor="white"
                  imageUrls={[
                    {
                      url: this.state.clickedImage,
                      height: Dimensions.get("window").height,
                      width: Dimensions.get("window").width + 50,

                      props: {
                        style: styles.imageViewerMagnify,
                      },
                    },
                  ]}
                />
              </View>
            </SafeAreaView>
          </Modal>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: constants.Colors.appBackgroundColor,
    flex: 1,
    paddingTop: moderateScale(18),
  },
  chatContainer: {
    marginTop: moderateScale(5),
    backgroundColor: constants.Colors.appBackgroundColor,
    flex: 1,
  },
  sliderContainer: {
    width: "90%",
    justifyContent: "center",
    height: 30,
  },
  sendContainer: {
    borderRadius: 70,
    marginRight: moderateScale(10),
    padding: moderateScale(10),
    backgroundColor: constants.Colors.ChatBlueColor,
    marginBottom: -5,
  },
  backContainer: {
    height: 45,
    width: 50,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "flex-start",
  },
  imageViewerMagnify: {
    width: "120%",
    height: "100%",
    resizeMode: "contain",
  },
  progressContainer: {
    marginTop: 45,
    marginLeft: -35,
  },
  imageContainer: {
    borderRadius: moderateScale(10),
    borderColor: constants.Colors.shadownColor,
    borderWidth: moderateScale(1),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: moderateScale(2) },
    shadowOpacity: moderateScale(0.3),
    shadowRadius: moderateScale(2),
    elevation: moderateScale(3),
    marginBottom: moderateScale(16),
    paddingBottom: moderateScale(16),
    backgroundColor: "white",
    height: 270,
    width: 220,
    padding: 10,
  },
  audioContainer: {
    borderRadius: moderateScale(10),
    borderColor: constants.Colors.shadownColor,
    borderWidth: moderateScale(0),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: moderateScale(2) },
    shadowOpacity: moderateScale(0.3),
    shadowRadius: moderateScale(2),
    elevation: moderateScale(3),
    marginBottom: moderateScale(16),
    // paddingBottom: moderateScale(16),
    backgroundColor: "white",
    height: 80,
    width: 270,
    padding: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  iconImage: {
    width: 30,
    height: 30,
  },
});

function mapStateToProps(state) {
  return {
    lastMessage: state.ChatBot.examLastMessage,
    messagesListing: state.ChatBot.examMessageListing,
    sessionId: state.ChatBot.sessionId,
    ChatResponsLoader: state.Loaders.ChatResponsLoaderExamination,
    scoreLoader: state.Score.loader,
    Auth: state.Auth.authData,
    serverUrl: state.AppConfig.serverUrl,
  };
}

const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExaminationScreen);
