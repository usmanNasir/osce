import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import { moderateScale } from "../helpers/ResponsiveFonts";
import { Navigation } from "react-native-navigation";
import Fonts from "../constants/Fonts";
import Icon from "react-native-vector-icons/FontAwesome";
import Constants from "../constants";

const Header = (props) => {
  let {
    text,
    previousScreen,
    navigate,
    home,
    border,
    checkMark,
    nextComponent,
    previousComponent,
    feedback,
    profile,
    hideBack,
    showForward,
    isScoreScreen,
    mixPanelEvent = () => {},
  } = props;
  let border_style = border
    ? {
        paddingHorizontal: moderateScale(15),
        backgroundColor: Constants.Colors.appBackgroundColor,
        borderBottomWidth: home ? 1 : null,
        borderBottomColor: home ? "#eeeeee" : null,
        alignItems: "center",
        justifyContent: "center",
      }
    : {};
  return (
    <View>
      <View style={[styles.container, border_style]}>
        {home && isScoreScreen ? null : (
          <TouchableOpacity
            style={{
              flex: 0.1,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() =>
              navigate
                ? previousComponent()
                : (Navigation.pop(previousScreen), mixPanelEvent())
            }
          >
            {!home && border && !isScoreScreen ? (
              <Image
                source={Constants.Images.Common.backButton}
                resizeMode={"contain"}
              />
            ) : border && isScoreScreen && !home ? null : !home &&
              !profile &&
              !hideBack ? (
              <Icon
                name="chevron-left"
                color={Constants.Colors.Black}
                size={22}
              />
            ) : null}
          </TouchableOpacity>
        )}
        <View
          style={{
            flex: 0.8,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {home ? (
            <View
              style={{
                flex: 0.8,
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Text
                style={[
                  styles.textStyleHome,
                  { fontFamily: Fonts.Bold_saira.fontFamily },
                ]}
              >
                {text}
              </Text>
            </View>
          ) : (
            <Text style={feedback ? styles.textStyleFeed : styles.textStyle}>
              {text}
            </Text>
          )}
        </View>
        {checkMark && !feedback && (
          <TouchableOpacity
            style={{
              flex: 0.1,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => nextComponent()}
          >
            <Image
              source={Constants.Images.Common.checkMark}
              resizeMode={"contain"}
            />
          </TouchableOpacity>
        )}
        {showForward && (
          <TouchableOpacity
            style={{
              flex: 0.1,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => nextComponent()}
          >
            <Image
              source={Constants.Images.Common.Forward}
              resizeMode={"contain"}
            />
          </TouchableOpacity>
        )}
        {!home && !checkMark && !feedback && !showForward && (
          <View style={{ flex: 0.1 }} />
        )}
        {feedback && !checkMark && (
          <TouchableOpacity
            style={{
              flex: 0.1,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => nextComponent()}
          >
            <Image
              source={Constants.Images.Common.Forward}
              resizeMode={"contain"}
            />
          </TouchableOpacity>
        )}
      </View>
      {border && !isScoreScreen ? (
        <View
          style={
            home && isScoreScreen ? styles.shadowViewHome : styles.shadowView
          }
        />
      ) : null}

      {hideBack ? (
        <View style={home ? styles.shadowViewHome : styles.shadowView} />
      ) : null}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    height: moderateScale(50),
    justifyContent: "space-between",
  },
  cont2: {
    flexDirection: "row",
    height: moderateScale(50),
    justifyContent: "space-between",
  },
  textStyle: {
    color: Constants.Colors.Black,
    fontSize: moderateScale(22),
    fontWeight: "bold",
    // right: -20
  },
  textStyleHome: {
    color: Constants.Colors.Black,
    fontSize: moderateScale(22),
    // right: -20
    //fontWeight: "bold"
  },
  textStyleFeed: {
    color: Constants.Colors.Black,
    fontSize: moderateScale(22),
    fontWeight: "bold",
    // textAlign:'center',
    // alignSelf:'center',
    //  right: -30
  },
  shadowView: {
    borderBottomColor: Constants.Colors.shadownColor,
    borderBottomWidth: 1,
    shadowColor: Constants.Colors.DarkBlack,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 1,
    elevation: 2,
  },
  shadowViewHome: {
    borderBottomColor: Constants.Colors.shadownColor,
    borderBottomWidth: 0.5,
    shadowColor: Constants.Colors.DarkBlack,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 1,
    elevation: 2,
  },
});
export default Header;
