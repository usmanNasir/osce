import React from "react";
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  AsyncStorage,
  Alert,
  Platform,
} from "react-native";
import { requestSubscription, getConnection } from "../../utilities/InApp";
import { MixPanelContext } from "../../utilities/MixPanel";
import { Activity, Time_Spent_MarksScheme } from "../../constants/Events";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { logger } from "./../../utilities/analytics-logger";
import { SequareButton } from "../../components/common";
import { Navigation } from "react-native-navigation";
import { WebView } from "react-native-webview";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import Modal from "react-native-modal";
import Constants from "../../constants";
import MainListItem from "../../components/collapsableList/MainListItem";
import ControlledLoader from "../../components/common/ControlledLoader";
import Header from "../Header";
import * as AppAction from "../../actions";

const deviceWidth = Dimensions.get("screen").width;
const deviceHeight = Dimensions.get("screen").height;

class Feedback extends React.Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
    this.state = {
      showAlert: true,
      key: true,
      openScreen: false,
      openModal: false,
      url: "",
      visible: false,
      products: null,
      index: 0,
      startTime: null,
      purchase_obj: {},
    };
  }

  showModal = (url) => {
    this.setState({
      url: url,
      openModal: !this.state.openModal,
    });
  };

  async getScoreData() {
    await this.props.AppAction.getScores(
      this.props.item._id,
      this.props.item.complaintId,
      this.props.sessionId,
      false,
      this.props.awardDignosisScore,
      this.props.awardScore
    );
  }

  async componentDidMount() {
    await this.getScoreData();
    this.setState({
      startTime: new Date().getTime(),
    });
    let rec = await AsyncStorage.getItem("receipt");
    if (rec) {
      this.setState({
        key: true,
      });
    }
    logger.logEventWithData("feedbackPage", { category: this.props.name });
    this.getSubscriptions();
  }

  getSubscriptions = async () => {
    let products = await getConnection();
    this.setState({
      products: {
        subs_1: products[0].productId,
        subs_2: products[1].productId,
      },
    });
  };

  requestPurchase = async (sku) => {
    let response = await requestSubscription(sku);
    await AsyncStorage.setItem("receipt", JSON.stringify(response));
    this.setState(
      {
        purchase_obj: response,
      },
      () => {
        if (this.state.purchase_obj.purchaseToken) {
          this.setState({ key: true, openScreen: false });
        }
      }
    );
  };

  goToFeedbackPage() {
    const difference = new Date().getTime() - this.state.startTime;
    this.context.mixPanel.track(Time_Spent_MarksScheme, {
      TimeSpent: `${difference / 60000} minutes`,
    });
    this.context.mixPanel.track(Activity.userCaseConditionsActivity);
    if (this.state.key === true) {
      this.props.AppAction.getConditions(this.props.item._id);
      Navigation.push(this.props.componentId, {
        component: {
          name: "ConditionPage",
          passProps: {
            ...this.props,
            name: this.props.name,
            Grades: this.props.Grades,
          },
          options: {
            topBar: {
              visible: false,
            },
            bottomTabs: {
              visible: false,
              drawBehind: Platform.OS === "android" ? true : false,
            },
            bottomTab: {
              visible: false,
            },
          },
        },
      });
    }
  }

  renderMainListItem = ({ item }) => {
    return (
      <MainListItem
        item={item}
        parentProps={{
          componentId: this.props.componentId,
          name: this.props.name,
          key: this.state.key,
          props: this.props,
        }}
      />
    );
  };

  showScreen = () => {
    this.setState({
      openScreen: !this.state.openScreen,
    });
  };

  render() {
    const scores = this.props.scoreData;
    let newScores = [];
    newScores.push();
    const previousScreen = this.props.isDifferentStack ? "HomeScreen" : "Home";

    return (
      <SafeAreaView
        style={{
          flex: 1,
          paddingTop: moderateScale(20),
          backgroundColor: Constants.Colors.appBackgroundColor,
        }}
      >
        <Header
          previousScreen={previousScreen}
          text={this.props.name}
          border={true}
          checkMark={false}
          componentId={this.props.componentId}
          nextComponent={this.goToFeedbackPage.bind(this)}
          feedback={true}
        />

        {this.props.scoreLoader ? (
          <ControlledLoader showLoader={this.props.scoreLoader} />
        ) : (
          <ScrollView
            style={{
              marginTop: moderateScale(5),
              backgroundColor: Constants.Colors.appBackgroundColor,
              paddingHorizontal: moderateScale(15),
            }}
          >
            <Modal
              style={{
                margin: 0,
                flex: 1,
                marginHorizontal: moderateScale(20),
              }}
              isVisible={
                this.state.showAlert &&
                this.props.item &&
                !this.props.item.grade
              }
            >
              <View
                style={{
                  backgroundColor: Constants.Colors.White,
                  marginVertical: moderateScale(20),
                  flexDirection: "column",
                  justifyContent: "space-between",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    marginLeft:
                      Platform.OS == "ios"
                        ? moderateScale(5)
                        : moderateScale(30),
                    justifyContent: "center",
                  }}
                >
                  <Text
                    style={[
                      styles.popupText,
                      { color: Constants.Colors.Black },
                    ]}
                  >
                    Tap on the tiles and the
                    <Text style={{ color: Constants.Colors.link }}>
                      {" "}
                      marks highlighted in blue{" "}
                    </Text>
                    for more information.
                  </Text>
                </View>
                <View
                  style={{
                    borderTopWidth: 1,
                    borderTopColor: Constants.Colors.gunPowderShade,
                    padding: moderateScale(15),
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.setState({ showAlert: false })}
                  >
                    <Text
                      style={{
                        fontSize: moderateScale(20),
                        alignSelf: "center",
                        color: Constants.Colors.link,
                        fontWeight: "bold",
                      }}
                    >
                      OK
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
            <View style={styles.firstContainer}>
              <View>
                {this.state.key === false ? (
                  <Text style={styles.gradeText}>Grade</Text>
                ) : (
                  <Text style={styles.gradeText}>
                    Grade {scores.finalScore}
                    {"\n"}
                    {scores.finalScoreSubHead}
                  </Text>
                )}
              </View>
              {this.state.key === false ? (
                <Text style={styles.gradeText}>{scores.finalScore}</Text>
              ) : null}
              <View>
                {this.state.key === false ? (
                  <Image
                    source={require("../../assets/img/starGrey.png")}
                    resizeMode={"contain"}
                    style={styles.resultIcon}
                  />
                ) : scores.finalScore !== "F" ? (
                  <Image
                    source={Constants.Images.Common.pass}
                    resizeMode={"contain"}
                    style={styles.resultIcon}
                  />
                ) : (
                  <Image
                    source={Constants.Images.Common.fail}
                    resizeMode={"contain"}
                    style={styles.resultIcon}
                  />
                )}
              </View>
            </View>
            {this.state.key === false ? (
              <ImageBackground
                source={require("../../assets/img/bg.png")}
                style={{
                  resizeMode: "cover",
                  maxWidth: "100%",
                  padding: 30,
                }}
              >
                <Text
                  style={{
                    fontSize: moderateScale(16),
                    textAlign: "center",
                  }}
                >
                  Unlock the OSCE.AI
                </Text>
                <Text
                  style={{
                    fontSize: moderateScale(16),
                    textAlign: "center",
                  }}
                >
                  Complete Feedback
                </Text>
                <SequareButton
                  onPress={() => {}}
                  onPress={() =>
                    this.setState({
                      openScreen: true,
                    })
                  }
                  loading={false}
                  style={{
                    marginTop: moderateScale(10),
                    backgroundColor: Constants.Colors.blue,
                    padding: 10,
                    width: "70%",
                    alignSelf: "center",
                  }}
                  text="SUBSCRIBE NOW"
                />
              </ImageBackground>
            ) : null}
            <Modal
              animationType="slide"
              transparent={false}
              style={{ margin: 0 }}
              visible={this.state.openScreen}
              scrollHorizontal={true}
              pagingEnabled={true}
              showsHorizontalScrollIndicator={false}
            >
              <View
                style={{
                  flex: 1,
                  height: deviceHeight,
                  width: deviceWidth,
                  backgroundColor: "white",
                }}
              >
                <TouchableOpacity
                  style={{
                    marginRight: moderateScale(20),
                    borderRadius: moderateScale(50),
                    width: 38,
                    alignSelf: "flex-end",
                    top: moderateScale(30),
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    zIndex: 999,
                    elevation: 5,
                  }}
                  onPress={() => this.showScreen()}
                >
                  <Image
                    source={require("../../assets/img/closeS.png")}
                    style={{
                      height: moderateScale(35),
                      width: moderateScale(35),
                    }}
                  />
                </TouchableOpacity>
                <ImageBackground
                  source={require("../../assets/img/bgS.png")}
                  style={{
                    flex: 1,
                    resizeMode: "cover",
                    width: "100%",
                    height: "100%",
                    marginTop:
                      deviceWidth > 375
                        ? deviceWidth / 2 - moderateScale(2)
                        : deviceWidth / 2 - moderateScale(180),
                    alignItems: "center",
                  }}
                >
                  <ScrollView
                    horizontal={true}
                    pagingEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{ justifyContent: "center" }}
                  >
                    <View style={{ width: deviceWidth, alignItems: "center" }}>
                      <Text
                        style={{
                          fontSize: moderateScale(20),
                          fontWeight: "bold",
                          marginTop: moderateScale(40),
                        }}
                      >
                        Subscribe to OSCE.AI
                      </Text>
                      <Text
                        style={{
                          fontSize: moderateScale(15),
                          textAlign: "center",
                          marginTop: moderateScale(10),
                        }}
                      >
                        Get access to the revision{"\n"}course with hundreds of
                        cases
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                          width: "30%",
                          justifyContent: "space-evenly",
                          alignItems: "center",
                          textAlign: "center",
                          marginTop: moderateScale(20),
                        }}
                      >
                        <Text>{"\u2B24"}</Text>
                        <Text
                          style={{ color: Constants.Colors.gray, opacity: 0.6 }}
                        >
                          {"\u2B24"}
                        </Text>
                        <Text
                          style={{ color: Constants.Colors.gray, opacity: 0.6 }}
                        >
                          {"\u2B24"}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-evenly",
                          width: "100%",
                          marginTop: moderateScale(30),
                        }}
                      >
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              index: 0,
                            })
                          }
                          style={styles.square2}
                        >
                          <View
                            style={{
                              backgroundColor: "#fff",
                              borderWidth:
                                this.state.index === 0
                                  ? moderateScale(3)
                                  : null,
                              borderColor:
                                this.state.index === 0 ? "#1c7cce" : null,
                              borderBottomWidth:
                                this.state.index === 0 ? 0 : null,
                              width: "100%",
                              flexDirection: "row",
                              justifyContent: "center",
                              borderTopLeftRadius: 12,
                              borderTopRightRadius: 12,
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(15),
                                fontWeight: "bold",
                                color: "white",
                              }}
                            >
                              MOST POPULAR
                            </Text>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: "column",
                              justifyContent: "space-around",
                              borderWidth:
                                this.state.index === 0
                                  ? moderateScale(3)
                                  : null,
                              borderColor:
                                this.state.index === 0 ? "#1c7cce" : null,
                              borderTopWidth: this.state.index === 0 ? 0 : null,
                              alignItems: "center",
                              backgroundColor: "white",
                              borderBottomLeftRadius: 12,
                              borderBottomRightRadius: 12,
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(20),
                                fontWeight: "bold",
                                marginTop: moderateScale(10),
                              }}
                            >
                              Monthly
                            </Text>
                            <Text
                              style={{
                                fontSize: moderateScale(25),
                                fontWeight: "bold",
                                bottom: moderateScale(8),
                              }}
                            >
                              {"\u00A3"}6.99
                            </Text>
                            <Text style={{ bottom: moderateScale(5) }}>
                              PER MONTH
                            </Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={styles.square2}
                          onPress={() => this.setState({ index: 1 })}
                        >
                          <View
                            style={{
                              backgroundColor: "#1c7cce",
                              width: "100%",
                              flexDirection: "row",
                              borderWidth:
                                this.state.index === 1 ? moderateScale(2) : 0,
                              borderColor:
                                this.state.index === 1 ? "#1c7cce" : null,
                              borderTopWidth: this.state.index === 1 ? 0 : null,
                              justifyContent: "center",
                              borderTopLeftRadius: 12,
                              borderTopRightRadius: 12,
                              padding: moderateScale(5),
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(15),
                                fontWeight: "bold",
                                color: "white",
                              }}
                            >
                              MOST POPULAR
                            </Text>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: "column",
                              justifyContent: "space-around",
                              borderWidth:
                                this.state.index === 1 ? moderateScale(2) : 0,
                              borderColor:
                                this.state.index === 1 ? "#1c7cce" : null,
                              borderTopWidth: this.state.index === 1 ? 0 : null,
                              alignItems: "center",
                              backgroundColor: "white",
                              borderBottomLeftRadius: 12,
                              borderBottomRightRadius: 12,
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(20),
                                fontWeight: "bold",
                              }}
                            >
                              6 Months
                            </Text>
                            <Text
                              style={{
                                fontSize: moderateScale(25),
                                fontWeight: "bold",
                              }}
                            >
                              {"\u00A3"}2.66
                            </Text>
                            <Text style={{ textAlign: "center" }}>
                              {"\u00A3"}15.99 one{"\n"}off fee
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View
                        style={{
                          textAlign: "center",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Text
                          style={{
                            fontWeight: "bold",
                            marginTop: moderateScale(30),
                          }}
                        >
                          Recurring Bill. Cancel Anytime.
                        </Text>
                        <TouchableOpacity
                          onPress={() =>
                            this.showModal("http://osce.ai/terms-conditions/")
                          }
                        >
                          <Text
                            style={{
                              textDecorationLine: "underline",
                              alignSelf: "center",
                              fontWeight: "bold",
                              marginTop: moderateScale(5),
                            }}
                          >
                            Terms & Conditions
                          </Text>
                        </TouchableOpacity>
                        <Text
                          style={{
                            textAlign: "center",
                            marginTop: moderateScale(8),
                            fontSize: 10,
                          }}
                        >
                          Subscription terms: The monthly subscription is £6.99
                          and
                          {"\n"}
                          automatically renews every month. The 6 monthly
                          subscription is{"\n"}£15.99 and is charged every 6
                          months.
                        </Text>
                      </View>
                    </View>

                    <View style={{ width: deviceWidth, alignItems: "center" }}>
                      <Text
                        style={{
                          fontSize: moderateScale(20),
                          fontWeight: "bold",
                          marginTop: moderateScale(40),
                        }}
                      >
                        State of the art learning
                      </Text>
                      <Text
                        style={{
                          fontSize: moderateScale(15),
                          textAlign: "center",
                          marginTop: moderateScale(10),
                        }}
                      >
                        Receive AI feedback on your{"\n"}consultation skills to
                        improve
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-evenly",
                          width: "30%",
                          alignItems: "center",
                          marginTop: moderateScale(20),
                        }}
                      >
                        <Text
                          style={{ color: Constants.Colors.gray, opacity: 0.6 }}
                        >
                          {"\u2B24"}
                        </Text>
                        <Text>{"\u2B24"}</Text>
                        <Text
                          style={{ color: Constants.Colors.gray, opacity: 0.6 }}
                        >
                          {"\u2B24"}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-evenly",
                          width: "100%",
                          marginTop: moderateScale(30),
                        }}
                      >
                        <View style={styles.square2}>
                          <View
                            style={{
                              backgroundColor: "#fff",
                              width: "100%",
                              flexDirection: "row",
                              justifyContent: "center",
                              borderTopLeftRadius: 12,
                              borderTopRightRadius: 12,
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(15),
                                fontWeight: "bold",
                                color: "white",
                              }}
                            >
                              MOST POPULAR
                            </Text>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: "column",
                              justifyContent: "space-around",
                              alignItems: "center",
                              backgroundColor: "white",
                              borderBottomLeftRadius: 12,
                              borderBottomRightRadius: 12,
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(20),
                                fontWeight: "bold",
                                marginTop: moderateScale(10),
                              }}
                            >
                              Monthly
                            </Text>
                            <Text
                              style={{
                                fontSize: moderateScale(25),
                                fontWeight: "bold",
                                bottom: moderateScale(8),
                              }}
                            >
                              {"\u00A3"}6.99
                            </Text>
                            <Text style={{ bottom: moderateScale(5) }}>
                              PER MONTH
                            </Text>
                          </View>
                        </View>
                        <View style={styles.square2}>
                          <View
                            style={{
                              backgroundColor: "#1c7cce",
                              width: "100%",
                              flexDirection: "row",
                              justifyContent: "center",
                              borderTopLeftRadius: 12,
                              borderTopRightRadius: 12,
                              paddingVertical: moderateScale(5),
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(15),
                                fontWeight: "bold",
                                color: "white",
                              }}
                            >
                              MOST POPULAR
                            </Text>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: "column",
                              justifyContent: "space-around",
                              alignItems: "center",
                              backgroundColor: "white",
                              borderBottomLeftRadius: 12,
                              borderBottomRightRadius: 12,
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(20),
                                fontWeight: "bold",
                              }}
                            >
                              6 Months
                            </Text>
                            <Text
                              style={{
                                fontSize: moderateScale(25),
                                fontWeight: "bold",
                              }}
                            >
                              {"\u00A3"}2.66
                            </Text>
                            <Text style={{ textAlign: "center" }}>
                              {"\u00A3"}15.99 one{"\n"}off fee
                            </Text>
                          </View>
                        </View>
                      </View>
                      <View
                        style={{
                          textAlign: "center",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Text
                          style={{
                            fontWeight: "bold",
                            marginTop: moderateScale(30),
                          }}
                        >
                          Recurring Bill. Cancel Anytime.
                        </Text>
                        <TouchableOpacity
                          onPress={() =>
                            this.showModal("http://osce.ai/terms-conditions/")
                          }
                        >
                          <Text
                            style={{
                              textDecorationLine: "underline",
                              alignSelf: "center",
                              fontWeight: "bold",
                              marginTop: moderateScale(5),
                            }}
                          >
                            Terms & Conditions
                          </Text>
                        </TouchableOpacity>
                        <Text
                          style={{
                            textAlign: "justify",
                            marginTop: moderateScale(8),
                            fontSize: 10,
                          }}
                        >
                          Subscription terms: The monthly subscription is £6.99
                          {"\n"}and automatically renews every month. The 6
                          monthly
                          {"\n"}
                          subscription is £15.99 and is charged every 6 months.
                        </Text>
                      </View>
                    </View>

                    <View style={{ width: deviceWidth, alignItems: "center" }}>
                      <Text
                        style={{
                          fontSize: moderateScale(20),
                          fontWeight: "bold",
                          marginTop: moderateScale(40),
                        }}
                      >
                        Everything you need to know
                      </Text>
                      <Text
                        style={{
                          fontSize: moderateScale(15),
                          textAlign: "center",
                          marginTop: moderateScale(10),
                        }}
                      >
                        Unlock condition fact sheets to{"\n"}increase your
                        knowledge base
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-evenly",
                          width: "30%",
                          alignItems: "center",
                          marginTop: moderateScale(20),
                        }}
                      >
                        <Text
                          style={{ color: Constants.Colors.gray, opacity: 0.6 }}
                        >
                          {"\u2B24"}
                        </Text>
                        <Text
                          style={{ color: Constants.Colors.gray, opacity: 0.6 }}
                        >
                          {"\u2B24"}
                        </Text>
                        <Text>{"\u2B24"}</Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-evenly",
                          width: "100%",
                          marginTop: moderateScale(30),
                        }}
                      >
                        <View style={styles.square2}>
                          <View
                            style={{
                              backgroundColor: "#fff",
                              width: "100%",
                              flexDirection: "row",
                              justifyContent: "center",
                              borderTopLeftRadius: 12,
                              borderTopRightRadius: 12,
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(15),
                                fontWeight: "bold",
                                color: "white",
                              }}
                            >
                              MOST POPULAR
                            </Text>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: "column",
                              justifyContent: "space-around",
                              alignItems: "center",
                              backgroundColor: "white",
                              borderBottomLeftRadius: 12,
                              borderBottomRightRadius: 12,
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(20),
                                fontWeight: "bold",
                                marginTop: moderateScale(10),
                              }}
                            >
                              Monthly
                            </Text>
                            <Text
                              style={{
                                fontSize: moderateScale(25),
                                fontWeight: "bold",
                                bottom: moderateScale(8),
                              }}
                            >
                              {"\u00A3"}6.99
                            </Text>
                            <Text style={{ bottom: moderateScale(5) }}>
                              PER MONTH
                            </Text>
                          </View>
                        </View>
                        <View style={styles.square2}>
                          <View
                            style={{
                              backgroundColor: "#1c7cce",
                              width: "100%",
                              flexDirection: "row",
                              justifyContent: "center",
                              borderTopLeftRadius: 12,
                              borderTopRightRadius: 12,
                              paddingVertical: moderateScale(5),
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(15),
                                fontWeight: "bold",
                                color: "white",
                              }}
                            >
                              MOST POPULAR
                            </Text>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: "column",
                              justifyContent: "space-around",
                              alignItems: "center",
                              backgroundColor: "white",
                              borderBottomLeftRadius: 12,
                              borderBottomRightRadius: 12,
                            }}
                          >
                            <Text
                              style={{
                                fontSize: moderateScale(20),
                                fontWeight: "bold",
                              }}
                            >
                              6 Months
                            </Text>
                            <View
                              style={{
                                flexDirection: "row",
                                justifyContent: "center",
                                alignItems: "center",
                              }}
                            >
                              <Text
                                style={{
                                  fontSize: moderateScale(25),
                                  fontWeight: "bold",
                                }}
                              >
                                {"\u00A3"}2.66
                              </Text>
                              <Text
                                style={{
                                  fontSize: moderateScale(16),
                                  top: moderateScale(2),
                                }}
                              >
                                /mo
                              </Text>
                            </View>
                            <Text style={{ textAlign: "center" }}>
                              {"\u00A3"}15.99 one{"\n"}off fee
                            </Text>
                          </View>
                        </View>
                      </View>
                      <View
                        style={{
                          textAlign: "center",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Text
                          style={{
                            fontWeight: "bold",
                            marginTop: moderateScale(30),
                          }}
                        >
                          Recurring Bill. Cancel Anytime.
                        </Text>
                        <TouchableOpacity
                          onPress={() =>
                            this.showModal("http://osce.ai/terms-conditions/")
                          }
                        >
                          <Text
                            style={{
                              textDecorationLine: "underline",
                              alignSelf: "center",
                              fontWeight: "bold",
                              marginTop: moderateScale(5),
                            }}
                          >
                            Terms & Conditions
                          </Text>
                        </TouchableOpacity>
                        <Text
                          style={{
                            textAlign: "justify",
                            marginTop: moderateScale(8),
                            fontSize: 10,
                          }}
                        >
                          Subscription terms: The monthly subscription is £6.99
                          {"\n"}and automatically renews every month. The 6
                          monthly
                          {"\n"}
                          subscription is £15.99 and is charged every 6 months.
                        </Text>
                      </View>
                    </View>
                  </ScrollView>
                  <SequareButton
                    onPress={() =>
                      this.requestPurchase(
                        this.state.index === 0
                          ? this.state.products.subs_1
                          : this.state.products.subs_2
                      )
                    }
                    loading={false}
                    style={{
                      // marginBottom: moderateScale(10),
                      bottom: moderateScale(25),
                      width: "90%",
                      backgroundColor: Constants.Colors.blue,
                    }}
                    text="SUBSCRIBE NOW"
                  />
                </ImageBackground>
              </View>
              {/* <View
                style={{
                  flex: 1,
                  height: deviceHeight,
                  width: deviceWidth,
                  backgroundColor: "white"
                }}
              >
                <TouchableOpacity
                  style={{
                    // marginVertical: moderateScale(30),
                    marginRight: moderateScale(20),
                    marginRight: moderateScale(20),
                    borderRadius: moderateScale(50),
                    width: 38,
                    alignSelf: "flex-end",
                    top: moderateScale(50),
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 2
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,

                    elevation: 5
                  }}
                  onPress={() => this.showScreen()}
                >
                  <Image
                    source={require("../../assets/img/closeS.png")}
                    style={{
                      height: moderateScale(35),
                      width: moderateScale(35)
                    }}
                  />
                </TouchableOpacity>
                <ImageBackground
                  source={require("../../assets/img/bgS.png")}
                  style={{
                    flex: 1,
                    resizeMode: "cover",
                    width: "100%",
                    height: "100%",
                    marginTop:
                      deviceWidth > 375
                        ? deviceWidth / 2 - moderateScale(2)
                        : deviceWidth / 2 - moderateScale(140),
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      fontSize: moderateScale(20),
                      fontWeight: "bold",
                      marginTop: moderateScale(40)
                    }}
                  >
                    State of the art learning
                  </Text>
                  <Text
                    style={{
                      fontSize: moderateScale(15),
                      textAlign: "center",
                      marginTop: moderateScale(10)
                    }}
                  >
                    Receive AI feedback on your{"\n"}consultation skills to
                    improve
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-evenly",
                      width: "30%",
                      alignItems: "center",
                      marginTop: moderateScale(20)
                    }}
                  >
                    <Text style={{ color: Constants.Colors.gray, opacity:0.6 }}>
                      {"\u2B24"}
                    </Text>
                    <Text>{"\u2B24"}</Text>
                    <Text style={{ color: Constants.Colors.gray, opacity:0.6 }}>
                      {"\u2B24"}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-evenly",
                      width: "100%",
                      marginTop: moderateScale(30)
                    }}
                  >
                    <TouchableOpacity style={styles.square2}>
                      <View
                        style={{
                          backgroundColor: "#fff",
                          width: "100%",
                          flexDirection: "row",
                          justifyContent: "center",
                          borderTopLeftRadius: 12,
                          borderTopRightRadius: 12
                        }}
                      >
                        <Text
                          style={{
                            fontSize: moderateScale(15),
                            fontWeight: "bold",
                            color: "white"
                          }}
                        >
                          MOST POPULAR
                        </Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "column",
                          justifyContent: "space-around",
                          alignItems: "center",
                          backgroundColor: "white",
                          borderBottomLeftRadius: 12,
                          borderBottomRightRadius: 12
                        }}
                      >
                        <Text
                          style={{
                            fontSize: moderateScale(20),
                            fontWeight: "bold",
                            marginTop: moderateScale(10)
                          }}
                        >
                          Monthly
                        </Text>
                        <Text
                          style={{
                            fontSize: moderateScale(25),
                            fontWeight: "bold",
                            bottom: moderateScale(8)
                          }}
                        >
                          {"\u00A3"}6.99
                        </Text>
                        <Text style={{ bottom: moderateScale(5) }}>
                          PER MONTH
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.square2}>
                      <View
                        style={{
                          backgroundColor: "#1c7cce",
                          width: "100%",
                          flexDirection: "row",
                          justifyContent: "center",
                          borderTopLeftRadius: 12,
                          borderTopRightRadius: 12,
                          paddingVertical: moderateScale(5)
                        }}
                      >
                        <Text
                          style={{
                            fontSize: moderateScale(15),
                            fontWeight: "bold",
                            color: "white"
                          }}
                        >
                          MOST POPULAR
                        </Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "column",
                          justifyContent: "space-around",
                          alignItems: "center",
                          backgroundColor: "white",
                          borderBottomLeftRadius: 12,
                          borderBottomRightRadius: 12
                        }}
                      >
                        <Text
                          style={{
                            fontSize: moderateScale(20),
                            fontWeight: "bold"
                            // marginTop: moderateScale(2),
                          }}
                        >
                          6 Months
                        </Text>
                        <Text
                          style={{
                            fontSize: moderateScale(25),
                            fontWeight: "bold"
                            // bottom: moderateScale(2),
                          }}
                        >
                          {"\u00A3"}4.99
                        </Text>
                        <Text style={{ textAlign: "center" }}>
                          {"\u00A3"}29.99 one{"\n"}off fee
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      textAlign: "center",
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: moderateScale(30)
                      }}
                    >
                      Recurring Bill. Cancel Anytime.
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        this.showModal("http://osce.ai/terms-conditions/")
                      }
                    >
                      <Text
                        style={{
                          textDecorationLine: "underline",
                          alignSelf: "center",
                          fontWeight: "bold",
                          marginTop: moderateScale(5)
                        }}
                      >
                        Terms & Conditions
                      </Text>
                    </TouchableOpacity>
                    <Text
                      style={{
                        textAlign: "justify",
                        marginTop: moderateScale(8),
                        fontSize: 10
                      }}
                    >
                      Subscription terms: The monthly subscription is £6.99
                      {"\n"}and automatically renews every month. The 6 monthly
                      {"\n"}
                      subscription is £29.99 and is charged every 6 months.
                    </Text>
                  </View>
                  <SequareButton
                    onPress={() => {}}
                    loading={false}
                    style={{
                      // marginBottom: moderateScale(10),
                      marginTop: moderateScale(30),
                      width: "90%",
                      backgroundColor: Constants.Colors.blue
                    }}
                    text="SUBSCRIBE NOW"
                  />
                </ImageBackground>
              </View> */}

              {/* <View
                style={{
                  flex: 1,
                  height: deviceHeight,
                  width: deviceWidth,
                  backgroundColor: "white"
                }}
              >
                <TouchableOpacity
                  style={{
                    // marginVertical: moderateScale(30),
                    marginRight: moderateScale(20),
                    borderRadius: moderateScale(50),
                    width: 38,
                    alignSelf: "flex-end",
                    top: moderateScale(50),
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 2
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,

                    elevation: 5
                  }}
                  onPress={() => this.showScreen()}
                >
                  <Image
                    source={require("../../assets/img/closeS.png")}
                    style={{
                      height: moderateScale(35),
                      width: moderateScale(35)
                    }}
                  />
                </TouchableOpacity>
                <ImageBackground
                  source={require("../../assets/img/bgS.png")}
                  style={{
                    flex: 1,
                    resizeMode: "cover",
                    width: "100%",
                    height: "100%",
                    marginTop:
                      deviceWidth > 375
                        ? deviceWidth / 2 - moderateScale(2)
                        : deviceWidth / 2 - moderateScale(140),
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      fontSize: moderateScale(20),
                      fontWeight: "bold",
                      marginTop: moderateScale(40)
                    }}
                  >
                    Everything you need to know
                  </Text>
                  <Text
                    style={{
                      fontSize: moderateScale(15),
                      textAlign: "center",
                      marginTop: moderateScale(10)
                    }}
                  >
                    Unlock condition fact sheets to{"\n"}increase your knowledge
                    base
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-evenly",
                      width: "30%",
                      alignItems: "center",
                      marginTop: moderateScale(20)
                    }}
                  >
                    <Text style={{ color: Constants.Colors.gray, opacity:0.6 }}>
                      {"\u2B24"}
                    </Text>
                    <Text style={{ color: Constants.Colors.gray, opacity:0.6 }}>
                      {"\u2B24"}
                    </Text>
                    <Text>{"\u2B24"}</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-evenly",
                      width: "100%",
                      marginTop: moderateScale(30)
                    }}
                  >
                    <TouchableOpacity style={styles.square2}>
                      <View
                        style={{
                          backgroundColor: "#fff",
                          width: "100%",
                          flexDirection: "row",
                          justifyContent: "center",
                          borderTopLeftRadius: 12,
                          borderTopRightRadius: 12
                        }}
                      >
                        <Text
                          style={{
                            fontSize: moderateScale(15),
                            fontWeight: "bold",
                            color: "white"
                          }}
                        >
                          MOST POPULAR
                        </Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "column",
                          justifyContent: "space-around",
                          alignItems: "center",
                          backgroundColor: "white",
                          borderBottomLeftRadius: 12,
                          borderBottomRightRadius: 12
                        }}
                      >
                        <Text
                          style={{
                            fontSize: moderateScale(20),
                            fontWeight: "bold",
                            marginTop: moderateScale(10)
                          }}
                        >
                          Monthly
                        </Text>
                        <Text
                          style={{
                            fontSize: moderateScale(25),
                            fontWeight: "bold",
                            bottom: moderateScale(8)
                          }}
                        >
                          {"\u00A3"}6.99
                        </Text>
                        <Text style={{ bottom: moderateScale(5) }}>
                          PER MONTH
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.square2}>
                      <View
                        style={{
                          backgroundColor: "#1c7cce",
                          width: "100%",
                          flexDirection: "row",
                          justifyContent: "center",
                          borderTopLeftRadius: 12,
                          borderTopRightRadius: 12,
                          paddingVertical: moderateScale(5)
                        }}
                      >
                        <Text
                          style={{
                            fontSize: moderateScale(15),
                            fontWeight: "bold",
                            color: "white"
                          }}
                        >
                          MOST POPULAR
                        </Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "column",
                          justifyContent: "space-around",
                          alignItems: "center",
                          backgroundColor: "white",
                          borderBottomLeftRadius: 12,
                          borderBottomRightRadius: 12
                        }}
                      >
                        <Text
                          style={{
                            fontSize: moderateScale(20),
                            fontWeight: "bold"
                            // marginTop: moderateScale(2),
                          }}
                        >
                          6 Months
                        </Text>
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <Text
                            style={{
                              fontSize: moderateScale(25),
                              fontWeight: "bold",
                            }}
                          >
                            {"\u00A3"}4.99
                          </Text>
                          <Text
                            style={{
                              fontSize: moderateScale(16),
                              top: moderateScale(2),
                            }}
                          >
                            /mo
                          </Text>
                        </View>
                        <Text style={{ textAlign: "center" }}>
                          {"\u00A3"}29.99 one{"\n"}off fee
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      textAlign: "center",
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: moderateScale(30)
                      }}
                    >
                      Recurring Bill. Cancel Anytime.
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        this.showModal("http://osce.ai/terms-conditions/")
                      }
                    >
                      <Text
                        style={{
                          textDecorationLine: "underline",
                          alignSelf: "center",
                          fontWeight: "bold",
                          marginTop: moderateScale(5)
                        }}
                      >
                        Terms & Conditions
                      </Text>
                    </TouchableOpacity>
                    <Text
                      style={{
                        textAlign: "justify",
                        marginTop: moderateScale(8),
                        fontSize: 10
                      }}
                    >
                      Subscription terms: The monthly subscription is £6.99
                      {"\n"}and automatically renews every month. The 6 monthly
                      {"\n"}
                      subscription is £29.99 and is charged every 6 months.
                    </Text>
                  </View>
                  <SequareButton
                    onPress={() => {}}
                    loading={false}
                    style={{
                      // marginBottom: moderateScale(10),
                      marginTop: moderateScale(30),
                      width: "90%",
                      backgroundColor: Constants.Colors.blue
                    }}
                    text="SUBSCRIBE NOW"
                  />
                </ImageBackground>
              </View> */}
            </Modal>
            <View style={styles.greyLine} />
            <FlatList
              data={scores.listing || []}
              extraData={this.state}
              keyExtractor={(item, index) => item.toString() + index.toString()}
              renderItem={(item, index) => this.renderMainListItem(item, index)}
            />
          </ScrollView>
        )}
        <View>
          <Modal
            animationType="slide"
            transparent={false}
            style={{ marginTop: moderateScale(20) }}
            visible={this.state.openModal}
          >
            <TouchableOpacity
              style={{
                marginVertical: moderateScale(30),
                marginRight: moderateScale(20),
              }}
              onPress={() => this.showModal()}
            >
              <Text
                style={{
                  fontSize: moderateScale(25),
                  fontWeight: "bold",
                  alignSelf: "flex-end",
                }}
              >
                X
              </Text>
            </TouchableOpacity>
            <WebView
              source={{ uri: this.state.url }}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              onLoadStart={() => this.setState({ visible: true })}
              onLoad={() => this.setState({ visible: false })}
            />
            {this.state.visible ? this.ActivityIndicatorElement() : null}
          </Modal>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: Constants.Colors.appBackgroundColor,
  },
  square: {
    width: 180,
    height: 180,
    backgroundColor: "white",
    borderRadius: 10,
    borderWidth: moderateScale(0),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: moderateScale(2) },
    shadowOpacity: moderateScale(0.3),
    shadowRadius: moderateScale(2),
    elevation: moderateScale(3),
  },
  activityIndicatorStyle: {
    flex: 1,
    position: "absolute",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "auto",
    marginBottom: "auto",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: "center",
  },
  square2: {
    width: "40%",
    height: 180,
    marginLeft: 10,
    borderRadius: 10,
    // borderWidth: moderateScale(2),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: moderateScale(2) },
    shadowOpacity: moderateScale(0.3),
    shadowRadius: moderateScale(2),
    elevation: moderateScale(3),
  },
  greyLine: {
    marginVertical: moderateScale(20),
    height: moderateScale(2),
    flex: 1,
    backgroundColor: Constants.Colors.placehoder,
  },
  firstContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: moderateScale(10),
  },
  secondContainer: {
    marginVertical: moderateScale(20),
  },
  resultIcon: {
    height: moderateScale(80),
    width: moderateScale(80),
  },
  lockIcon: {
    height: moderateScale(60),
    width: moderateScale(60),
    alignSelf: "center",
  },
  converText: {
    color: Constants.Colors.Black,
    fontSize: moderateScale(22),
    fontWeight: "600",
  },
  gradeText: {
    fontSize: moderateScale(35),
    color: Constants.Colors.Black,
    fontWeight: "700",
  },
  percentBox: {
    backgroundColor: Constants.Colors.parrotGreen,
    padding: moderateScale(4),
  },
  popupText: {
    fontSize: moderateScale(20),
    marginVertical: moderateScale(40),
  },

  firstView: {
    width: deviceWidth,
    height: "100%",
    backgroundColor: "#F44336",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  secondView: {
    width: deviceWidth,
    backgroundColor: "#9C27B0",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  thirdView: {
    width: deviceWidth,
    backgroundColor: "#3F51B5",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  forthView: {
    width: deviceWidth,
    backgroundColor: "#009688",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
});

function mapStateToProps(state) {
  return {
    scoreData: state.Score.scores,
    scoreLoader: state.Score.loader,
  };
}

const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Feedback);
