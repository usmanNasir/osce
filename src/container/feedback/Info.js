import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  SafeAreaView,
} from "react-native";
import { connect } from "react-redux";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import { logger } from "./../../utilities/analytics-logger";
import ImageLoad from "../../components/ImageLoad";
import Constants from "../../constants";
import Header from "../Header";
import InfoTiles from "../../components/InfoTiles";
import ControlledLoader from "../../components/common/ControlledLoader";
import * as AppAction from "../../actions";

class Info extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      errors: {},
      errorMessage: ""
    };
  }

  componentDidMount() {
    logger.logEventWithData("infoPage", { category: this.props.name });
  }

  componentWillUnmount() {
    this.props.AppAction.resetInfoData();
  }

  render() {
    const { loader, infoData } = this.props;
    let complaintData = {};
    if (!infoData) {
      alert("No information.");
      Navigation.pop("Home");
    } else {
      complaintData = infoData[0];
    }
const previousScreen = this.props?.props?.isDifferentStack ? "HomeScreen" : "Home";
    return (
      <SafeAreaView
        style={{
          flex: 1,
          paddingTop: moderateScale(20),
          backgroundColor: Constants.Colors.appBackgroundColor
        }}
      >
        <Header previousScreen={previousScreen} text={this.props.name} border={true} />
        {loader ?
            <ControlledLoader showLoader={loader}/>
            : (
          <ScrollView
            style={{
              marginTop: moderateScale(5),
              backgroundColor: Constants.Colors.appBackgroundColor,
              paddingHorizontal: moderateScale(15)
            }}
          >
            <View style={styles.firstContainer}>
              <View style={{ width: moderateScale(240) }}>
                <Text style={styles.gradeText}>{complaintData.title}</Text>
              </View>
              <View style={{ justifyContent: "center" }}>
                <ImageLoad
                  isShowActivity={false}
                  style={{
                    height: moderateScale(70),
                    width: moderateScale(70)
                  }}
                  placeholderStyle={{
                    height: moderateScale(70),
                    width: moderateScale(70)
                  }}
                  loadingStyle={{ size: "small", color: "blak" }}
                  source={{ uri: complaintData.iconUrl }}
                  placeholderSource={{
                    uri: complaintData.thumbnailUrl
                      ? complaintData.thumbnailUrl
                      : null
                  }}
                  resizeMode="contain"
                />
              </View>
            </View>
            <View style={styles.greyLine} />
            <InfoTiles data={complaintData.tileData} />
          </ScrollView>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  firstContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: moderateScale(10)
  },
  greyLine: {
    height: moderateScale(2),
    flex: 1,
    backgroundColor: Constants.Colors.placehoder,
    marginVertical: moderateScale(20)
  },
  gradeText: {
    fontSize: moderateScale(35),
    color: Constants.Colors.Black,
    fontWeight: "700"
  }
});

function mapStateToProps(state) {
  return {
    infoData: state.Score.infoData,
    loader: state.Score.loader
  };
}

const mapDispatchToProps = dispatch => ({
  AppAction: bindActionCreators(AppAction, dispatch),
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Info);
