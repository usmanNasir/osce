import React from "react";
import {
  Case_Completed,
  Case_Left_UnCompleted,
  Time_Spent_Conditions,
} from "../../constants/Events";
import { View, Text, StyleSheet, ScrollView, SafeAreaView } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { logger } from "./../../utilities/analytics-logger";
import { Navigation } from "react-native-navigation";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { INITIAL_STATE } from "../../reducers/appConfig/index";
import { MixPanelContext } from "../../utilities/MixPanel";
import Constants from "../../constants";
import Header from "../Header";
import InfoTiles from "../../components/InfoTiles";
import ControlledLoader from "../../components/common/ControlledLoader";
import * as AppAction from "../../actions";

class ConditionPage extends React.Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      errors: {},
      errorMessage: "",
      Grades: [],
      gradingList: [],
      startTime: null,
    };
  }
  async componentDidMount() {
    await this.getScoreData();
    this.setState({
      startTime: new Date().getTime(),
    });
    logger.logEventWithData("conditionPage", { category: this.props.name });
    this.getDaataa();
  }

  componentWillUnmount() {
    const difference = new Date().getTime() - this.state.startTime;
    this.context.mixPanel.track(Time_Spent_Conditions, {
      TimeSpent: `${difference / 60000} minutes`,
    });
    this.props.AppAction.resetConditionsData();
  }

  getDaataa = async () => {
    let response = await fetch(`${INITIAL_STATE.serverUrl}/history/get`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.Auth.token,
      },
    });
    let responseJson = await response.json();
    this.setState(
      {
        gradingList: responseJson.data.histories,
      },
      function() {
        this.getProgress();
      }
    );
  };

  getProgress = () => {
    let Data = this.state.gradingList;
    function onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
    }
    let newArr = [];
    let newArr2 = [];
    if (Data != undefined) {
      for (var i = 0; i < Data.length; i++) {
        newArr[i] = Data[i].complaintId;
      }
      for (var i = 0; i < Data.length; i++) {
        newArr2.push({
          complaintId: Data[i].complaintId,
          grade: Data[i].grade,
        });
      }
      let gradingArrayList = [];
      var unique = newArr.filter(onlyUnique);
      for (var j = 0; j < unique.length; j++) {
        gradingArrayList.push({
          count: 0,
          complaintId: unique[j],
        });
      }
      for (var j = 0; j < gradingArrayList.length; j++) {
        for (var k = 0; k < newArr2.length; k++) {
          if (unique[j] == newArr2[k].complaintId) {
            if (
              newArr2[k].grade == "A" ||
              newArr2[k].grade == "B" ||
              newArr2[k].grade == "C"
            ) {
              gradingArrayList[j].count++;
            }
          }
        }
      }
      this.state.Grades = gradingArrayList;
      var num = gradingArrayList.reduce(function(n, person) {
        return n + person.count;
      }, 0);
    } 
  };

  goToHistoryScreen() {
    this.props.AppAction.resetChatBotDataExam();
    this.props.AppAction.resetChatBotDataInvestigation();
    this.props.isDifferentStack && this.props.AppAction.triggerMarginchange();
    if (this.props.isChallenge) {
      this.props.AppAction.triggerMarginchange();
      Navigation.popToRoot("HomeScreen");
    } else {
      this.context.mixPanel.track(Case_Completed);
      Navigation.setStackRoot(this.props.componentId, {
        component: {
          name: "ComplaintTree",
          passProps: {
            item: this.props.complaintData,
            Grades: this.state.Grades,
            isDifferentStack: this.props.isDifferentStack ? true : false,
          },
          options: {
            topBar: {
              visible: false,
              title: {
                text: "Home",
              },
            },
            bottomTab: {
              visible: true,
            },
            bottomTabs: {
              visible: true,
            },
          },
        },
      });
    }
  }

  async getScoreData() {
    await this.props.AppAction.getScores(
      this.props.item._id,
      this.props.item.complaintId,
      this.props.sessionId,
      true,
      this.props.awardDignosisScore,
      this.props.awardScore
    );
  }

  render() {
    const { loader, conditionsData } = this.props;
    let dataToRender = {};
    if (conditionsData) {
      dataToRender = conditionsData[0];
    }
    const previousScreen = this.props.isDifferentStack ? "HomeScreen" : "Home";
    return (
      <SafeAreaView
        style={{
          flex: 1,
          paddingTop: moderateScale(20),
          backgroundColor: Constants.Colors.appBackgroundColor,
        }}
      >
        <Header
          mixPanelEvent={() =>
            this.context.mixPanel.track(Case_Left_UnCompleted)
          }
          previousScreen={previousScreen}
          text={this.props.name}
          border={true}
          viewShadow="yes"
          checkMark={true}
          componentId={this.props.componentId}
          nextComponent={this.goToHistoryScreen.bind(this)}
        />
        {loader ? (
          <ControlledLoader showLoader={loader} />
        ) : (
          <ScrollView
            style={{
              marginTop: moderateScale(5),
              backgroundColor: Constants.Colors.appBackgroundColor,
              paddingHorizontal: moderateScale(15),
            }}
          >
            <View style={styles.firstContainer}>
              <Text style={styles.gradeText}>
                <Text style={{ color: Constants.Colors.caseColor }}>
                  {dataToRender?.case}{" "}
                </Text>
                {dataToRender?.title}{" "}
              </Text>
            </View>
            <View style={styles.greyLine} />
            <View>
              <Text
                style={{
                  fontSize: moderateScale(22),
                  color: Constants.Colors.Black,
                }}
              >
                {dataToRender?.subTitle}
              </Text>
              <InfoTiles data={dataToRender?.tileData} />
            </View>
          </ScrollView>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  firstContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: moderateScale(10),
  },
  greyLine: {
    height: moderateScale(2),
    flex: 1,
    backgroundColor: Constants.Colors.placehoder,
    marginVertical: moderateScale(20),
  },
  gradeText: {
    fontSize: moderateScale(35),
    color: Constants.Colors.Black,
    fontWeight: "700",
  },
});
function mapStateToProps(state) {
  return {
    conditionsData: state.Score.conditionsData,
    loader: state.Score.loader,
    Auth: state.Auth.authData,
    scores: state.Score.scores,
  };
}
const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
  setScrenStack: (componentId, name, visible) =>
    dispatch(setScrenStack(componentId, name, visible)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConditionPage);
