import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Dimensions,
  Image,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  AsyncStorage,
} from "react-native";
import { moderateScale } from "../../helpers/ResponsiveFonts";
var deviceWidth = Dimensions.get("window").width;
import { Navigation } from "react-native-navigation";
import constants from "../../constants";
import { MixPanelContext } from "../../utilities/MixPanel";
import { Activity } from "../../constants/Events";

export default class Tutorial extends Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    console.log("tutorial routes", this.props);
    // let showTutorial = await AsyncStorage.setItem('showTutorial', 'false')
  }
  nextScreen = () => {
    if (this.props.route === "Profile") {
      Navigation.push(this.props.componentId, {
        component: {
          name: "Home",
          passProps: {},
          options: {
            topBar: {
              visible: false,
            },
          },
        },
      });
    } else {
      this.context.mixPanel.track(Activity.UserSkippedOnBoardingActivity);
      Navigation.push(this.props.componentId, {
        component: {
          name: "FirstScreen",
          passProps: {},
          options: {
            topBar: {
              visible: false,
            },
          },
        },
      });
    }
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          horizontal={true}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
        >
          <ImageBackground
          resizeMode="cover"
            source={require("../../assets/Onboarding/tu1.png")}
            style={styles.firstView}
          >
            <TouchableOpacity
              onPress={this.nextScreen}
              style={{
                backgroundColor: constants.Colors.White,
                alignSelf: "flex-start",
                marginBottom: deviceWidth / 11,
                height: 40,
                width: 40,
                alignItems: "center",
                justifyContent: "center",
                marginLeft: deviceWidth / 1.2,
                marginTop: 10,
              }}
            >
              <Image
                source={require("../../assets/img/closeS.png")}
                height={40}
                width={40}
                style={{  height: 40, width: 40 }}
              />
            </TouchableOpacity>
          </ImageBackground>
          <ImageBackground
                   resizeMode="cover"
            source={require("../../assets/Onboarding/tu2.png")}
            style={styles.firstView}
          >
            <TouchableOpacity
              onPress={this.nextScreen}
              style={{
                backgroundColor: constants.Colors.White,
                alignSelf: "flex-start",
                marginBottom: deviceWidth / 11,
                height: 40,
                width: 40,
                alignItems: "center",
                justifyContent: "center",
                marginLeft: deviceWidth / 1.2,
                marginTop: 10,
              }}
            >
              <Image
                source={require("../../assets/img/closeS.png")}
                height={40}
                width={40}
                style={{  height: 40, width: 40 }}
              />
            </TouchableOpacity>
          </ImageBackground>
          <ImageBackground
                   resizeMode="cover"
            source={require("../../assets/Onboarding/tu3.png")}
            style={styles.firstView}
          >
            <TouchableOpacity
              onPress={this.nextScreen}
              style={{
                backgroundColor: constants.Colors.White,
                alignSelf: "flex-start",
                marginBottom: deviceWidth / 11,
                height: 40,
                width: 40,
                alignItems: "center",
                justifyContent: "center",
                marginLeft: deviceWidth / 1.2,
                marginTop: 10,
              }}
            >
              <Image
                source={require("../../assets/img/closeS.png")}
                height={40}
                width={40}
                style={{  height: 40, width: 40 }}
              />
            </TouchableOpacity>
          </ImageBackground>
          <ImageBackground
                   resizeMode="cover"
            source={require("../../assets/Onboarding/tu4.png")}
            style={styles.firstView}
          >
            <TouchableOpacity
              onPress={this.nextScreen}
              style={{
                backgroundColor: constants.Colors.White,
                alignSelf: "flex-start",
                marginBottom: deviceWidth / 11,
                height: 40,
                width: 40,
                alignItems: "center",
                justifyContent: "center",
                marginLeft: deviceWidth / 1.2,
                marginTop: 10,
              }}
            >
              <Image
                source={require("../../assets/img/closeS.png")}
                height={40}
                width={40}
                style={{  height: 40, width: 40 }}
              />
            </TouchableOpacity>
          </ImageBackground>
          <ImageBackground
                   resizeMode="cover"
            source={require("../../assets/Onboarding/tu5.png")}
            style={styles.firstView}
          >
             <TouchableOpacity
              onPress={this.nextScreen}
              style={{
                position:'absolute',
                top:10,
                right: 10,
                // backgroundColor: constants.Colors.White,
                // alignSelf: "flex-start",
                // marginBottom: deviceWidth / 11,
                // height: 40,
                // width: 40,
                // alignItems: "center",
                // justifyContent: "center",
                // marginLeft: deviceWidth / 1.2,
                // marginTop: 10,
              }}
            >
              <Image
                source={require("../../assets/img/closeS.png")}
                height={40}
                width={40}
                style={{  height: 40, width: 40 }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.nextScreen}
              style={{
                backgroundColor: constants.Colors.White,
                alignSelf: "flex-end",
                marginBottom: deviceWidth / 11,
                height: 70,
                width: 70,
                // width: "15%",
                // height: "8%",
                borderRadius: 35,
                alignItems: "center",
                justifyContent: "center",
                marginLeft: deviceWidth / 1.5,
                borderColor: constants.Colors.shadownColor,
                borderWidth: moderateScale(0),
                shadowColor: "#000",
                shadowOffset: { width: 0, height: moderateScale(2) },
                shadowOpacity: moderateScale(0.3),
                shadowRadius: moderateScale(2),
                elevation: moderateScale(3),
              }}
            >
              <Image
                source={require("../../assets/img/Forward.png")}
                height={30}
                width={30}
                style={{  height: 30, width: 30 }}
              />
            </TouchableOpacity>
          </ImageBackground>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "#80b6ec",
  },
  headerText: {
    fontSize: 30,
    textAlign: "center",
    margin: 10,
    color: "white",
    fontWeight: "bold",
  },
  firstView: {
    width: deviceWidth,
    height: "100%",
    backgroundColor: "#F44336",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  secondView: {
    width: deviceWidth,
    backgroundColor: "#9C27B0",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  thirdView: {
    width: deviceWidth,
    backgroundColor: "#3F51B5",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  forthView: {
    width: deviceWidth,
    backgroundColor: "#009688",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
});
