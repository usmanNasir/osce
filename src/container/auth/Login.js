import React from "react";
import { connect } from "react-redux";

import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  Platform,
    Alert
} from "react-native";
import { Colors } from "react-native/Libraries/NewAppScreen";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes
} from "react-native-google-signin";
import  {
  AppleButton,
  AppleAuthError,
  AppleAuthRequestScope,
  AppleAuthRequestOperation,
    appleAuth
} from '@invertase/react-native-apple-authentication'
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { bindActionCreators } from "redux";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { Navigation } from "react-native-navigation";
import { MixPanelContext } from "../../utilities/MixPanel";
import { Activity, App_Launch, User_Login } from "../../constants/Events";
import Constants from "../../constants";
import { SequareButton, TextField } from "../../components/common";
import { checkValidation } from "../../config/validation";
import * as AppAction from "../../actions";
import Header from "../Header";
import WebViewComponent from "./WebViewComponent";

import ControlledLoader from "../../components/common/ControlledLoader";

class Login extends React.Component {

static contextType = MixPanelContext;
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      errors: {},
      errorMessage: "",

      userInfo: null,
      gettingLoginStatus: true
    };

    this.onChangeText = this.onChangeText.bind(this);
  }

  componentWillUnmount() {
    //this.props.AppAction.resetLoginData();
  }

  componentDidMount() {
    setTimeout(()=>this.context.mixPanel.track(Activity.userLoginActivity),3000);
    //initial configuration
    // GoogleSignin.configure({
    //   //It is mandatory to call this method before attempting to call signIn()
    //   scopes: ["https://www.googleapis.com/auth/drive.readonly"],
    //   // Repleace with your webClientId generated from Firebase console
    //   webClientId:
    //     "863583170600-17s25342dahgfcj6jg1d20nlvj9s1k68.apps.googleusercontent.com",
    // });
    // //Check if user is already signed in
    // this._isSignedIn();

  }

  // _isSignedIn = async () => {
  //   const isSignedIn = await GoogleSignin.isSignedIn();
  //   if (isSignedIn) {
  //     alert("User is already signed in");
  //     //Get the User details as user is already signed in
  //     this._getCurrentUserInfo();
  //   } else {
  //     //alert("Please Login");

  //   }
  //   this.setState({ gettingLoginStatus: false });
  // };

  // _getCurrentUserInfo = async () => {
  //   try {
  //     const userInfo = await GoogleSignin.signInSilently();
  //     this.setState({ userInfo: userInfo });
  //   } catch (error) {
  //     if (error.code === statusCodes.SIGN_IN_REQUIRED) {
  //       alert("User has not signed in yet");
  //     } else {
  //       alert("Something went wrong. Unable to get user's info");
  //     }
  //   }
  // };

  // _signIn = async () => {
  //   //Prompts a modal to let the user sign in into your application.
  //   try {
  //     await GoogleSignin.hasPlayServices({
  //       //Check if device has Google Play Services installed.
  //       //Always resolves to true on iOS.
  //       showPlayServicesUpdateDialog: true,
  //     });
  //     const userInfo = await GoogleSignin.signIn();
  //     this.setState({ userInfo: userInfo });
  //   } catch (error) {
  //     console.log("Message", error.message);
  //     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
  //       console.log("User Cancelled the Login Flow");
  //     } else if (error.code === statusCodes.IN_PROGRESS) {
  //       console.log("Signing In");
  //     } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
  //       console.log("Play Services Not Available or Outdated");
  //     } else {
  //       console.log("Some Other Error Happened");
  //     }
  //   }
  // };
  // loginWithG = () => {
  //   console.log(this.props.AppAction);
  //   this.props.AppAction._signIn();
  // };

  // _signOut = async () => {
  //   //Remove user session from the device.
  //   try {
  //     await GoogleSignin.revokeAccess();
  //     await GoogleSignin.signOut();
  //     this.setState({ userInfo: null }); // Remove the user from your app's state as well
  //   } catch (error) {
  //     console.error(error);
  //   }
  // };

  onChangeText(name, value) {
    let errors = this.state.errors;
    errors[name] = "";
    this.setState({
      [name]: name === "email" ? value.toLowerCase() : value,
      errors: { ...errors, both: false },
      errorMessage: ""
    });
  }

  // static getDerivedStateFromProps(nextProps) {
  //   if (nextProps.authRes) {
  //     const{statusCode="", message=""} = nextProps.authRes
  //     if (
  //       statusCode &&
  //       statusCode !== 200
  //     ) {
  //       return {
  //         errorMessage: message
  //       };
  //     }
  //   }

  //   if (nextProps.error) {
  //     if (nextProps.error && nextProps.error.message) {
  //       return {
  //         errorMessage: nextProps.error.message
  //       };
  //     }
  //   }
  // }
  loginWithFb = () => {
    this.props.AppAction.loginWithFb();

  };
  login = async () => {
    let validationCheck = checkValidation(this.state);

    this.setState({
      errors: validationCheck
    });

    if (!Object.keys(validationCheck).length) {
      let params = {
        email: this.state.email,
        password: this.state.password
      };
      const {
        statusCode = "",
        message = ""
      } = await this.props.AppAction.login(params);
      this.context.mixPanel.track(User_Login,{email:this.state.email});
      statusCode !== 200 &&
        this.setState({
          errors: { both: statusCode !== 200 },
          errorMessage: statusCode !== 200 ? message : ""
        });
    }
  };
   onAppleButtonPress = async () => {
     // const responseLogout =  await appleAuth.performRequest({
     //   requestedOperation: appleAuth.Operation.LOGOUT
     // });
     // console.log("response Logout function",responseLogout);

     // start a login request




     try {

       const appleAuthRequestResponse = await appleAuth.performRequest({
         requestedOperation: appleAuth.Operation.LOGIN,
         // requestedScopes:AppleAuthRequestScope
         requestedScopes: [
           appleAuth.Scope.EMAIL,
           appleAuth.Scope.FULL_NAME,
         ],
       });
       const {
         user: newUser,
         email,
         nonce,
         identityToken,
         realUserStatus /* etc */,
           fullName,
       } = appleAuthRequestResponse;

       this.user = newUser;
       const userObj ={
         name:fullName?.givenName + fullName?.familyName,
         email,
         identityToken,
         user:newUser
       }




       if (identityToken) {
         // e.g. sign in with Firebase Auth using `nonce` & `identityToken`
         this.fetchAndUpdateCredentialState(this.user,userObj)
             .then(res => {
                   this.setState({credentialStateForUser: res});

                 }
             )
             .catch(error =>
                 this.setState({ credentialStateForUser: `Error: ${error.code}` }),
             );

       } else {
         Alert.alert("Failed","Invalid Credentials try Again",[{text:'OK'}]);
         // no token - failed sign-in?
       }

     } catch (error) {
       if (error.code === appleAuth.Error.CANCELED) {
         console.log('User canceled Apple Sign in.');
       } else {
         console.log("error",error);
       }
     }
   };
  fetchAndUpdateCredentialState = async (usertoken,user) => {
    if (usertoken === null) {
      this.setState({ credentialStateForUser: 'N/A' });
    } else {

      const credentialState = await appleAuth.getCredentialStateForUser(usertoken);
      if (credentialState === appleAuth.State.AUTHORIZED) {
        this.setState({ credentialStateForUser: 'AUTHORIZED' });
        this.props.AppAction.apple_signIn(user);
      } else {
        this.setState({ credentialStateForUser: credentialState });
      }
    }
  }

  previousScreen = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "FirstScreen",
        passProps: {},
        options: {
          topBar: {
            visible: false
          }
        }
      }
    });
  };

  render() {
    const { email, password, errors, errorMessage } = this.state;

    return (
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
              <View style={styles.content}>
              <View>
              <Header
                previousComponent={this.previousScreen}
                text="Welcome Back"
                navigate={true}
              />

              <View style={{ left: moderateScale(50) }}>
                <Text
                  style={{
                    color: Constants.Colors.gunPowderShade,
                    fontSize: moderateScale(40)
                  }}
                >
                  Hey.
                </Text>

                <Text style={styles.practisingText}>Let&apos;s keep</Text>

                <Text style={styles.practisingText}>practising!</Text>
              </View>
            </View>

            <View style={{ height: "40%" }}>
              <TextField
                onChange={this.onChangeText}
                value={email}
                stateVariable="email"
                type="email"
                keyboardType="email-address"
                placeholderValue="Email"
                errors={errors}
              />
              <TextField
                onChange={this.onChangeText}
                value={password}
                type="password"
                stateVariable="password"
                placeholderValue="Password"
                errors={errors}
              />

              {errorMessage ? (
                <View>
                  <Text
                    style={{
                      fontSize: moderateScale(12),
                      // fontWeight: "bold",
                      color: Constants.Colors.errorRed
                    }}
                  >
                    {errorMessage}
                  </Text>
                </View>
              ) : null}
            </View>

            <View>
              <SequareButton
                onPress={() => this.login()}
                loading={false}
                style={{
                  marginBottom: moderateScale(10),
                  backgroundColor: Constants.Colors.blue
                }}
                text="LOG IN"
              />
              {/* <SequareButton
                onPress={this.props.AppAction._signIn}
                loading={false}
                style={{
                  marginBottom: moderateScale(10),
                  backgroundColor: Constants.Colors.manatee
                }}
                text="LOG IN WITH GOOGLE"
              />
              {Platform.OS === "android" ? (
                <SequareButton
                  onPress={this.loginWithFb}
                  loading={false}
                  style={{
                    marginBottom: moderateScale(10),
                    backgroundColor: Constants.Colors.manatee
                  }}
                  text="LOG IN WITH FACEBOOK"
                />
              ) : null}
              {Platform.OS === "ios"?(
                  <SequareButton
                      onPress={() => this.onAppleButtonPress()}
                      loading={false}
                      style={{
                        marginBottom: moderateScale(10),
                        backgroundColor: Constants.Colors.manatee
                      }}
                      text="LOG IN WITH APPLE"
                  />
                  // <AppleButton
                  //     buttonStyle={AppleButton.Style.WHITE}
                  //     buttonType={AppleButton.Type.SIGN_IN}
                  //     style={styles.appleButton}
                  //     // style={{
                  //     //   width: 160, // You must specify a width
                  //     //   height: 45, // You must specify a height
                  //     // }}
                  //     onPress={() => this.onAppleButtonPress()}
                  // />
              ):null} */}

              {/* <WebViewComponent /> */}
            </View>
            <View style={{ height: moderateScale(20) }} />
          </View>
     {   this.props.loader &&
        <ControlledLoader showLoader={this.props.loader}/>}
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  appleButton: {
    width: '100%',
    height: 55,
    shadowColor: '#555',
    shadowOpacity: 0.5,
    shadowOffset: {
      width: 0,
      height: 3
    },
    marginBottom: 10,
  },
  container: {
    flexGrow: 1,
    backgroundColor: Constants.Colors.appBackgroundColor
  },
  content: {
    flexDirection: "column",
    justifyContent: "space-between",
    padding: moderateScale(20),
    height: "100%",
    paddingBottom: "10%"
  },
  practisingText: {
    fontSize: moderateScale(22),
    color: Constants.Colors.darkGrey1
  },

});

function mapStateToProps(state) {
  return {
    authRes: state,
    error: state.Auth.error,
    loader: state.Auth.loader
  };
}

const mapDispatchToProps = dispatch => ({
  AppAction: bindActionCreators(AppAction, dispatch),
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
