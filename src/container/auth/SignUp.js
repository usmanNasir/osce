import React from "react";
import { connect } from "react-redux";

import { View, Text, StyleSheet, ActivityIndicator } from "react-native";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { bindActionCreators } from "redux";

import WebViewComponent from "./WebViewComponent";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import Constants from "../../constants";
import { SequareButton, TextField } from "../../components/common";
import { checkValidation } from "../../config/validation";
import * as AppAction from "../../actions";
import Header from "../Header";
import { Navigation } from "react-native-navigation";
import { MixPanelContext } from "../../utilities/MixPanel";
import { Account_Activation, Activity } from "../../constants/Events";
import ControlledLoader from "../../components/common/ControlledLoader";


class SignUp extends React.Component {

static contextType = MixPanelContext;
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      university: "",
      year_group: "",
      email: "",
      password: "",
      job_title: "",
      errors: {},
      errorMessage: ""
    };

    this.onChangeText = this.onChangeText.bind(this);
  }

  //  mixPanel = this.context.mixPanel;



  componentWillUnmount() {
    //this.props.AppAction.resetLoginData();
  }

  onChangeText(name, value) {
    let errors = this.state.errors;
    errors[name] = "";
    this.setState({
      [name]: name === "email" ? value.toLowerCase() : value,
      errors,
      errorMessage: ""
    });
  }

  // static getDerivedStateFromProps(nextProps) {
  //   if (nextProps.authRes) {
  //     if (
  //       nextProps.authRes.statusCode &&
  //       nextProps.authRes.statusCode !== 200
  //     ) {
  //       return {
  //         errorMessage: nextProps.authRes.message
  //       };
  //     }
  //   }

  //   if (nextProps.error) {
  //     if (nextProps.error && nextProps.error.message) {
  //       return {
  //         errorMessage: nextProps.error.message
  //       };
  //     }
  //   }
  // }

  previousScreen = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "FirstScreen",
        passProps: {},
        options: {
          topBar: {
            visible: false
          }
        }
      }
    });
  };

componentDidMount(){
  setTimeout(()=>  this.context.mixPanel.track(Activity.userSignUpActivity),3000);

}

  signUp = async () => {
    let validationCheck = checkValidation(this.state, "signup");
    this.setState({
      errors: validationCheck
    });

    if (!Object.keys(validationCheck).length) {
      let params = {
        email: this.state.email,
        password: this.state.password,
        name: this.state.name,
        university: this.state.university,
        year_group: this.state.year_group,
        job_title: this.state.job_title,
        source: "mobile",
        type: "general"
      };
      const {
        statusCode = "",
        message = ""
      } = await this.props.AppAction.signUp(params);

      this.context.mixPanel.track(Account_Activation,{EmailAddress:this.state.email});
      this.setState({ errorMessage: statusCode !== 200 ? message : "" });

    }
  };

  render() {
    const {
      name,
      university,
      year_group,
      email,
      password,
      job_title,
      errors,
      errorMessage
    } = this.state;

    return (
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>

          <View style={styles.content}>
            <View>
              <Header
                previousComponent={this.previousScreen}
                text="Sign Up"
                navigate={true}
              />
              <View style={{ left: moderateScale(50) }}>
                <Text
                  style={{
                    color: Constants.Colors.gunPowderShade,
                    fontSize: moderateScale(40)
                  }}
                >
                  Hi.
                </Text>

                <Text style={styles.practisingText}>
                  Let&apos;s practise for
                </Text>

                <Text style={styles.practisingText}>your osces!</Text>
              </View>
            </View>

            {/* height: "60%" */}
            <View style={{}}>
              <TextField
                onChange={this.onChangeText}
                value={name}
                stateVariable="name"
                type="text"
                placeholderValue="Name"
                errors={errors}
              />
              <TextField
                onChange={this.onChangeText}
                value={university}
                type="text"
                stateVariable="university"
                placeholderValue="Organisation"
                errors={errors}
              />
              <TextField
                onChange={this.onChangeText}
                value={year_group}
                type="text"
                stateVariable="year_group"
                placeholderValue="Year Group"
                errors={errors}
              />
              <TextField
                onChange={this.onChangeText}
                value={job_title}
                type="text"
                stateVariable="job_title"
                placeholderValue="Job Title"
                errors={errors}
              />
              <TextField
                onChange={this.onChangeText}
                value={email}
                stateVariable="email"
                type="email"
                keyboardType="email-address"
                placeholderValue="Email"
                errors={errors}
              />
              <TextField
                onChange={this.onChangeText}
                value={password}
                type="password"
                stateVariable="password"
                placeholderValue="Password"
                errors={errors}
              />

              {errorMessage ? (
                <View>
                  <Text
                    style={{
                      fontSize: moderateScale(13),
                      // fontWeight: "bold",
                      color: Constants.Colors.errorRed
                    }}
                  >
                    {errorMessage}
                  </Text>
                </View>
              ) : null}
            </View>
            <View style={{ height: moderateScale(20) }} />
            <View>
              <SequareButton
                onPress={() => this.signUp()}
                loading={false}
                style={{
                  marginBottom: moderateScale(10),
                  backgroundColor: Constants.Colors.blue
                }}
                text="GET STARTED"
              />

              <WebViewComponent />
            </View>

            <View style={{ height: moderateScale(20) }} />
          </View>
        {this.props.loader &&
            <ControlledLoader showLoader={this.props.loader}/>}
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: Constants.Colors.appBackgroundColor
  },
  content: {
    flexDirection: "column",
    justifyContent: "space-between",
    padding: moderateScale(20),
    height: "100%",
    paddingBottom: "10%"
  },
  practisingText: {
    fontSize: moderateScale(22),
    color: Constants.Colors.darkGrey1
  }
});

function mapStateToProps(state) {
  return {
    authRes: state.Auth.authData,
    error: state.Auth.error,
    loader: state.Auth.loader
  };
}

const mapDispatchToProps = dispatch => ({
  AppAction: bindActionCreators(AppAction, dispatch),
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp);
