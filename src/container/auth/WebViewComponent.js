import React from "react";
import { View, Text, Modal, TouchableOpacity, StyleSheet } from "react-native";
import { WebView } from "react-native-webview";

import Constants from "../../constants";
import { moderateScale } from "../../helpers/ResponsiveFonts";

class WebViewComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      url: "",
      openModal: false
    };
  }

  showModal = url => {
    this.setState({
      url: url,
      openModal: !this.state.openModal
    });
  };

  render() {
    return (
      <View>
        <View>
          <Text>
            <Text style={styles.text}>
              By continuing, you agree to the osce.ai{" "}
            </Text>

            <Text
              onPress={() => this.showModal("http://osce.ai/terms-conditions/")}
              style={styles.link}
            >
              Terms & Conditions
            </Text>

            <Text style={styles.text}> and </Text>

            <Text
              onPress={() => this.showModal("http://osce.ai/privacy-policy/")}
              style={styles.link}
            >
              Privacy Policy.
            </Text>
          </Text>
        </View>

        <View>
          <Modal
            animationType="slide"
            transparent={false}
            style={{ marginTop: moderateScale(20) }}
            visible={this.state.openModal}
          >
            <TouchableOpacity
              style={{
                marginVertical: moderateScale(30),
                marginRight: moderateScale(20)
              }}
              onPress={() => this.showModal()}
            >
              <Text
                style={{
                  fontSize: moderateScale(25),
                  fontWeight: "bold",
                  alignSelf: "flex-end"
                }}
              >
                X
              </Text>
            </TouchableOpacity>
            <WebView source={{ uri: this.state.url }} />
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  link: {
    color: Constants.Colors.red
  },
  text: {
    fontSize: moderateScale(14),
    color: Constants.Colors.gunPowderShade
  }
});

export default WebViewComponent;
