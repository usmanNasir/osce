import React from "react";
import { AsyncStorage } from "react-native";
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Navigation } from "react-native-navigation";
import * as AppAction from "../../actions";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from "react-native-google-signin";
import {
  AppleButton,
  AppleAuthError,
  AppleAuthRequestScope,
  AppleAuthRequestOperation,
  appleAuth,
} from "@invertase/react-native-apple-authentication";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import Constants from "../../constants";
import { SequareButton } from "../../components/common";
import WebViewComponent from "./WebViewComponent";
import AppleIcon from "../../assets/img/apple.png";
import GoogleIcon from "../../assets/img/google.png";
import FaceBookIcon from "../../assets/img/facebook.png";
import { MixPanelContext } from "../../utilities/MixPanel";
import { App_Launch, User_Login } from "../../constants/Events";
import { checkVersion } from "../../utilities/updateCheck";
import InAppUpdateModal from "../../components/common/inAppUpdateModal";
import SpInAppUpdates, {
  NeedsUpdateResponse,
  IAUUpdateKind,
  StartUpdateOptions,
} from "sp-react-native-in-app-updates";
import VersionCheck from "react-native-version-check";

class FirstScreen extends React.Component {
  static contextType = MixPanelContext;
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      errorMessage: "",
      showAppUpdate: false,
      userInfo: null,
      gettingLoginStatus: true,
    };
  }


  async  checkForVersion() {
    const result = await checkVersion();
   this.props.AppAction.getAppStatus().then(() => {
       if ( Platform.OS === 'android' && this.props.appStatus.isUpdateAvailableAndroid) {
           if (result) {
             this.setState({ showAppUpdate: true });
           }
       }else if ( Platform.OS === 'ios' && this.props.appStatus.isUpdateAvailableIOS) {

         const inAppUpdates = new SpInAppUpdates(
           false // isDebug
         );
         const currentVersion = VersionCheck.getCurrentVersion();
           inAppUpdates
             .checkNeedsUpdate({ curVersion: currentVersion })
             .then((result) => {
               if (result.shouldUpdate) {
                 this.setState({ showAppUpdate: true });
               }
             });
       }
   });
 }

  async componentDidMount() {
    this.checkForVersion().then(() => {
      setTimeout(async () => {
        this.context.mixPanel.track(App_Launch);
        AsyncStorage.setItem("triggerEvent", "false");
        const isTriggred = await AsyncStorage.getItem("triggerEvent");
      }, 5000);
    });
    // setTimeout(async()=>{
    //   this.context.mixPanel.track(App_Launch);
    //   AsyncStorage.setItem('triggerEvent','false');
    //   const isTriggred = await AsyncStorage.getItem('triggerEvent');
    // },5000)
  }

  onPress = (componentName) => {
    Navigation.push(this.props.componentId, {
      component: {
        name: componentName,
        options: {
          topBar: {
            visible: false,
          },
        },
      },
    });
  };
  loginWithFb = () => {
    this.props.AppAction.loginWithFb();
    this.context.mixPanel.track(User_Login);
  };
  onAppleButtonPress = async () => {
    // const responseLogout =  await appleAuth.performRequest({
    //   requestedOperation: appleAuth.Operation.LOGOUT
    // });

    // start a login request

    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        // requestedScopes:AppleAuthRequestScope
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });

      const {
        user: newUser,
        email,
        nonce,
        identityToken,
        realUserStatus /* etc */,
        fullName,
      } = appleAuthRequestResponse;

      this.user = newUser;
      const userObj = {
        name: fullName?.givenName + fullName?.familyName,
        email,
        identityToken,
        user: newUser,
      };

      if (identityToken) {
        // e.g. sign in with Firebase Auth using `nonce` & `identityToken`
        this.fetchAndUpdateCredentialState(this.user, userObj)
          .then((res) => {
            this.setState({ credentialStateForUser: res });
          })
          .catch((error) =>
            this.setState({ credentialStateForUser: `Error: ${error.code}` })
          );
      } else {
        Alert.alert("Failed", "Invalid Credentials try Again", [
          { text: "OK" },
        ]);
        // no token - failed sign-in?
      }
    } catch (error) {
      if (error.code === appleAuth.Error.CANCELED) {
        console.log("User canceled Apple Sign in.");
      }
    }
  };
  fetchAndUpdateCredentialState = async (usertoken, user) => {
    if (usertoken === null) {
      this.setState({ credentialStateForUser: "N/A" });
    } else {
      const credentialState = await appleAuth.getCredentialStateForUser(
        usertoken
      );
      if (credentialState === appleAuth.State.AUTHORIZED) {
        this.setState({ credentialStateForUser: "AUTHORIZED" });
        this.props.AppAction.apple_signIn(user);
      } else {
        this.setState({ credentialStateForUser: credentialState });
      }
    }
  };

  render() {
    if (this.state.showAppUpdate) {
      return <InAppUpdateModal />;
    } else {
      return (
        <SafeAreaView style={styles.container}>
          <ScrollView>
            <View style={{ padding: moderateScale(40), alignItems: "center" }}>
              <Image source={require("../../assets/img/login-logo/chip.png")} />

              <Text
                style={{
                  top: moderateScale(30),
                  alignSelf: "center",
                  fontSize: moderateScale(22),
                  color: Constants.Colors.darkGrey1,
                }}
              >
                Prepare for OSCEs with AI.
              </Text>
            </View>

            <View style={styles.buttons}>
              <SequareButton
                onPress={() => this.onPress("SignUp")}
                loading={false}
                style={{
                  marginBottom: moderateScale(10),
                  backgroundColor: Constants.Colors.blue,
                }}
                text="SIGN UP"
              />
              <SequareButton
                onPress={() => this.onPress("Login")}
                loading={false}
                style={{
                  marginBottom: moderateScale(10),
                  backgroundColor: Constants.Colors.manatee,
                }}
                text="LOG IN"
              />
              <SequareButton
                showIcon
                iconSource={GoogleIcon}
                onPress={() => {
                  this.props.AppAction._signIn();
                  this.context.mixPanel.track(User_Login);
                }}
                loading={false}
                style={{
                  marginBottom: moderateScale(10),
                  backgroundColor: Constants.Colors.manatee,
                }}
                text="CONTINUE WITH GOOGLE"
              />
              {Platform.OS === "android" ? (
                <SequareButton
                  showIcon
                  iconSource={FaceBookIcon}
                  onPress={this.loginWithFb}
                  loading={false}
                  style={{
                    marginBottom: moderateScale(10),
                    backgroundColor: Constants.Colors.manatee,
                  }}
                  text="CONTINUE WITH FACEBOOK"
                />
              ) : null}
              {Platform.OS === "ios" ? (
                <SequareButton
                  showIcon
                  iconSource={AppleIcon}
                  onPress={() => this.onAppleButtonPress()}
                  loading={false}
                  style={{
                    marginBottom: moderateScale(10),
                    backgroundColor: Constants.Colors.manatee,
                  }}
                  text="CONTINUE WITH APPLE"
                />
              ) : // <AppleButton
              //     buttonStyle={AppleButton.Style.WHITE}
              //     buttonType={AppleButton.Type.SIGN_IN}
              //     style={styles.appleButton}
              //     // style={{
              //     //   width: 160, // You must specify a width
              //     //   height: 45, // You must specify a height
              //     // }}
              //     onPress={() => this.onAppleButtonPress()}
              // />
              null}

              <WebViewComponent />
            </View>
          </ScrollView>
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    backgroundColor: Constants.Colors.appBackgroundColor,
  },
  buttons: {
    padding: moderateScale(25),
  },
});

function mapStateToProps(state) {
  return {
    appStatus: state.AppStatus.appStatus,
    authRes: state,
    error: state.Auth.error,
    loader: state.Auth.loader,
  };
}

const mapDispatchToProps = (dispatch) => ({
  AppAction: bindActionCreators(AppAction, dispatch),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FirstScreen);

// export default FirstScreen;
