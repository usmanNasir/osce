import * as RNIap  from 'react-native-iap';
import {
    consumeAllItemsAndroid,
    finishTransaction,
    finishTransactionIOS, flushFailedPurchasesCachedAsPendingAndroid,
    purchaseErrorListener,
    purchaseUpdatedListener
} from "react-native-iap";
const itemSubs = Platform.select({
    ios: [
        'com_subscription2_6months',
        'com_subscription1_monthly'// subscription
    ],
    android: [
        'com_subscription2_6months',
          'com_subscription1_monthly'// subscription
      ],
});
let purchaseUpdateSubscription;
let purchaseErrorSubscription;
export const getConnection = async () => {
    return new Promise(async (resolve, reject) => {
        try {
            const result =  await RNIap.initConnection();
            let del = await flushFailedPurchasesCachedAsPendingAndroid();
            console.log('del',del)
            console.log('result', result);
        } catch (err) {
            console.warn(err.code, err.message);
        }
        updatePurchase();
        try {
            const products = await RNIap.getSubscriptions(itemSubs);
            console.log( products );
            resolve(products)
        } catch(err) {
            console.warn(err); // standardized err.code and err.message available
        }
    })
}
export const requestSubscription = async (sku) => {
    return new Promise(async (resolve, reject) => {
        try {
            let res = await RNIap.requestSubscription(sku);
            resolve(res)
        } catch (err) {
            console.log(err.message);
        }
    })
};
export const updatePurchase = async () => {
    purchaseUpdateSubscription = purchaseUpdatedListener(
        async (purchase) => {
            const receipt = purchase.transactionReceipt;
            if (receipt) {
                try {
                    if (Platform.OS === 'ios') {
                        finishTransactionIOS(purchase.transactionId);
                    }
                    let a = await finishTransaction(purchase);
                    console.log(a,"AAAAAAAA")
                } catch (ackErr) {
                    console.log('ackErr', ackErr);
                }
            }
        },
    );
    purchaseErrorSubscription = purchaseErrorListener(
        (error) => {
            console.log('purchaseErrorListener', error);
        },
    );
    return (() => {
        if (purchaseUpdateSubscription) {
            purchaseUpdateSubscription.remove();
            purchaseUpdateSubscription = null;
        }
        if (purchaseErrorSubscription) {
            purchaseErrorSubscription.remove();
            purchaseErrorSubscription = null;
        }
    })
}
