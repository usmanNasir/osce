import { Platform } from "react-native";
import firebase from "react-native-firebase";

import type { Notification, NotificationOpen } from "react-native-firebase";

/*
Get the Fcm token of the device
*/
const getToken = async () => {
  const fcmToken = await firebase.messaging().getToken();
  if (fcmToken) {
    console.log("TOKEN", fcmToken);
  }
};

export const ApnToFcmToken = async (apnToken)=>{
try {
  const response = await fetch('https://iid.googleapis.com/iid/v1:batchImport', {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'key=AAAAyRGRcCg:APA91bE6JVBjH3eub2oLNoouGImwpF5OHXFA4OGMH71p74zrQzNTaGf1TP2XGfw-Oxzxxsnqe-qBBSZAknqGuSxd5zGoGiZD-uXj81YDbX0fB2XSGhtpJeI1bOuzTNXjuSvVp7yTOCZ6'
    },

    body: JSON.stringify({
      "application": "com.osce.ai",
      "sandbox": true,
      "apns_tokens": [`${apnToken}`]
    })
  });
  const responseBody = await response.json();
  const fcm  = responseBody.results[0]?.registration_token;
  return fcm;
}catch (e) {
    console.log("response err",e)
}
// parses JSON response into native JavaScript objects
}
/*
All Listeners related to Firebase
*/
export const listeners = () => {
  this.notificationDisplayedListener = firebase
    .notifications()
    .onNotificationDisplayed(notification => {});
  this.notificationListener = firebase
    .notifications()
    .onNotification(notification => {
      // When app is in forground  and push come immedialtely show (Without Touch)
    });
  this.notificationOpenedListener = firebase
    .notifications()
    .onNotificationOpened((notificationOpen: NotificationOpen) => {
      //when app is in background (not killed ) tapping on the push notification call that
    });
};
/*
when app is killed or not in memory push noptification come then cick on the push notification will call that function
*/
const getInitialNotification = async () => {
  const notificationOpen: NotificationOpen = await firebase
    .notifications()
    .getInitialNotification();
  if (notificationOpen) {
    //When the app is killed and tapping on the push will call this function
    console.log("getInitialNotification", notificationOpen);
  }
};
/**
 * Checking the app has permission for using firebase in ios
 */
const checkPermision = async () => {
  const enabled = await firebase.messaging().hasPermission();
  if (enabled) {
    trigerAllEvents();
  } else {
    requestpermission();
  }
};
/**
 * Requesting the app permission for firebase in ios
 */
const requestpermission = async () => {
  try {
    const enabled = await firebase.messaging().requestPermission();
    if (enabled) {
      trigerAllEvents();
    } else {
      requestpermission();
    }
  } catch (error) {
    // User has rejected permissions
  }
};

const trigerAllEvents = () => {
  getToken();
  getInitialNotification();
  listeners();
};
/*
Remove All Listeners
*/
export const removeListeners = () => {
  this.notificationDisplayedListener();
  this.notificationListener();
  this.notificationOpenedListener();
};
/**
 It loads the fcm
 */
export const pushNotifificationInit = async () => {
  if (Platform.OS === "ios") {
    checkPermision();
  } else {
    trigerAllEvents();
  }
};
