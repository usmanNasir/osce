import { Platform } from 'react-native';
import VersionCheck from 'react-native-version-check';
import { getAppStatus } from '../actions/appUpdateStatus';



export const checkVersion = async ()=>{
  let isUpdated = false;
    let currentVersion = VersionCheck.getCurrentVersion();
    let latestVersion = VersionCheck.getLatestVersion({provider:Platform.OS === 'ios' ? 'appStore':'playStore'})
    if( currentVersion < latestVersion){
      isUpdated = true;
    } 
  return isUpdated;
}

export const isAppLive = async ()=>{
  const isLive = await getAppStatus();
  return isLive;
}