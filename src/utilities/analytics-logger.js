import RNAmplitute from "react-native-amplitude-analytics";

class Logger {
  constructor() {
    this.logger = new RNAmplitute("cf9b1790e00269f28d3fab01955465ab", true);
  }
  logEvent = (event, data) => {
    this.logger.logEvent(event, data);
  };

  logEventWithData = (event, dataObject) => {
    this.logger.logEvent(event, dataObject);
  };

  logEventWithTimestamp = () => {};

  setUserId = id => {
    this.logger.setUserId(id);
  };
  setUserProperties = propetyObj => {
    this.logger.setUserProperties(propetyObj);
  };
  logRevenue = (productIdentifier, quantity, amount) => {
    this.logger.logRevenue(productIdentifier, quantity, amount);
  };
}

const logger = new Logger();

export { logger };

///How to use

// import { logger } from "analytics-logger";

// logger.logEvent("APP_LAUNCH");
