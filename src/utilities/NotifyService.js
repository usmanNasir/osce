// import PushNotification from "react-native-push-notification";
// import NotificationHandler from "./NotificationHandler";
// import PushNotificationIOS from "@react-native-community/push-notification-ios";
// import moment from "moment";
//
// export default class NotifService {
//
// constructor(onRegister, onNotification) {
//   this.lastId = 0;
//   this.lastChannelCounter = 0;
//
//
//   NotificationHandler.attachRegister(onRegister);
//   NotificationHandler.attachNotification(onNotification);
//
//   // Clear badge number at start
//   PushNotification.getApplicationIconBadgeNumber(function (number) {
//     if (number > 0) {
//       PushNotification.setApplicationIconBadgeNumber(0);
//     }
//   });
//
//   PushNotification.getChannels(function(channels) {
//     console.log(channels);
//   });
// }
//
//
//   createOrUpdateChannel() {
//     this.lastChannelCounter++;
//     PushNotification.createChannel(
//         {
//           channelId: "custom-channel-id", // (required)
//           channelName: `Custom channel - Counter: ${this.lastChannelCounter}`, // (required)
//           channelDescription: `A custom channel to categorise your custom notifications. Updated at: ${Date.now()}`, // (optional) default: undefined.
//           soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
//           // importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
//           vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
//         },
//         (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
//     );
//   }
//
//   popInitialNotification() {
//     PushNotification.popInitialNotification((notification) => console.log('InitialNotication:', notification));
//   }
//
//
//
//   localNotif(soundName) {
//     this.lastId++;
//     PushNotification.localNotification({
//       /* Android Only Properties */
//       channelId: soundName ? 'sound-channel-id' : 'default-channel-id',
//       ticker: 'My Notification Ticker', // (optional)
//       autoCancel: true, // (optional) default: true
//       largeIcon: 'ic_launcher', // (optional) default: "ic_launcher"
//       smallIcon: 'ic_notification', // (optional) default: "ic_notification" with fallback for "ic_launcher"
//       bigText: 'My big text that will be shown when notification is expanded', // (optional) default: "message" prop
//       subText: 'This is a subText', // (optional) default: none
//       color: 'red', // (optional) default: system default
//       vibrate: true, // (optional) default: true
//       vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
//       tag: 'some_tag', // (optional) add tag to message
//       group: 'group', // (optional) add group to message
//       groupSummary: false, // (optional) set this notification to be the group summary for a group of notifications, default: false
//       ongoing: false, // (optional) set whether this is an "ongoing" notification
//       actions: ['Yes', 'No'], // (Android only) See the doc for notification actions to know more
//       invokeApp: true, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true
//
//       when: null, // (optionnal) Add a timestamp pertaining to the notification (usually the time the event occurred). For apps targeting Build.VERSION_CODES.N and above, this time is not shown anymore by default and must be opted into by using `showWhen`, default: null.
//       usesChronometer: false, // (optional) Show the `when` field as a stopwatch. Instead of presenting `when` as a timestamp, the notification will show an automatically updating display of the minutes and seconds since when. Useful when showing an elapsed time (like an ongoing phone call), default: false.
//       timeoutAfter: null, // (optional) Specifies a duration in milliseconds after which this notification should be canceled, if it is not already canceled, default: null
//
//       /* iOS only properties */
//       category: '', // (optional) default: empty string
//       subtitle: "My Notification Subtitle", // (optional) smaller title below notification title
//
//       /* iOS and Android properties */
//       id: this.lastId, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
//       title: 'Local Notification', // (optional)
//       message: 'My Notification Message', // (required)
//       userInfo: { screen: 'home' }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       playSound: !!soundName, // (optional) default: true
//       soundName: soundName ? soundName : 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
//       number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
//     });
//   }
//
//
//   scheduleLocalNotification() {
//     console.log(
//       "Moment",
//       moment()
//         .add(10, "days")
//         .format("YYYY-MM-DDTHH:mm:ss.sssZ")
//     );
//     PushNotificationIOS.scheduleLocalNotification({
//       alertTitle: "Continue learning",
//       alertBody:
//         "Comeback and complete the course to further enhance your knowledge",
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message:
//           "Comeback and complete the course to further enhance your knowledge"
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       repeatInterval: "week", // 30 seconds
//       fireDate: moment()
//         .add(10, "days")
//         .format("YYYY-MM-DDTHH:mm:ss.sssZ")
//     });
//   }
// scheduleLocalNotification4 () {
//     PushNotificationIOS.scheduleLocalNotification({
//       alertTitle: "Unfinished Tests",
//       alertBody: "You have some unfinished tests. Comeback and finish them to enhance your knowledge.",
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message: "You have some unfinished tests. Comeback and finish them to enhance your knowledge.",
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       repeatInterval: "week", // 30 seconds
//       fireDate: moment().add(4, 'days').format("YYYY-MM-DDTHH:mm:ss.sssZ"),
//     });
//   };
//     scheduleLocalNotification7 () {
//     PushNotificationIOS.scheduleLocalNotification({
//       alertTitle: "Unfinished Tests",
//       alertBody: "You have some unfinished tests. Comeback and finish them to enhance your knowledge.",
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message: "You have some unfinished tests. Comeback and finish them to enhance your knowledge.",
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       repeatInterval: "week", // 30 seconds
//       fireDate: moment().add(7, 'days').format("YYYY-MM-DDTHH:mm:ss.sssZ"),
//     });
//   };
//   scheduleLocalNotification15 () {
//     PushNotificationIOS.scheduleLocalNotification({
//       alertTitle: "Unfinished Tests",
//       alertBody: "You have some unfinished tests. Comeback and finish them to enhance your knowledge.",
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message: "You have some unfinished tests. Comeback and finish them to enhance your knowledge.",
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       repeatInterval: "week", // 30 seconds
//       fireDate: moment().add(15, 'days').format("YYYY-MM-DDTHH:mm:ss.sssZ"),
//     });
//   };
//
//   scheduleLocalNotification35 () {
//     PushNotificationIOS.scheduleLocalNotification({
//       alertTitle: "Unfinished Tests",
//       alertBody: "You have some unfinished tests. Comeback and finish them to enhance your knowledge.",
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message: "You have some unfinished tests. Comeback and finish them to enhance your knowledge.",
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       repeatInterval: "day", // 30 seconds
//       fireDate: moment().add(35, 'days').format("YYYY-MM-DDTHH:mm:ss.sssZ"),
//     });
//   };
//   scheduleLocalNotification60 () {
//     PushNotificationIOS.scheduleLocalNotification({
//       alertTitle: "Unfinished Tests",
//       alertBody: "You have some unfinished tests. Comeback and finish them to enhance your knowledge.",
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message: "You have some unfinished tests. Comeback and finish them to enhance your knowledge.",
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       repeatInterval: "week", // 30 seconds
//       fireDate: moment().add(60, 'days').format("YYYY-MM-DDTHH:mm:ss.sssZ"),
//     });
//   };
//   scheduleLocalNotification7Weekly () {
//     PushNotificationIOS.scheduleLocalNotification({
//       alertTitle: "Comeback",
//       alertBody: "Comeback and treat your patients. They are waiting.",
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message: "Comeback and treat your patients. It's been a while. They are waiting.",
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       repeatInterval: "week", // 30 seconds
//       fireDate: moment().add(7, 'days').format("YYYY-MM-DDTHH:mm:ss.sssZ"),
//     });
//   };
//
//   scheduleLocalNotification14 () {
//     PushNotificationIOS.scheduleLocalNotification({
//       alertTitle: "Comeback",
//       alertBody: "Comeback and treat your patients. They are waiting.",
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message: "Comeback and treat your patients. It's been a while. They are waiting.",
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       repeatInterval: "week", // 30 seconds
//       fireDate: moment().add(14, 'days').format("YYYY-MM-DDTHH:mm:ss.sssZ"),
//     });
//   };
//
//   scheduleLocalNotification21 () {
//     PushNotificationIOS.scheduleLocalNotification({
//       alertTitle: "Comeback",
//       alertBody: "Comeback and treat your patients. They are waiting.",
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message: "Comeback and treat your patients. It's been a while. They are waiting.",
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       repeatInterval: "week", // 30 seconds
//       fireDate: moment().add(21, 'days').format("YYYY-MM-DDTHH:mm:ss.sssZ"),
//     });
//   };
//
//   scheduleLocalNotification35Weekly () {
//     PushNotificationIOS.scheduleLocalNotification({
//       alertTitle: "Comeback",
//       alertBody: "Comeback and treat your patients. They are waiting.",
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message: "Comeback and treat your patients. It's been a while. They are waiting.",
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       repeatInterval: "week", // 30 seconds
//       fireDate: moment().add(35, 'days').format("YYYY-MM-DDTHH:mm:ss.sssZ"),
//     });
//   };
//
//   scheduleLocalNotification42 () {
//     PushNotificationIOS.scheduleLocalNotification({
//       alertTitle: "Comeback",
//       alertBody: "Comeback and treat your patients. They are waiting.",
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message: "Comeback and treat your patients. It's been a while. They are waiting.",
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       repeatInterval: "week", // 30 seconds
//       fireDate: moment().add(42, 'days').format("YYYY-MM-DDTHH:mm:ss.sssZ"),
//     });
//   };
//
//
//   scheduleNotif(soundName) {
//     this.lastId++;
//     var myDate = new Date();
//     myDate.setDate(myDate.getDate() + parseInt(2));
//     console.log(myDate);
//     PushNotification.localNotificationSchedule({
//       /* Android Only Properties */
//       id: this.lastId, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
//       ticker: "My Notification Ticker", // (optional)
//       autoCancel: true, // (optional) default: true
//       largeIcon: "ic_launcher", // (optional) default: "ic_launcher"
//       smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher"
//       bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
//       subText: "This is a subText", // (optional) default: none
//       color: "blue", // (optional) default: system default
//       vibrate: true, // (optional) default: true
//       vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
//       tag: "some_tag", // (optional) add tag to message
//       group: "group", // (optional) add group to message
//       ongoing: false, // (optional) set whether this is an "ongoing" notification
//
//       /* iOS only properties */
//       alertAction: "view", // (optional) default: view
//       category: "", // (optional) default: empty string
//       userInfo: {
//         message: "My Notification Message",
//       }, // (optional) default: {} (using null throws a JSON value '<null>' error)
//       // fireDate:new Date(Date.now() + 1 * 1000),
//       repeatInterval: "week",
//       date: new Date(Date.now() + 2 * 24 * 60 * 60 * 1000),
//       // fireDate: Date.now(),
//
//       /* iOS and Android properties */
//       title: "Scheduled Notification", // (optional)
//       message: "My Notification Message", // (required)
//       playSound: !!soundName, // (optional) default: true
//       number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
//       soundName: soundName ? soundName : "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
//     });
//   }
//
//   checkPermission(cbk) {
//     return PushNotification.checkPermissions(cbk);
//   }
//
//   requestPermissions() {
//     return PushNotification.requestPermissions();
//   }
//
//   cancelNotif() {
//     PushNotification.cancelLocalNotifications({ id: "" + this.lastId });
//   }
//
//   cancelAll() {
//     PushNotification.cancelAllLocalNotifications();
//   }
//
//   abandonPermissions() {
//     PushNotification.abandonPermissions();
//   }
//
//   getScheduledLocalNotifications(callback) {
//     PushNotification.getScheduledLocalNotifications(callback);
//   }
//
//   getDeliveredNotifications(callback) {
//     PushNotification.getDeliveredNotifications(callback);
//   }
//  }



import PushNotification, {Importance} from 'react-native-push-notification';
import NotificationHandler from './NotificationHandler';

export default class NotifService {
  constructor(onRegister, onNotification) {
    this.lastId = 0;
    this.lastChannelCounter = 0;

    NotificationHandler.attachRegister(onRegister);
    NotificationHandler.attachNotification(onNotification);

    // Clear badge number at start
    PushNotification.getApplicationIconBadgeNumber(function (number) {
      if (number > 0) {
        PushNotification.setApplicationIconBadgeNumber(0);
      }
    });

    PushNotification.getChannels(function(channels) {
      console.log(channels);
    });
  }

  createDefaultChannels() {
    PushNotification.createChannel(
        {
          channelId: "default-channel-id", // (required)
          channelName: `Default channel`, // (required)
          channelDescription: "A default channel", // (optional) default: undefined.
          soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
           // (optional) default: Importance.HIGH. Int value of the Android notification importance
          vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
        },
        (created) => console.log(`createChannel 'default-channel-id' returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );
    PushNotification.createChannel(
        {
          channelId: "sound-channel-id", // (required)
          channelName: `Sound channel`, // (required)
          channelDescription: "A sound channel", // (optional) default: undefined.
          soundName: "sample.mp3", // (optional) See `soundName` parameter of `localNotification` function// (optional) default: Importance.HIGH. Int value of the Android notification importance
          vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
        },
        (created) => console.log(`createChannel 'sound-channel-id' returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );
  }

  createOrUpdateChannel() {
    this.lastChannelCounter++;
    PushNotification.createChannel(
        {
          channelId: "custom-channel-id", // (required)
          channelName: `Custom channel - Counter: ${this.lastChannelCounter}`, // (required)
          channelDescription: `A custom channel to categorise your custom notifications. Updated at: ${Date.now()}`, // (optional) default: undefined.
          soundName: "default", // (optional) See `soundName` parameter of `localNotification` function// (optional) default: Importance.HIGH. Int value of the Android notification importance
          vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
        },
        (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );
  }

  popInitialNotification() {
    PushNotification.popInitialNotification((notification) => console.log('InitialNotication:', notification));
  }

  localNotif(soundName) {
    this.lastId++;
    PushNotification.localNotification({
      /* Android Only Properties */
      channelId: soundName ? 'sound-channel-id' : 'default-channel-id',
      ticker: 'My Notification Ticker', // (optional)
      autoCancel: true, // (optional) default: true
      largeIcon: 'ic_launcher', // (optional) default: "ic_launcher"
      smallIcon: 'ic_notification', // (optional) default: "ic_notification" with fallback for "ic_launcher"
      bigText: 'My big text that will be shown when notification is expanded', // (optional) default: "message" prop
      subText: 'This is a subText', // (optional) default: none
      color: 'red', // (optional) default: system default
      vibrate: true, // (optional) default: true
      vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
      tag: 'some_tag', // (optional) add tag to message
      group: 'group', // (optional) add group to message
      groupSummary: false, // (optional) set this notification to be the group summary for a group of notifications, default: false
      ongoing: false, // (optional) set whether this is an "ongoing" notification
      actions: ['Yes', 'No'], // (Android only) See the doc for notification actions to know more
      invokeApp: true, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true

      when: null, // (optionnal) Add a timestamp pertaining to the notification (usually the time the event occurred). For apps targeting Build.VERSION_CODES.N and above, this time is not shown anymore by default and must be opted into by using `showWhen`, default: null.
      usesChronometer: false, // (optional) Show the `when` field as a stopwatch. Instead of presenting `when` as a timestamp, the notification will show an automatically updating display of the minutes and seconds since when. Useful when showing an elapsed time (like an ongoing phone call), default: false.
      timeoutAfter: null, // (optional) Specifies a duration in milliseconds after which this notification should be canceled, if it is not already canceled, default: null

      /* iOS only properties */
      category: '', // (optional) default: empty string
      subtitle: "My Notification Subtitle", // (optional) smaller title below notification title

      /* iOS and Android properties */
      id: this.lastId, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
      title: 'Local Notification', // (optional)
      message: 'My Notification Message', // (required)
      userInfo: { screen: 'home' }, // (optional) default: {} (using null throws a JSON value '<null>' error)
      playSound: !!soundName, // (optional) default: true
      soundName: soundName ? soundName : 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
      number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
    });
  }

  scheduleNotif(soundName) {
    this.lastId++;
    PushNotification.localNotificationSchedule({
      date: new Date(Date.now() + 30 * 1000), // in 30 secs

      /* Android Only Properties */
      channelId: soundName ? 'sound-channel-id' : 'default-channel-id',
      ticker: 'My Notification Ticker', // (optional)
      autoCancel: true, // (optional) default: true
      largeIcon: 'ic_launcher', // (optional) default: "ic_launcher"
      smallIcon: 'ic_notification', // (optional) default: "ic_notification" with fallback for "ic_launcher"
      bigText: 'My big text that will be shown when notification is expanded', // (optional) default: "message" prop
      subText: 'This is a subText', // (optional) default: none
      color: 'blue', // (optional) default: system default
      vibrate: true, // (optional) default: true
      vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
      tag: 'some_tag', // (optional) add tag to message
      group: 'group', // (optional) add group to message
      groupSummary: false, // (optional) set this notification to be the group summary for a group of notifications, default: false
      ongoing: false, // (optional) set whether this is an "ongoing" notification
      actions: ['Yes', 'No'], // (Android only) See the doc for notification actions to know more
      invokeApp: false, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true

      when: null, // (optionnal) Add a timestamp pertaining to the notification (usually the time the event occurred). For apps targeting Build.VERSION_CODES.N and above, this time is not shown anymore by default and must be opted into by using `showWhen`, default: null.
      usesChronometer: false, // (optional) Show the `when` field as a stopwatch. Instead of presenting `when` as a timestamp, the notification will show an automatically updating display of the minutes and seconds since when. Useful when showing an elapsed time (like an ongoing phone call), default: false.
      timeoutAfter: null, // (optional) Specifies a duration in milliseconds after which this notification should be canceled, if it is not already canceled, default: null

      /* iOS only properties */
      category: '', // (optional) default: empty string

      /* iOS and Android properties */
      id: this.lastId, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
      title: 'Scheduled Notification', // (optional)
      message: 'My Notification Message', // (required)
      userInfo: { sceen: "home" }, // (optional) default: {} (using null throws a JSON value '<null>' error)
      playSound: !!soundName, // (optional) default: true
      soundName: soundName ? soundName : 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
      number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
    });
  }

  checkPermission(cbk) {
    return PushNotification.checkPermissions(cbk);
  }

  requestPermissions() {
    return PushNotification.requestPermissions();
  }

  cancelNotif() {
    PushNotification.cancelLocalNotifications({id: '' + this.lastId});
  }

  cancelAll() {
    PushNotification.cancelAllLocalNotifications();
  }

  abandonPermissions() {
    PushNotification.abandonPermissions();
  }

  getScheduledLocalNotifications(callback) {
    PushNotification.getScheduledLocalNotifications(callback);
  }

  getDeliveredNotifications(callback) {
    PushNotification.getDeliveredNotifications(callback);
  }
}
