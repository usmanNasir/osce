import React, { useEffect, useState } from "react";
import { Mixpanel } from "mixpanel-react-native";

export const MixPanelContext = React.createContext();

const MixPanelProvider = (props) => {
    
  const [mixPanelValue, setMixPanel] = useState();

  useEffect(() => {
    const initMixPanel = async () => {
      const mixpanel = await Mixpanel.init('475e25103ef731274d35f1272f68c789')
      setMixPanel(mixpanel);
    };
    initMixPanel();
  }, []);

  return (
    <MixPanelContext.Provider value={{mixPanel:mixPanelValue}}>
      {props.children}
    </MixPanelContext.Provider>
  );
};

export default MixPanelProvider;
