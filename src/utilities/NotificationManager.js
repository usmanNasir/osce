import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from "react-native-push-notification";
import { Platform } from "react-native";

class NotificationManager {
  configure = () => {
    // PushNotification.configure({
    //   // (optional) Called when Token is generated (iOS and Android)
    //   onRegister: function(token) {
    //     console.log("TOKEN:", token);
    //   },
    //
    //   // (required) Called when a remote is received or opened, or local notification is opened
    //   onNotification: function(notification) {
    //     console.log("NOTIFICATION:", notification);
    //     // process the notification
    //
    //     // (required) Called when a remote is received or opened, or local notification is opened
    //     notification.finish(PushNotificationIOS.FetchResult.NoData);
    //   },
    //   onRegistrationError: function(err) {
    //     console.log("ERROR CHCKNG")
    //     console.log(err.message, err);
    //   },
    // });
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.log("TOKEN:", token);
      },

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);

        // process the notification

        // (required) Called when a remote is received or opened, or local notification is opened
        if(Platform.OS ==='ios'){
          notification.finish(PushNotificationIOS.FetchResult.NoData);
        }

      },

      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: function (notification) {
        console.log("ACTION:", notification.action);
        console.log("NOTIFICATION:", notification);

        // process the action
      },

      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: function(err) {
        console.error(err.message, err);
      },

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });
  };
  _buildIOSNotification = (id, data = {}, message, title, options = {}) => {
    return {
      alertAction: options.alertAction || "view", // (optional) default: view
      category: options.alertAction || "", // (optional) default: empty string
      userInfo: {
        id: id,
        item: data
      }, // (optional) default: {} (using null throws a JSON value '<null>' error)

      /* iOS and Android properties */
      title: "My Notification Title", // (optional)
      message: "My Notification Message", // (required)
      playSound: false, // (optional) default: true
      soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
      number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
      repeatType: "day" // (optional) Repeating interval. Check 'Repeating Notifications' section for more info.
    };
  };

  showNotification = (id, data = {}, message, title, options = {}) => {
    PushNotification.localNotification({
      ...this._buildIOSNotification(id, data, message, title, options),
      title: title || "",
      message: message || "",
      playSound: options.playSound || false,
      soundName: options.soundName || "default",
      userInteraction: false
    });
  };

  sendLocalNotification = () => {
    PushNotificationIOS.presentLocalNotification({
      alertTitle: "Sample Title",
      alertBody: "Sample local notification",
      applicationIconBadgeNumber: 1,
      fireDate: new Date().toISOString()
    });
  };

  scheduleLocalNotification = () => {
    PushNotificationIOS.scheduleLocalNotification({
      alertBody: "Test Local Notification",
      message: "Hello",
      fireDate: new Date("July 14, 2020 01:55:00")
    });
  };

  cancelAllNotifications = () => {
    PushNotificationIOS.removeAllDeliveredNotifications();
  };
  unregister = () => {
    PushNotification.unregister;
  };
}

export const notificationManager = new NotificationManager();
