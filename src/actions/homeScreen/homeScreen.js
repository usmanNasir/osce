import RestClient from "../../helpers/RestClient";
import {
  GET_LAST_COMPLAINTS_FULLFILLED,
  GET_LAST_COMPLAINTS_REJECTED,
  GET_LAST_COMPLAINTS_PENDING,
  GET_NEWS_CASE_PENDING,
  GET_NEWS_CASE_REJECTED,
  GET_NEWS_CASE_FULLFILLED,
  GET_CHALLENGE_PENDING,
  GET_CHALLENGE_REJECTED,
  GET_CHALLENGE_FULLFILLED,
  GET_HEADING_CONSTANTS_PENDING,
  GET_HEADING_CONSTANTS_REJECTED,
  GET_HEADING_CONSTANTS_FULLFILLED,
  GET_HOME_SCREEN_DATA_PENDING,
  GET_HOME_SCREEN_DATA_REJECTED,
  GET_HOME_SCREEN_DATA_FULLFILLED,
  CREATE_LAST_COMPLAINT_PENDING,
  CREATE_LAST_COMPLAINT_REJECTED,
  CREATE_LAST_COMPLAINT_FULLFILLED,
  //  CHANGE CHALLENGE FOR USER
  CREATE_SPECIFIC_CHALLENGE_PENDING,
  CREATE_SPECIFIC_CHALLENGE_REJECTED,
  CREATE_SPECIFIC_CHALLENGE_FULLFILLED,
} from "../../actionTypes";

import _ from "underscore";

export const getLastComplaints = (userId = "") => async (
  dispatch,
  getstate
) => {
  try {
    dispatch({ type: GET_LAST_COMPLAINTS_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}lastComplaint/get/${userId}`
    );
    const { data = {} } = response;
    const { lastComplaints = [] } = data;
    if (response && response.statusCode === 200) {
      dispatch({
        type: GET_LAST_COMPLAINTS_FULLFILLED,
        payload: { lastComplaints },
      });
    } else dispatch({ type: GET_LAST_COMPLAINTS_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: GET_LAST_COMPLAINTS_REJECTED, payload: {} });
  }
};
export const postChallengeforUser = (userId) => async (dispatch, getstate) => {
  try {
    dispatch({ type: CREATE_SPECIFIC_CHALLENGE_PENDING, payload: true });
    const response = await RestClient.post(
      `${getstate().AppConfig.serverUrl}challenge/postSpecifichallenge/`,
      { userId: userId }
    );
    if (response && response.statusCode === 200) {
      dispatch({
        type: CREATE_SPECIFIC_CHALLENGE_FULLFILLED,
        payload: {},
      });
    } else dispatch({ type: CREATE_SPECIFIC_CHALLENGE_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: CREATE_SPECIFIC_CHALLENGE_REJECTED, payload: {} });
  }
};

export const getHomeScreenData = (userId = "") => async (
  dispatch,
  getstate
) => {
  try {
    dispatch({ type: GET_HOME_SCREEN_DATA_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}homeScreen/get/${userId}`
    );
    const { data = {} } = response;
    const { headingConstants = {}, HomeContent = [] } = data;
    if (response && response.statusCode === 200) {
      dispatch({
        type: GET_HOME_SCREEN_DATA_FULLFILLED,
        payload: { headingConstants, HomeContent },
      });
    } else dispatch({ type: GET_HOME_SCREEN_DATA_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: GET_HOME_SCREEN_DATA_REJECTED, payload: {} });
  }
};

export const createLastComplaint = ({
  userId = "",
  complaintId = "",
  caseCompleted = "",
  progressBar = "",
  complaintTitle = "",
  complaint = {},
}) => async (dispatch, getstate) => {
  try {
    dispatch({ type: CREATE_LAST_COMPLAINT_PENDING, payload: true });
    const bodyObject = {
      userId,
      complaintId,
      caseCompleted,
      progressBar,
      complaintTitle,
      complaint,
    };
    const response = await RestClient.post(
      `${getstate().AppConfig.serverUrl}lastComplaint/create`,
      bodyObject
    );
    const { data = {} } = response;
    const { lastComplaints = [] } = data;
    if (response && response.statusCode === 200) {
      dispatch({
        type: CREATE_LAST_COMPLAINT_FULLFILLED,
        payload: { lastComplaints },
      });
    } else dispatch({ type: CREATE_LAST_COMPLAINT_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: CREATE_LAST_COMPLAINT_REJECTED, payload: {} });
  }
};

export const getNewsBlog = () => async (dispatch, getstate) => {
  try {
    dispatch({ type: GET_NEWS_CASE_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}newsBlog/get/`
    );
    const { data = {} } = response;
    const newsBlog = data;
    if (response && response.statusCode === 200) {
      dispatch({
        type: GET_NEWS_CASE_FULLFILLED,
        payload: { data },
      });
    } else dispatch({ type: GET_NEWS_CASE_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: GET_NEWS_CASE_REJECTED, payload: {} });
  }
};

export const getChallenge = (userId = "") => async (dispatch, getstate) => {
  const serverUrl = `https://688643444fca.ngrok.io/challenge/get/${userId}`;
  try {
    dispatch({ type: GET_CHALLENGE_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}challenge/get/${userId}`
    );
    const { data = {} } = response;
    // const { challenge = [] } = data;

    if (response && response.statusCode === 200) {
      dispatch({
        type: GET_CHALLENGE_FULLFILLED,
        payload: { ...data },
      });
    } else {
      dispatch({ type: GET_CHALLENGE_REJECTED, payload: {} });
    }
  } catch (error) {
    dispatch({ type: GET_CHALLENGE_REJECTED, payload: {} });
  }
};

export const getHeadingConstants = (token) => async (dispatch, getstate) => {
  try {
    dispatch({ type: GET_HEADING_CONSTANTS_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}headingConstants/get/`,
      token
    );
    const { data = {} } = response;
    const responseObject = data[0];
    if (response && response.statusCode === 200) {
      dispatch({
        type: GET_HEADING_CONSTANTS_FULLFILLED,
        payload: { ...responseObject },
      });
    } else dispatch({ type: GET_HEADING_CONSTANTS_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: GET_HEADING_CONSTANTS_REJECTED, payload: {} });
  }
};
