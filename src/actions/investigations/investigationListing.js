import RestClient from "../../helpers/RestClient";
import {
    GET_INVESTIGATION_LISTING,
    INVESTIGATION_LOADER_VISIBILITY,
    TOTAL_PAGE_NO_INVESTIGATION,
    UPDATE_INVESTIGATION_LISTING,
    PAGINATION_LOADER_INVESTIGATION,
    GET_COMPLAINTS_REJECTED,
    GET_COMPLAINTS_FULLFILLED,
    GET_COMPLAINTS_PENDING,
    ADD_GRADING,
    GET_GRADING_REJECTED,
    GET_GRADING_FULLFILLED,
    GET_GRADING_PENDING,
    ADD_GRADING_HOME,
    GET_INVESTIGATIONS_PENDING,
    GET_INVESTIGATIONS_FULLFILLED,
    GET_INVESTIGATIONS_REJECTED
} from "../../actionTypes";
import { showMessage } from "react-native-flash-message";

export const InvestigationListing = (page = 1, isPagination = false) => {
  return (dispatch, getstate) => {
    //show history loader
    isPagination
      ? dispatch({ type: PAGINATION_LOADER_INVESTIGATION, payload: true })
      : dispatch({ type: INVESTIGATION_LOADER_VISIBILITY, payload: true });
    RestClient.getCall(`${getstate().AppConfig.serverUrl}complaints/${page}`)
      .then(response => {
        //hide history loader
        isPagination
          ? dispatch({ type: PAGINATION_LOADER_INVESTIGATION, payload: false })
          : dispatch({ type: INVESTIGATION_LOADER_VISIBILITY, payload: false });
        if (response.statusCode == 200) {
          //update total page nos
          dispatch({ type: TOTAL_PAGE_NO_INVESTIGATION, payload: response.data.totalPages });
          //set update history data
          isPagination
            ? dispatch({
                type: UPDATE_INVESTIGATION_LISTING,
                payload: response.data.histories
              })
            : dispatch({
                type: GET_INVESTIGATION_LISTING,
                payload: response.data.histories
              });
        } else {
          //show error message
          showMessage({
            message: response.message,
            type: "warning",
            icon: "warning"
          });
        }
      })
      .catch(error => {
        //hide history loader
        isPagination
          ? dispatch({ type: PAGINATION_LOADER_INVESTIGATION, payload: false })
          : dispatch({ type: INVESTIGATION_LOADER_VISIBILITY, payload: false });
      });
  };
};

export const getInvestigation = ({ pageNo = 1, _id = "" }) => async (
  dispatch,
  getstate
) => {
  try {
    dispatch({ type: GET_INVESTIGATIONS_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}investigation/get/${pageNo}/${_id}`
    );
    const { data = {} } = response;
    const { investigations = [] } = data;
    if (response && response.statusCode === 200) {
      dispatch({ type: GET_INVESTIGATIONS_FULLFILLED, payload: { investigations } });
    } else dispatch({ type: GET_INVESTIGATIONS_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: GET_INVESTIGATIONS_REJECTED, payload: {} });
  }
};

// export const getAllHistory = () => async (dispatch, getstate) => {
//   try {
//     const response = await RestClient.getCall(
//       `${getstate().AppConfig.serverUrl}history/get`
//     );

//     if (response && response.statusCode === 200) {
//       dispatch({
//         type: ADD_GRADING_HOME,
//         payload: response.data
//       });
//     } else dispatch({ type: GET_GRADING_REJECTED, payload: {} });
//   } catch (error) {
//     dispatch({ type: GET_GRADING_REJECTED, payload: {} });
//   }
//};
