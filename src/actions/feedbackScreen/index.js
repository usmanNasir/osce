import RestClient from "../../helpers/RestClient";
import {
CREATE_FEEDBACK_PENDING,
CREATE_FEEDBACK_FULLFILLED,
CREATE_FEEDBACK_REJECTED
  
} from "../../actionTypes";

import _ from "underscore";


export const createFeedBack = ({
  receipt = "",
  sender = "",
  subject = "",
  feedback = ""
}) => async (dispatch, getstate) => {
  try {
    dispatch({ type: CREATE_FEEDBACK_PENDING, payload: true });
    const bodyObject = {
    receipt,
    sender,
    subject,
    feedback
    };
    const response = await RestClient.post(
      `${getstate().AppConfig.serverUrl}feedback/sendMail`,
      bodyObject
    );
    if (response && response.statusCode === 200) {
      dispatch({
        type: CREATE_FEEDBACK_FULLFILLED,
        payload: {  },
      });
    } else dispatch({ type: CREATE_FEEDBACK_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: CREATE_FEEDBACK_REJECTED, payload: {} });
  }
};

