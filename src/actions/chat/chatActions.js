import RestClient from "../../helpers/RestClient";
import {
  APPEND_MESAGE,
  RESET_CHAT_BOT_DATA,
  SESSION_ID,
  CHAT_RESPONSE_LOADER,
    APPEND_MESSAGE_EXAMINATION,
    APPEND_MESSAGE_INVESTIGATION,
    RESET_CHAT_BOT_DATA_EXAMINATION,
    RESET_CHAT_BOT_DATA_INVESTIGATION,
    CHAT_RESPONSE_LOADER_EXAMINATION,
    CHAT_RESPONSE_LOADER_INVESTIGATION,
    RESET_MESSAGE_LIST_EXAMINATION,
    RESET_MESSAGE_LIST_INVESTIGATION
} from "../../actionTypes";

import _ from "underscore";


export const sendMessageExam = (query = {}) => {

  return (dispatch, getstate) => {
    dispatch({ type: CHAT_RESPONSE_LOADER_EXAMINATION, payload: true });
    RestClient.restCall(`${getstate().AppConfig.serverUrl}chat`, query)
        .then(response => {
          dispatch({ type: CHAT_RESPONSE_LOADER_EXAMINATION, payload: false });
          if (response.statusCode === 200) {
            let { sessionId, botResponse } = response.data;
            let botMessage;let type;
            if(response.data?.payload && response.data?.payload?.url !== '' ){
              type =response.data?.payload?.type === 'image' ? 'image':'audio'
              botMessage = [
                {
                  _id: _.uniqueId("OSCE_"),
                  [type]: response.data?.payload.url,
                  createdAt: new Date(),
                  user: {
                    _id: "bot",
                    name: "bot"
                  }
                }
              ];
            }else {
              botMessage = [
                {
                  _id: _.uniqueId("OSCE_"),
                  text: botResponse,
                  createdAt: new Date(),
                  user: {
                    _id: "bot",
                    name: "bot"
                  }
                }
              ];
            }
            dispatch({ type: APPEND_MESSAGE_EXAMINATION, payload: botMessage });
            if(getstate().ChatBot.sessionId === null){
              dispatch({ type: SESSION_ID, payload: sessionId })
            }
          } else {
            alert(response.message);
            dispatch({ type: APPEND_MESSAGE_EXAMINATION, payload: [] });
          }
        })
        .catch(error => {
          dispatch({ type: CHAT_RESPONSE_LOADER_EXAMINATION, payload: false });
        });
  };
};

export const resetChatBotDataInvestigation = () => {
  return dispatch => {
    dispatch({ type: RESET_CHAT_BOT_DATA_INVESTIGATION });
  };
};

export const resetChatBotListingInvestigation = () => {
  return dispatch => {
    dispatch({ type:RESET_MESSAGE_LIST_INVESTIGATION  });
  };
};


export const sendMessageInvestigation = (query = {}) => {
  return (dispatch, getstate) => {

    dispatch({ type: CHAT_RESPONSE_LOADER_INVESTIGATION, payload: true });
    RestClient.restCall(`${getstate().AppConfig.serverUrl}chat`, query)
        .then(response => {
          dispatch({ type: CHAT_RESPONSE_LOADER_INVESTIGATION, payload: false });
          if (response.statusCode === 200) {
            let { sessionId, botResponse } = response.data;
            let botMessage;let type;
            if(response.data?.payload && response.data?.payload?.url !== '' ){
              type =response.data?.payload?.type === 'image' ? 'image':'audio'
              botMessage = [
                {
                  _id: _.uniqueId("OSCE_"),
                  [type]: response.data?.payload.url,
                  createdAt: new Date(),
                  user: {
                    _id: "bot",
                    name: "bot"
                  }
                }
              ];
            }else {
              botMessage = [
                {
                  _id: _.uniqueId("OSCE_"),
                  text: botResponse,
                  createdAt: new Date(),
                  user: {
                    _id: "bot",
                    name: "bot"
                  }
                }
              ];
            }
            dispatch({ type: APPEND_MESSAGE_INVESTIGATION, payload: botMessage });
            if(getstate().ChatBot.sessionId === null){
              dispatch({ type: SESSION_ID, payload: sessionId })
            }
          } else {
            alert(response.message);
            dispatch({ type: APPEND_MESSAGE_INVESTIGATION, payload: [] });
          }
        })
        .catch(error => {
          dispatch({ type: CHAT_RESPONSE_LOADER_INVESTIGATION, payload: false });
        });
  };
};

export const resetChatBotDataExam = () => {
  return dispatch => {
    dispatch({ type: RESET_CHAT_BOT_DATA_EXAMINATION });
  };
};

export const resetChatBotListingExam = () => {
  return dispatch => {
    dispatch({ type:RESET_MESSAGE_LIST_EXAMINATION  });
  };
};




export const sendMessage = (query = {}) => {
  return (dispatch, getstate) => {

    dispatch({ type: CHAT_RESPONSE_LOADER, payload: true });
    RestClient.restCall(`${getstate().AppConfig.serverUrl}chat`, query)
      .then(response => {
        dispatch({ type: CHAT_RESPONSE_LOADER, payload: false });
        if (response.statusCode === 200) {
          let { sessionId, botResponse } = response.data;
          let botMessage;let type;
          if(response.data?.payload && response.data?.payload?.url !== '' ){
            //  type =response.data?.payload?.type === 'image' ? 'image':'audio'
            type = response.data?.payload?.type
            botMessage = [
              {
                _id: _.uniqueId("OSCE_"),
                [type]: response.data?.payload.url,
                createdAt: new Date(),
                user: {
                  _id: "bot",
                  name: "bot"
                }
              }
            ];
          }else {
            botMessage = [
              {
                _id: _.uniqueId("OSCE_"),
                text: botResponse,
                createdAt: new Date(),
                user: {
                  _id: "bot",
                  name: "bot"
                }
              }
            ];
          }
          dispatch({ type: APPEND_MESAGE, payload: botMessage });
          //only for the first message
          if(getstate().ChatBot.sessionId === null){
            dispatch({ type: SESSION_ID, payload: sessionId })
          }
          // dispatch({ type: SESSION_ID, payload: sessionId });
        } else {
          alert(response.message);
          dispatch({ type: APPEND_MESAGE, payload: [] });
        }
      })
      .catch(error => {
        dispatch({ type: CHAT_RESPONSE_LOADER, payload: false });
      });
   };
};
//reset chatbot listing data
export const resetChatBotData = () => {
  return dispatch => {
    dispatch({ type: RESET_CHAT_BOT_DATA });
  };
};
