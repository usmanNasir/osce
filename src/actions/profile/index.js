import { TRIGGER_SWITCH } from "../../actionTypes";

export const triggerSwitch = value => ({
  type: TRIGGER_SWITCH,
  currentValue: value
});
