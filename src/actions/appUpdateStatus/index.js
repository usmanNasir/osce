import RestClient from "../../helpers/RestClient";
import {
  GET_APPSTATUS_PENDING,
  GET_APPSTATUS_FULLFILLED,
  GET_APPSTATUS_REJECTED,
} from "../../actionTypes";


export const getAppStatus = () => async (dispatch, getstate) => { 
  try {
    dispatch({ type: GET_APPSTATUS_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}appUpdate`
    );
    if (response && response.statusCode === 200) {
      const data = response.data.appUpdateData;
      let appStatus = data[0];
       dispatch({
        type: GET_APPSTATUS_FULLFILLED,
        payload: { data: appStatus },
      });
    } else {
      dispatch({
        type: GET_APPSTATUS_REJECTED,
        payload: { error: response.error },
      });
    }
  } catch (error) {
    dispatch({
      type: GET_APPSTATUS_REJECTED,
      payload: { error: error },
    });
  }
};
