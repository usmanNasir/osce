import RestClient from "../../helpers/RestClient";
import {
  GET_SPECIFIC_DIGNOSIS_FULLFILLED,
  GET_SPECIFIC_DIGNOSIS_REJECTED,
  GET_SPECIFIC_DIGNOSIS_PENDING,
  GET_ALL_DIGNOSIS_FULLFILLED,
  GET_ALL_DIGNOSIS_REJECTED,
  GET_ALL_DIGNOSIS_PENDING,
} from "../../actionTypes";

export const getAllDignosis = () => async (dispatch, getstate) => {
  try {
    dispatch({ type: GET_ALL_DIGNOSIS_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}dignosis/`
    );
    if (response && response.statusCode === 200) {
      dispatch({
        type: GET_ALL_DIGNOSIS_FULLFILLED,
        payload: { data: response.data },
      });
    } else
      dispatch({
        type: GET_ALL_DIGNOSIS_REJECTED,
        payload: { error: response.error },
      });
  } catch (error) {
    dispatch({ type: GET_ALL_DIGNOSIS_REJECTED, payload: { error: error } });
  }
};

export const getSpecificDignosis = (historyId) => async (
  dispatch,
  getstate
) => {
  try {
    dispatch({ type: GET_SPECIFIC_DIGNOSIS_PENDING, payload: true });
    const url = `${
      getstate().AppConfig.serverUrl
    }dignosis/history/${historyId}`;
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}dignosis/history/${historyId}`
    );
    if (response && response.statusCode === 200) {
      dispatch({
        type: GET_SPECIFIC_DIGNOSIS_FULLFILLED,
        payload: { data: response.data.DignosisList[0] },
      });
    } else
      dispatch({
        type: GET_SPECIFIC_DIGNOSIS_REJECTED,
        payload: { error: response.error },
      });
  } catch (error) {
    dispatch({
      type: GET_SPECIFIC_DIGNOSIS_REJECTED,
      payload: { error: error },
    });
  }
};
