import { TRIGGER_PADDING_CHANGE,TRIGGER_MARGIN_CHANGE,TRIGGER_RESET_CHANGE_TRIGGER,TRIGGER_BUTTON_CHANGE,TRIGGER_RESET_BUTTON_CHANGE,TRIGGER_HOME_REFRESH_CHANGE } from "../../actionTypes";

export const triggerPaddingchange = () => ({
  type: TRIGGER_PADDING_CHANGE,
});
export const triggerMarginchange = () => ({
  type: TRIGGER_MARGIN_CHANGE,
});
export const triggerResetChange = ()=>({
  type:TRIGGER_RESET_CHANGE_TRIGGER
});
export const triggerButtonChange = ()=>({
  type:TRIGGER_BUTTON_CHANGE
})
export const triggerResetButtonChange = ()=>({
  type:TRIGGER_RESET_BUTTON_CHANGE
})
export const triggerHomeRefreshChange = (value)=> ({
    type: TRIGGER_HOME_REFRESH_CHANGE,
    payload: value
  })
