import RestClient from "../../helpers/RestClient";
import {appleAuth,AppleAuthRequestOperation} from '@invertase/react-native-apple-authentication';
import { goHome, goToFirstScreen } from "../../config/navigation";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes
} from "react-native-google-signin";
import {postChallengeforUser} from '../homeScreen/homeScreen';

import {
  AUTH_PENDING,
  AUTH_FULLFILLED,
  AUTH_REJECTED,
  AUTH_UPDATE_PENDING,
  AUTH_UPDATE_REJECTED,
  AUTH_UPDATE_FULLFILLED,
  RESET_AUTH,
  AUTH_LOGOUT,
  AUTH_UPDATE_UNIVERSITY_PENDING,
  AUTH_UPDATE_UNIVERSITY_FULLFILLED,
  AUTH_UPDATE_UNIVERSITY_REJECTED,
  AUTH_UPDATE_NOTIFICATION_PENDING,
  AUTH_UPDATE_NOTIFICATION_REJECTED,
  AUTH_UPDATE_NOTIFICATION_FULLFILLED

} from "../../actionTypes";
import { LoginManager, AccessToken } from "react-native-fbsdk";
``;
import _ from "underscore";
import { logger } from "./../../utilities/analytics-logger";
import {AsyncStorage} from 'react-native';
const fetchFbProfile = async accessToken => {
  try {
    const response = await fetch(
      `${"https://graph.facebook.com/v2.5/me?fields=email,first_name,last_name,name,picture&access_token="}${accessToken}`
    );
    return response.json();
  } catch (error) {
    return error;
  }
};

//changed login action, set response only in reducer if code is 200
export const login = params => async (dispatch, getstate) => {
  try {

    dispatch({ type: AUTH_PENDING, payload: true });
    const response = await RestClient.post(
      `${getstate().AppConfig.serverUrl}user/login/`,
      {...params}
    );

    if (response && response.statusCode === 200) {
      logger.logEventWithData("User_LoggedIn", { email: response.data.email });
      const id = response.data._id;
      logger.setUserId(id);
      logger.setUserProperties({ email: response.data.email });
      dispatch({ type: AUTH_FULLFILLED, payload: response });
      if (response.data && response.data.token) goHome();
    } else dispatch({ type: AUTH_REJECTED, payload: {} });
    return new Promise(res => res(response));
  } catch (error) {
    dispatch({ type: AUTH_REJECTED, payload: error });
    return new Promise(res => res({ message: "", error: "", statusCode: 400 }));
  }
};

export const updateNotificationPermission = (isEnabled,email) => async (dispatch, getstate) => {
    try {
    
      dispatch({ type: AUTH_UPDATE_NOTIFICATION_PENDING, payload: true });
      const response = await RestClient.post(
        `${getstate().AppConfig.serverUrl}user/updateNotificationPermission/`,
          {   email:email,
            notificationEnabled:isEnabled
          }
      );
  
      if (response && response.statusCode === 200) {
        logger.logEventWithData("USER UPDATED", { email: response.data.email });
        const id = response.data._id;
        logger.setUserId(id);
        logger.setUserProperties({ email: response.data.email });
        dispatch({ type: AUTH_UPDATE_NOTIFICATION_FULLFILLED, payload: response });
      } else dispatch({ type: AUTH_UPDATE_NOTIFICATION_REJECTED, payload: {} });
      return new Promise(res => res(response));
    } catch (error) {
      dispatch({ type: AUTH_UPDATE_NOTIFICATION_REJECTED, payload: error });
      return new Promise(res => res({ message: "", error: "", statusCode: 400 }));
    }
  };

// UPDATE FCM TOKEN
export const updateFcmToken = () => async (dispatch, getstate) => {
  try {
    const keys = await AsyncStorage.getAllKeys();
    const fcmToken = await AsyncStorage.getItem('fcmToken');
    dispatch({ type: AUTH_UPDATE_PENDING, payload: true });
    const response = await RestClient.post(
      `${getstate().AppConfig.serverUrl}user/updatefcmToken/`,
        {   email:getstate().Auth.authData.email,
          fcmToken:fcmToken
        }
    );

    if (response && response.statusCode === 200) {
      logger.logEventWithData("USER UPDATED", { email: response.data.email });
      const id = response.data._id;
      logger.setUserId(id);
      logger.setUserProperties({ email: response.data.email });
      dispatch({ type: AUTH_UPDATE_FULLFILLED, payload: response });
    } else dispatch({ type: AUTH_UPDATE_REJECTED, payload: {} });
    return new Promise(res => res(response));
  } catch (error) {
    dispatch({ type: AUTH_UPDATE_REJECTED, payload: error });
    return new Promise(res => res({ message: "", error: "", statusCode: 400 }));
  }
};


// UPDATE USER UNIVERSITY
export const updateUniversity = (university,email) => async (dispatch, getstate) => {
  try {
  
    dispatch({ type: AUTH_UPDATE_UNIVERSITY_FULLFILLED, payload: true });
    const response = await RestClient.post(
      `${getstate().AppConfig.serverUrl}user/updateUniversity/`,
        {   email:email,
          university:university
        }
    );

    if (response && response.statusCode === 200) {
      logger.logEventWithData("USER UPDATED", { email: response.data.email });
      const id = response.data._id;
      logger.setUserId(id);
      logger.setUserProperties({ email: response.data.email });
      dispatch({ type: AUTH_UPDATE_UNIVERSITY_FULLFILLED, payload: response });
    } else dispatch({ type: AUTH_UPDATE_UNIVERSITY_REJECTED, payload: {} });
    return new Promise(res => res(response));
  } catch (error) {
    dispatch({ type: AUTH_UPDATE_UNIVERSITY_REJECTED, payload: error });
    return new Promise(res => res({ message: "", error: "", statusCode: 400 }));
  }
};


const facebookLogin = async () => {
  const result = await LoginManager.logInWithPermissions([
    "email",
    "public_profile",
    "user_frineds"
  ]);
  if (!result.isCancelled) {
    const { accessToken } = await AccessToken.getCurrentAccessToken();
    if (accessToken) {
      const {
        first_name,
        last_name,
        name,
        id,
        email,
        picture
      } = await fetchFbProfile(accessToken);

      return new Promise(res =>
        res({ first_name, last_name, email, name, id, picture, accessToken })
      );
    } else
      return new Promise((res, rej) =>
        rej({ message: "There is some error occured. Please try again later." })
      );
  } else
    return new Promise((res, rej) =>
      rej({ message: "There is some error occured. Please try again later." })
    );
};

export const loginWithFb = () => async (dispatch, getstate) => {
  try {
    const {
      email = "",
      name = "",
      id = "",
      accessToken = ""
    } = await facebookLogin();
    dispatch({ type: AUTH_PENDING, payload: true });
    const response = await RestClient.post(
      `${getstate().AppConfig.serverUrl}user/fbLogin/`,
      {
        email,
        name,
        facebookId: id,
        fbToken: accessToken,
        type: "facebook",
        source: "mobile"
      }
    );
    if (response && response.statusCode === 200) {
      logger.logEvent("User LoggedInFacebook");
      dispatch({ type: AUTH_FULLFILLED, payload: response });
      if (response.data && response.data.token) goHome();
    } else
      throw {
        message:
          "The server is not reachable right now, sorry for inconvenience."
      };
    return new Promise(res => res(response));
  } catch (error) {
    dispatch({ type: AUTH_REJECTED, payload: error });
    return new Promise(res =>
      res({ message: error.message, error: "", statusCode: 400 })
    );
  }
};

const googleSignin = async () => {
  //  initial configuration
  GoogleSignin.configure({
    //It is mandatory to call this method before attempting to call signIn()
    // Repleace with your webClientId generated from Firebase console
    webClientId:
      "863583170600-17s25342dahgfcj6jg1d20nlvj9s1k68.apps.googleusercontent.com"
  });
};

export const _signIn = () => async (dispatch, getstate) => {
  googleSignin();
  //Prompts a modal to let the user sign in into your application.
  try {
    await GoogleSignin.hasPlayServices({
      //Check if device has Google Play Services installed.
      //Always resolves to true on iOS.
      showPlayServicesUpdateDialog: true
    });

    const userInfo = await GoogleSignin.signIn();
    logger.logEvent("User LoggedInGoogle");
    dispatch({ type: AUTH_PENDING, payload: true });
    const response = await RestClient.post(
      "http://35.178.22.72:4571/user/gmailLogin/",
      {
        name: "" + userInfo.user.name + "",
        source: "mobile",
        type: "gmail",
        gmailId: "" + userInfo.user.id + "",
        email: "" + userInfo.user.email + ""
      }
    );
    if (response && response.statusCode === 200) {
      logger.logEvent("User LoggedInGoogle",response);
      dispatch({ type: AUTH_FULLFILLED, payload: response });
      if (response.data && response.data.token) goHome();
    } else
      throw {
        message:
          "The server is not reachable right now, sorry for inconvenience."
      };
    return new Promise(res => res(response));
  } catch (error) {
    dispatch({ type: AUTH_REJECTED, payload: error });
    return new Promise(res =>
      res({
        message: error.message,
        error: "",
        statusCode: 400
      })
    );
  }
};

export const apple_signIn = (userData) => async (dispatch, getstate) => {

  try {


    dispatch({ type: AUTH_PENDING, payload: true });

    const response = await RestClient.post(
        "http://35.178.22.72:4571/user/gmailLogin/",
        {
          name: "" + userData.name + "",
          source: "mobile",
          type: "apple",
          gmailId: "" + userData.user + "",
          email: "" + userData.email + ""
        }
    );
    if (response && response.statusCode === 200) {
      logger.logEvent("User LoggedInApple");
      dispatch({ type: AUTH_FULLFILLED, payload: response });
      if (response.data && response.data.token) goHome();
    } else
      throw {
        message:
            "The server is not reachable right now, sorry for inconvenience."
      };
    return new Promise(res => res(response));
  } catch (error) {
    dispatch({ type: AUTH_REJECTED, payload: error });
    return new Promise(res =>
        res({
          message: error.message,
          error: "",
          statusCode: 400
        })
    );
  }
};
export const signUp = params => async (dispatch, getstate) => {
  try {

    dispatch({ type: AUTH_PENDING, payload: true });
    const response = await RestClient.post(
      `${getstate().AppConfig.serverUrl}user/registration/`,
      params
    );

    if (response && response.statusCode === 200) {
      logger.logEvent("User SignUp");
      const id = response.data._id;
      logger.setUserId(id);
      logger.setUserProperties({ email: response.data.email });
      dispatch({ type: AUTH_FULLFILLED, payload: response });
      if (response.data && response.data.token) goHome();
    } else dispatch({ type: AUTH_REJECTED, payload: {} });
    return new Promise(res => res(response));
  } catch (error) {
    dispatch({ type: AUTH_REJECTED, payload: error });
    return new Promise(res => res({ message: "", error: "", statusCode: 400 }));
  }
};
// logout from app
export const logout =  () => {
  return async dispatch => {
    const keys = await AsyncStorage.getAllKeys();
    await AsyncStorage.setItem('showTutorial','false');
    dispatch({ type: AUTH_LOGOUT });
    logger.logEvent("User_LoggedOut");
    goToFirstScreen();
  };
};
// export const logoutwithApple = ()=>{
//   return dispatch=>{
//     dispatch({type:AUTH_LOGOUT});
//      appleAuth.performRequest({
//       requestedOperation: AppleAuthRequestOperation.LOGOUT
//     })
//   }
// }

export const logoutfromGoogle =  () => {
  return async dispatch => {
    await AsyncStorage.setItem('showTutorial','false');
   
    dispatch({ type: AUTH_LOGOUT });
    _signOut();
  
    logger.logEvent("User_LoggedOut");
    goToFirstScreen();
  };
};

//reset chatbot listing data
export const resetLoginData = () => {
  return async dispatch => {
    await AsyncStorage.setItem('showTutorial','false');
    dispatch({ type: RESET_AUTH });
  };
};

export const _signOut = async () => {
  googleSignin();
  //Remove user session from the device.
  try {
    await AsyncStorage.setItem('showTutorial','false');
    await GoogleSignin.revokeAccess();
    await GoogleSignin.signOut();
    goToFirstScreen(); // Remove the user from your app's state as well
  } catch (error) {
    console.error(error);
  }
};
