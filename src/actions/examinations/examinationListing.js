import RestClient from "../../helpers/RestClient";
import {
    GET_EXAMINATION_LISTING,
    EXAMINATION_LOADER_VISIBILITY,
    TOTAL_PAGE_NO_EXAMINATION,
    UPDATE_EXAMINATION_LISTING,
    PAGINATION_LOADER_EXAMINATION,
    GET_EXAMINATIONS_REJECTED,
    GET_EXAMINATIONS_FULLFILLED,
    GET_EXAMINATIONS_PENDING,
    ADD_GRADING,
    GET_GRADING_REJECTED,
    GET_GRADING_FULLFILLED,
    GET_GRADING_PENDING,
    ADD_GRADING_HOME
} from "../../actionTypes";
import { showMessage } from "react-native-flash-message";

export const ExaminationListing = (page = 1, isPagination = false) => {
  return (dispatch, getstate) => {
    //show history loader
    isPagination
      ? dispatch({ type: PAGINATION_LOADER_EXAMINATION, payload: true })
      : dispatch({ type: EXAMINATION_LOADER_VISIBILITY, payload: true });
    RestClient.getCall(`${getstate().AppConfig.serverUrl}complaints/${page}`)
      .then(response => {
        //hide history loader
        isPagination
          ? dispatch({ type: PAGINATION_LOADER_EXAMINATION, payload: false })
          : dispatch({ type: EXAMINATION_LOADER_VISIBILITY, payload: false });
        if (response.statusCode == 200) {
          //update total page nos
          dispatch({ type: TOTAL_PAGE_NO_EXAMINATION, payload: response.data.totalPages });
          //set update history data
          isPagination
            ? dispatch({
                type: UPDATE_EXAMINATION_LISTING,
                payload: response.data.histories
              })
            : dispatch({
                type: GET_EXAMINATION_LISTING,
                payload: response.data.histories
              });
        } else {
          //show error message
          showMessage({
            message: response.message,
            type: "warning",
            icon: "warning"
          });
        }
      })
      .catch(error => {
        //hide history loader
        isPagination
          ? dispatch({ type: PAGINATION_LOADER_EXAMINATION, payload: false })
          : dispatch({ type: EXAMINATION_LOADER_VISIBILITY, payload: false });
      });
  };
};

export const getExamination = ({ pageNo = 1, _id = "" }) => async (
  dispatch,
  getstate
) => {
  try {
    dispatch({ type: GET_EXAMINATIONS_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}examination/get/${pageNo}/${_id}`
    );
    const { data = {} } = response;
    const { examinations = [] } = data;
    if (response && response.statusCode === 200) {
      dispatch({ type: GET_EXAMINATIONS_FULLFILLED, payload: { examinations } });
    } else dispatch({ type: GET_EXAMINATIONS_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: GET_EXAMINATIONS_REJECTED, payload: {} });
  }
};

// export const getAllHistory = () => async (dispatch, getstate) => {
//   try {
//     const response = await RestClient.getCall(
//       `${getstate().AppConfig.serverUrl}history/get`
//     );

//     if (response && response.statusCode === 200) {
//       dispatch({
//         type: ADD_GRADING_HOME,
//         payload: response.data
//       });
//     } else dispatch({ type: GET_GRADING_REJECTED, payload: {} });
//   } catch (error) {
//     dispatch({ type: GET_GRADING_REJECTED, payload: {} });
//   }
//};
