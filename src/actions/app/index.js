import * as types from "../../actionTypes";
import { Navigation } from "react-native-navigation";

export const pushToParticularScreen = (componentId, screenName, passProps) => {
  return dispatch => {
    Navigation.push(componentId, {
      component: {
        name: screenName,
        passProps
      }
    });
  };
};

export const pop = componentId => {
  return dispatch => {
    Navigation.pop(componentId);
  };
};

/**
 * Navigate to a new page and clear the backstack.
 */
export function resetTo(newScreen: string): Function {
  return dispatch => {
    dispatch(navigate(newScreen, true));
  };
}

export const mergeOptions = (componentId, draweropen) => {
  return dispatch => {
    Navigation.mergeOptions(componentId, {
      sideMenu: {
        left: {
          visible: draweropen
        }
      }
    });
  };
};
export const setScrenStack = (componentId, screen, visible, props) => {
  return dispatch => {
    Navigation.setStackRoot(componentId, {
      component: {
        name: screen,
        passProps: { item: props },
        options: {
          topBar: {
            visible: visible,
            title: {
              text: "Home"
            }
          },
          bottomTabs: {
            visible:true,
            drawBehind: false
          }
        }
      }
    });
  };
};

/**
 * Internal helper method for setting the redux state
 */
export const navigate = (newScreen: string, reset: boolean) => {
  return {
    type: t.SCREEN,
    screen: newScreen,
    isReset: reset
  };
};
