import RestClient from "../../helpers/RestClient";
import {
  GET_HISTORY_LISTING,
  HISTORY_LOADER_VISIBILITY,
  TOTAL_PAGE_NO,
  UPDATE_HISTORY_LISTING,
  PAGINATION_LOADER_HISTORY,
  GET_COMPLAINTS_REJECTED,
  GET_COMPLAINTS_FULLFILLED,
  GET_COMPLAINTS_PENDING,
  ADD_GRADING,
  GET_GRADING_REJECTED,
  GET_GRADING_FULLFILLED,
  GET_GRADING_PENDING,
  ADD_GRADING_HOME
} from "../../actionTypes";
import { showMessage } from "react-native-flash-message";
export const HistoryListing = (page = 1, isPagination = false) => {
  return (dispatch, getstate) => {
    //show history loader
    isPagination
      ? dispatch({ type: PAGINATION_LOADER_HISTORY, payload: true })
      : dispatch({ type: HISTORY_LOADER_VISIBILITY, payload: true });
    RestClient.getCall(`${getstate().AppConfig.serverUrl}complaints/${page}`)
      .then(response => {
        //hide history loader
        isPagination
          ? dispatch({ type: PAGINATION_LOADER_HISTORY, payload: false })
          : dispatch({ type: HISTORY_LOADER_VISIBILITY, payload: false });
        if (response.statusCode == 200) {
          //update total page nos
          dispatch({ type: TOTAL_PAGE_NO, payload: response.data.totalPages });
          //set update history data
          isPagination
            ? dispatch({
                type: UPDATE_HISTORY_LISTING,
                payload: response.data.histories
              })
            : dispatch({
                type: GET_HISTORY_LISTING,
                payload: response.data.histories
              });
        } else {
          //show error message
          showMessage({
            message: response.message,
            type: "warning",
            icon: "warning"
          });
        }
      })
      .catch(error => {
        //hide history loader
        isPagination
          ? dispatch({ type: PAGINATION_LOADER_HISTORY, payload: false })
          : dispatch({ type: HISTORY_LOADER_VISIBILITY, payload: false });
      });
  };
};

export const getHistory = ({ pageNo = 1, _id = "" }) => async (
  dispatch,
  getstate
) => {
  try {
    dispatch({ type: GET_COMPLAINTS_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}history/get/${pageNo}/${_id}`
    );
    const { data = {} } = response;
    const { histories = [] } = data;
    if (response && response.statusCode === 200) {
      dispatch({ type: GET_COMPLAINTS_FULLFILLED, payload: { histories } });
    } else dispatch({ type: GET_COMPLAINTS_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: GET_COMPLAINTS_REJECTED, payload: {} });
  }
};

// export const getAllHistory = () => async (dispatch, getstate) => {
//   try {
//     const response = await RestClient.getCall(
//       `${getstate().AppConfig.serverUrl}history/get`
//     );

//     if (response && response.statusCode === 200) {
//       dispatch({
//         type: ADD_GRADING_HOME,
//         payload: response.data
//       });
//     } else dispatch({ type: GET_GRADING_REJECTED, payload: {} });
//   } catch (error) {
//     dispatch({ type: GET_GRADING_REJECTED, payload: {} });
//   }
//};
