/*
Creator: Jaspreet kaur
Date: 6 nov 2019
purpose: This file handles all actions related to histoy listing
*/
import RestClient from "../../helpers/RestClient";
import {
  GET_SCORES_REJECTED,
  GET_SCORES_FULLFILLED,
  GET_SCORES_PENDING,
  RESET_SCORES,
  GET_INFO_REJECTED,
  GET_INFO_FULLFILLED,
  GET_INFO_PENDING,
  RESET_INFO,
  GET_CONDITIONS_REJECTED,
  GET_CONDITIONS_FULLFILLED,
  GET_CONDITIONS_PENDING,
  RESET_CONDITIONS
} from "../../actionTypes";

// export const getScores = (
//   historyId = "",
//   complaintId = "",
//   sessionId = ""
// ) => async (dispatch, getstate) => {

//   try {
//     dispatch({ type: GET_SCORES_PENDING, payload: true });
//     const response = await RestClient.getCall(
//       `${
//         getstate().AppConfig.serverUrl
//       }score/get/${historyId}/${complaintId}/${sessionId}`
//       // `http://192.168.100.51:4571/scores/get/${historyId}/${complaintId}/${sessionId}`
//     );

//     const { data = {} } = response;

//     if (response && response.statusCode === 200) {
//       dispatch({ type: GET_SCORES_FULLFILLED, payload: data });
//     } else dispatch({ type: GET_SCORES_REJECTED, payload: {} });

//     return new Promise(res => res(response));
//   } catch (error) {
//     dispatch({ type: GET_SCORES_REJECTED, payload: {} });
//     return new Promise(res =>
//       res({ message: error.message, error: error, statusCode: 400 })
//     );
//   }
// };
export const getSharingUrl = async (url)=>{
  const response = await RestClient.getCall(url);
  return response.data;
}

export const getScores = (
  historyId = "",
  complaintId = "",
  sessionId = "",
  isChallenge = false,
  awardDignosis = false,
  awardScore = 0,
) => async (dispatch, getstate) => {
  const userId = getstate().Auth.authData._id;
  const examinationIndex = getstate().ExaminationListing.examinationListing.examinations.findIndex((item)=>item.historyId === historyId);
  const investigationIndex = getstate().InvestigationListing.investigationListing.investigations.findIndex((item)=>item.historyId === historyId);
  try {
    dispatch({ type: GET_SCORES_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${
        getstate().AppConfig.serverUrl
      }scores/get/${historyId}/${complaintId}/${sessionId}/${userId}/${isChallenge}/${awardDignosis}/${awardScore}`
    );

    const { data = {} } = response;

    if (response && response.statusCode === 200) {
      let newData = data.listing;
      if(examinationIndex === -1  && investigationIndex === -1 ){
        //not exists index
          newData =  newData.filter((item)=>item.subHeading !== 'Examination' && item.subHeading !== 'Investigation');
      }else if(examinationIndex >-1 && investigationIndex === -1){
        newData =  newData.filter((item)=>item.subHeading !== 'Investigation');
      }else if(investigationIndex > -1 && examinationIndex === -1) {
        newData =  newData.filter((item)=>item.subHeading !== 'Examination');
      }


      data.listing = newData;
      dispatch({ type: GET_SCORES_FULLFILLED, payload: data });
    } else dispatch({ type: GET_SCORES_REJECTED, payload: {} });

    return new Promise(res => res(response));
  } catch (error) {
    dispatch({ type: GET_SCORES_REJECTED, payload: {} });
    return new Promise(res =>
      res({ message: error.message, error: error, statusCode: 400 })
    );
  }
};

export const resetScores = () => {
  return dispatch => {
    dispatch({ type: RESET_SCORES });
  };
};

export const getInfo = (feedbackId = "", status = "") => async (
  dispatch,
  getstate
) => {
  try {
    dispatch({ type: GET_INFO_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}moreInfo/${feedbackId}/${status}`
    );
    const { data = {} } = response;
    if (response && response.statusCode === 200) {
      dispatch({ type: GET_INFO_FULLFILLED, payload: data });
    } else dispatch({ type: GET_INFO_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: GET_INFO_REJECTED, payload: {} });
  }
};

// http://dfedfb9d7ef9.ngrok.io/leaderBoard/getPosition/5dedd8f9ca6787225b23773d



export const getRank = (userId,url) => async () => {

  try {
    const response = await RestClient.getCall(`${url}leaderBoard/getPosition/${userId}`);
    const { data = {} } = response;
      return data;
  } catch (error) {
   return error
  }
};


export const resetInfoData = () => {
  return dispatch => {
    dispatch({ type: RESET_INFO });
  };
};

export const getConditions = (historyId = "") => async (dispatch, getstate) => {
  try {
    dispatch({ type: GET_CONDITIONS_PENDING, payload: true });
    const response = await RestClient.getCall(
      `${getstate().AppConfig.serverUrl}conditions/${historyId}`
    );
    const { data = {} } = response;
    if (response && response.statusCode === 200) {
      dispatch({ type: GET_CONDITIONS_FULLFILLED, payload: data });
    } else dispatch({ type: GET_CONDITIONS_REJECTED, payload: {} });
  } catch (error) {
    dispatch({ type: GET_CONDITIONS_REJECTED, payload: {} });
  }
};

export const resetConditionsData = () => {
  return dispatch => {
    dispatch({ type: RESET_CONDITIONS });
  };
};
