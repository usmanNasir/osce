/* eslint-disable */

"use strict";

import NetInfo from "@react-native-community/netinfo";
import { storeObj } from "../store/setup";

class RestClient {
  static isConnected() {
    return new Promise(function(fulfill, reject) {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) fulfill(isConnected);
        else {
          reject(isConnected);
        }
      });
    });
  }

  static restCall(url, params, token = null, type = "POST", transToken = null) {
    let context = this;
    // getting token from state reducer
    const state = storeObj.store ? storeObj.store.getState() : {};
    const jwtoken = state.Auth ? state["Auth"].token : null;
    return new Promise(function(fulfill, reject) {
      context
        .isConnected()
        .then(() => {
          fetch(url, {
            method: type,
            timeout: 1000 * 1 * 60,
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              Authorization: token
                ? token
                : jwtoken
                ? `bearer ${jwtoken}`
                : null,
              transToken: transToken
            },
            body: JSON.stringify(params)
          })
            .then(response => {
              return response.text();
            })
            .then(responseText => {
              fulfill(JSON.parse(responseText));
            })
            .catch(error => {
              reject({
                error: true,
                frontend: true,
                message:
                  "The server is not reachable right now, sorry for inconvenience."
              });
            });
        })
        .catch(error => {
          reject({
            error: true,
            frontend: true,
            message: "Please check your internet connectivity."
          });
        });
    });
  }

  static getCall(url, token = null) {
    let context = this;
    // getting token from state reducer
    const state = storeObj.store ? storeObj.store.getState() : {};
    const jwtoken = state.Auth ? state["Auth"].token : null;

    return new Promise(function(fulfill, reject) {
      context
        .isConnected()
        .then(() => {
          fetch(url, {
            method: "GET",
            timeout: 1000 * 1 * 60,
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              Authorization: token
                ? token
                : jwtoken
                ? `bearer ${jwtoken}`
                : null
            }
          })
            .then(response => {
              return response.text();
            })
            .then(responseText => {
              fulfill(JSON.parse(responseText));
            })
            .catch(error => {
              reject({
                error: true,
                frontend: true,
                message:
                  "The server is not reachable right now, sorry for inconvenience."
              });
            });
        })
        .catch(error => {
          reject({
            error: true,
            frontend: true,
            message: "Please check your internet connectivity."
          });
        });
    });
  }

  static delCall(url, token = null) {
    let context = this;

    return new Promise(function(fulfill, reject) {
      context
        .isConnected()
        .then(() => {
          fetch(url, {
            method: "Delete",
            timeout: 1000 * 1 * 60,
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              Authorization: token
            }
          })
            .then(response => {
              return response.text();
            })
            .then(responseText => {
              fulfill(JSON.parse(responseText));
            })
            .catch(error => {
              reject({
                error: true,
                frontend: true,
                message:
                  "The server is not reachable right now, sorry for inconvenience."
              });
            });
        })
        .catch(error => {
          reject({
            error: true,
            frontend: true,
            message: "Please check your internet connectivity."
          });
        });
    });
  }

  static post(url, params, deviceToken = null, deviceType = null) {
    let context = this;

    return new Promise(function(fulfill, reject) {
      context
        .isConnected()
        .then(() => {
          fetch(url, {
            method: "POST",
            timeout: 1000 * 1 * 60,
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              "Cache-Control": "no-cache",
              "device-type": deviceType,
              "device-token": deviceToken
            },
            body: JSON.stringify(params)
          })
            .then(response => {
              return response.text();
            })
            .then(responseText => {
              fulfill(JSON.parse(responseText));
            })
            .catch(error => {
              reject({
                error: true,
                frontend: true,
                message:
                  "The server is not reachable right now, sorry for inconvenience."
              });
            });
        })
        .catch(error => {
          reject({
            error: true,
            frontend: true,
            message: "Please check your internet connectivity."
          });
        });
    });
  }

  static formData(
    url,
    params = {},
    token = null,
    type = "POST",
    transToken = null
  ) {
    let context = this;
    let formData = new FormData();
    for (let [key, value] of Object.entries(params)) {
      formData.append(key, value);
    }
    return new Promise(function(fulfill, reject) {
      context
        .isConnected()
        .then(() => {
          fetch(url, {
            method: type,
            timeout: 1000 * 1 * 60,
            headers: {
              Accept: "application/json",
              "Cache-Control": "no-cache",
              Authorization: token,
              transToken: transToken
            },
            body: formData
          })
            .then(response => {
              return response.text();
            })
            .then(responseText => {
              fulfill(JSON.parse(responseText));
            })
            .catch(error => {
              reject({
                error: true,
                frontend: true,
                message:
                  "The server is not reachable right now, sorry for inconvenience."
              });
            });
        })
        .catch(error => {
          reject({
            error: true,
            frontend: true,
            message: "Please check your internet connectivity."
          });
        });
    });
  }
}

export default RestClient;
