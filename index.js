import React from 'react';
import { Navigation } from "react-native-navigation";
import { registerScreens } from "./src/config/routes";
import { addListeners } from "./src/utilities/listeners";
import { Provider } from "react-redux";
import {Linking} from 'react-native';
import setup from "./src/store/setup";
import SplashScreen from "react-native-splash-screen";
import { logger } from "./src/utilities/analytics-logger";
import NotifyService from './src/utilities/NotifyService';
import { MixPanelContext } from './src/utilities/MixPanel';
import { App_Launch } from './src/constants/Events';
import inAppUpdateModal from './src/components/common/inAppUpdateModal';
import { checkVersion } from './src/utilities/updateCheck';
import { goToAppUpdate } from './src/config/navigation';

import('./src/utilities/ReactotronConfig').then(() => console.log('Reactotron Configured'))



  // Linking.getInitialURL().then((url)=>{
  //   console.log("URL",url)
  //      const route = url.replace(/.*?:\/\//g, '');
  //   const idSplit = route.match(/\/([^\/]+)\/?$/)[1];
  //   const profileId = idSplit.split('?id:')[1]
  //   const routeName = route.split('/')[1];
  //   const id = profileId
  // console.log("ID",id);
  // console.log("ROUTE",route);
  //   console.log("PROFILE ID",profileId);
  //     console.log("ID",id);
  //   if(routeName === 'LeaderBoard'){
  //     Navigation.navigate('LeaderBoard',{id})
  //   }

  //   });

  


Navigation.events().registerAppLaunchedListener(() => {
  logger.logEvent("APP_LAUNCH");
  SplashScreen.hide();
  const store = setup();
  registerScreens(store, Provider);
  addListeners();
  Navigation.setDefaultOptions({
    topBar: {
      visible: true,
      drawBehind: false
    },
    bottomTabs: {visible: true,  animate: true},
    layout: { orientation: ["portrait"] }
  });
  Navigation.setRoot({
    root: {
      component: {
        name: "Loader"
      }
    }
  });
});
