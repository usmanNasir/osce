package com.osce;
import android.os.Bundle;
import com.reactnativenavigation.NavigationActivity;
import org.devio.rn.splashscreen.SplashScreen;
import io.branch.rnbranch.*; // <-- add this
import android.content.Intent; // <-- and this

public class MainActivity extends NavigationActivity{
 @Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this);  // here
        super.onCreate(savedInstanceState);
      
    }
   @Override
      protected void onStart() {
          super.onStart();
          RNBranchModule.initSession(getIntent().getData(), this);
      }

    @Override
    public void onNewIntent(Intent intent) {
       super.onNewIntent(intent);
         setIntent(intent);
         RNBranchModule.onNewIntent(intent);
    }
       @Override
        protected void onRestart() {
            super.onRestart();
            Intent intent = getIntent();
            setIntent(intent);
            intent.putExtras(this.getIntent());
            intent.putExtra("branch_force_new_session", true);
        }
}
